DROP SCHEMA IF EXISTS meta;
CREATE SCHEMA meta
  CREATE VIEW reference AS
    SELECT
      kcu.constraint_name AS NAME,
      kcu.table_name      AS "from",
      kcu.column_name     AS "from_field",
      ccu.table_name      AS "to",
      ccu.column_name     AS "to_field",
      rc.update_rule,
      rc.delete_rule
    FROM information_schema.key_column_usage kcu
      JOIN information_schema.constraint_column_usage ccu ON kcu.constraint_name = ccu.constraint_name
      JOIN information_schema.referential_constraints rc ON kcu.constraint_name = rc.constraint_name;

CREATE OR REPLACE VIEW meta.attribute AS
  SELECT
    c.relname                                                        AS name,
    json_object_agg(a.attname, format_type(a.atttypid, a.atttypmod)) AS attributes
  FROM pg_class c
    JOIN pg_attribute a ON a.attrelid = c.oid
    JOIN pg_namespace n ON c.relnamespace = n.oid
  WHERE nspname = 'public' AND (relkind = 'r' OR relkind = 'v') AND a.attstattarget <> 0
  GROUP BY c.relname
  ORDER BY relname;

CREATE OR REPLACE VIEW token_user AS
  SELECT
    t.created,
    t.id,
    t.ip,
    t.time,
    t.type,
    t.expires,
    u.email,
    u.nick,
    u.admin,
    u.avatar,
    u.parent,
    row_to_json(u.*) AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id;

CREATE OR REPLACE VIEW identity AS
  SELECT
    t.created,
    t.id,
    t.ip,
    t.time,
    t.type,
    t.expires,
    u.email,
    u.nick,
    u.admin,
    u.avatar,
    u.surname,
    u.geo,
    u.forename,
    u.parent,
    u.created AS user_created,
    u.id      AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id;

CREATE OR REPLACE VIEW user_about AS
  SELECT
    id,
    type,
    nick,
    forename,
    surname,
    avatar,
    email,
    skype,
    phone,
    telegram,
    country,
    settlement,
    address,
    admin
  FROM "user";

CREATE OR REPLACE VIEW transfer_user AS
  SELECT
    tr.id,
    tr.amount,
    tr.type,
    tr.status,
    tr.wallet,
    tr.ip,
    tr.created,
    tr.time,
    tr."from",
    tr."to",
    tr.currency,
    tr.txid,
    f.nick AS from_nick,
    t.nick AS to_nick
  FROM transfer tr
    LEFT JOIN "user" f ON f.id = tr."from"
    LEFT JOIN "user" t ON t.id = tr."to";

CREATE OR REPLACE VIEW user_transfer AS
  SELECT
    *,
    CASE WHEN type IN ('buy', 'withdraw')
      THEN "from"
    ELSE "to" END AS "user"
  FROM transfer;

CREATE OR REPLACE VIEW balance AS
  WITH
      a AS (
      SELECT
        "to" AS "user",
        currency,
        amount
      FROM transfer it
      WHERE "to" IS NOT NULL AND it.status = 'success'
      UNION ALL
      SELECT
        "from" AS "user",
        currency,
        -amount
      FROM transfer ot
      WHERE "from" IS NOT NULL AND ot.status = 'success'
    ),
      b AS (
        SELECT
          a."user",
          currency,
          sum(amount) AS amount
        FROM a
        GROUP BY a."user", currency
    )
  SELECT
    u.id                  AS "user",
    currency,
    coalesce(b.amount, 0) AS amount
  FROM b
    RIGHT JOIN "user" u ON b."user" = u.id;

CREATE OR REPLACE VIEW referral AS
  WITH RECURSIVE r(id, "nick", parent, email, root, level, phone, forename, surname, skype, avatar, telegram, created) AS (
    SELECT
      id,
      nick,
      parent,
      email,
      id AS root,
      0  AS level,
      phone,
      forename,
      surname,
      skype,
      avatar,
      telegram,
      created
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.parent,
      u.email,
      r.root,
      r.level + 1 AS level,
      u.phone,
      u.forename,
      u.surname,
      u.skype,
      u.avatar,
      u.telegram,
      u.created
    FROM "user" u
      JOIN r ON u.parent = r.id
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    parent,
    email,
    root,
    level,
    phone,
    forename,
    surname,
    skype,
    avatar,
    telegram,
    created
  FROM r
  ORDER BY root, level, id;

CREATE OR REPLACE VIEW referral_sponsor AS
  SELECT
    r.*,
    u.nick                                                                   AS sponsor,
    coalesce((SELECT sum(amount)
              FROM transfer
              WHERE "from" = r.id AND type = 'buy' AND currency = 'ETH'), 0) AS eth,
    coalesce((SELECT sum(amount)
              FROM transfer
              WHERE "from" = r.id AND type = 'buy' AND currency = 'BTC'), 0) AS btc,
    coalesce((SELECT sum(amount)
              FROM transfer
              WHERE "from" = r.id AND type = 'buy' AND currency = 'USD'), 0) AS usd,
    (SELECT count(*)
     FROM "user" c
     WHERE c.parent = r.id)                                                  AS children
  FROM referral r
    JOIN "user" u ON r.parent = u.id
    JOIN reward w ON w.level = r.level;

CREATE OR REPLACE VIEW finance AS
  SELECT
    t."user",
    t.type,
    t.currency,
    sum(t.amount) AS amount
  FROM user_transfer t
  WHERE t.status = 'success'
  GROUP BY t."user", t.type, t.currency;

CREATE OR REPLACE VIEW informer AS
  SELECT
    u.id,
    (SELECT count(*)
     FROM "visit" v
     WHERE v.url = '/ref/' || u.nick)                AS visit,
    (SELECT count(*)
     FROM "user" r
     WHERE r.parent = u.id)                          AS invited,
    (SELECT count(*)
     FROM referral r
       JOIN reward w ON w.level = r.level
     WHERE r.root = u.id)                            AS referral,
    (SELECT count(*)
     FROM node n
       JOIN "user" c ON n."user" = c.id
     WHERE c.parent = u.id)                          AS node,
    extract(DAYS FROM CURRENT_TIMESTAMP - u.created) AS day,
    (SELECT count(*) > 0
     FROM node
     WHERE "user" = u.id
     LIMIT 1)                                        AS is_investor,
    (SELECT count(*) > 0
     FROM transfer
     WHERE "from" = u.id AND type = 'withdraw' AND status = 'created'
     LIMIT 1)                                        AS has_unapproved_withdraw,
    u.nick,
    u.type,
    u.surname,
    u.forename,
    u.email,
    u.avatar,
    u.skype,
    u.phone,
    u.telegram,
    u.country,
    u.settlement,
    u.address,
    to_json(p.*)                                     AS parent
  FROM "user" u
    LEFT JOIN user_about p ON u.parent = p.id;

CREATE OR REPLACE VIEW admin_user AS
  SELECT
    u.id,
    u.nick,
    u.type,
    u.admin,
    u.surname,
    u.forename,
    u.skype,
    u.avatar,
    u.email,
    u.parent,
    s.nick                                                      AS sponsor,
    coalesce((SELECT count(*) :: INT
              FROM "user" c
              WHERE c.parent = u.id), 0)                        AS children,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'USD'), 0) AS usd,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'BTC'), 0) AS btc,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'ETH'), 0) AS eth
  FROM "user" u LEFT JOIN "user" s ON u.parent = s.id;

CREATE OR REPLACE VIEW sponsor AS
  WITH RECURSIVE r(id, "nick", type, parent, root, level) AS (
    SELECT
      id,
      nick,
      type,
      parent,
      id AS root,
      0  AS level
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.type,
      u.parent,
      r.root,
      r.level + 1 AS level
    FROM "user" u
      JOIN r ON u.id = r.parent
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    type,
    parent,
    root,
    level
  FROM r
  ORDER BY root, level, id;

CREATE OR REPLACE VIEW table_size AS
  WITH
      a AS (
        SELECT
          c.oid,
          nspname                               AS table_schema,
          relname                               AS "table_name",
          c.reltuples                           AS row_estimate,
          pg_total_relation_size(c.oid)         AS total_bytes,
          pg_indexes_size(c.oid)                AS index_bytes,
          pg_total_relation_size(reltoastrelid) AS toast_bytes
        FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
        WHERE relkind = 'r'
    ),
      b AS (
        SELECT
          *,
          total_bytes - index_bytes - COALESCE(toast_bytes, 0) AS table_bytes
        FROM a
    )
  SELECT
    "table_name",
    pg_size_pretty(total_bytes) AS total,
    pg_size_pretty(index_bytes) AS INDEX,
    pg_size_pretty(toast_bytes) AS toast,
    pg_size_pretty(table_bytes) AS "table",
    total_bytes
  FROM b
  WHERE table_schema = 'public';

CREATE OR REPLACE VIEW top_referrals AS
  SELECT
    replace(url, '/ref/', '') AS nick,
    count
  FROM (SELECT
          url,
          count(*)
        FROM visit
        WHERE url LIKE '/ref/%'
        GROUP BY url) t
  WHERE count > 1
  ORDER BY count DESC;

CREATE OR REPLACE VIEW consolidated_transfer AS
  SELECT
    id,
    type,
    status,
    CASE WHEN currency = 'USD'
      THEN amount
    ELSE ceil(amount * rate * 100) END AS amount
  FROM transfer t
    JOIN exchange x ON t.currency = x."to" AND x."from" = 'USD';

CREATE OR REPLACE VIEW consolidated_balance AS
  SELECT CASE WHEN currency = 'USD'
    THEN amount
         ELSE amount * rate END AS amount
  FROM balance b
    JOIN exchange x ON b.currency = x."to" AND x."from" = 'USD';

CREATE OR REPLACE VIEW statistics AS
  SELECT
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'payment' AND status = 'success')                        AS payment,
    coalesce((SELECT sum(amount)
              FROM consolidated_transfer
              WHERE type = 'payment' AND status =
                                         'pending'), 0)                    AS pending,
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'withdraw' AND status =
                                 'success')                                AS withdraw,
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'accrue' AND status =
                               'success')                                  AS accrue,
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'buy' AND status =
                            'success')                                     AS buy,
    (SELECT count(*)
     FROM "user")                                                          AS users,
    (SELECT count(*)
     FROM "user" ud
     WHERE ud.created > CURRENT_TIMESTAMP -
                        INTERVAL '1 day')                                  AS users_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP)               AS visitors,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS visitors_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS new_visitors_day,
    (SELECT count(*)
     FROM log
     WHERE entity = 'handshake' AND action = 'update'
           AND to_timestamp(id / (1000 * 1000 * 1000)) > CURRENT_TIMESTAMP -
                                                         INTERVAL '1 day') AS activities_day,
    (SELECT count(*)
     FROM visit
     WHERE url LIKE '/ref/%' AND visit.created > CURRENT_TIMESTAMP -
                                                 INTERVAL '1 day')         AS referral_day,
    (SELECT sum(amount)
     FROM consolidated_balance)                                            AS balance,
    NOW()                                                                  AS "time";

CREATE OR REPLACE VIEW program_deposit AS
  SELECT
    d.program,
    d.currency,
    p.quantity,
    p.name,
    d.range,
    p.accrue
  FROM program p
    JOIN deposit d ON p.id = d.program
  WHERE p.active;

CREATE OR REPLACE VIEW node_program AS
  SELECT
    n.id,
    n.program,
    p.range,
    p.currency,
    n."user",
    t.amount,
    p.accrue,
    n.created,
    p.quantity,
    p.name,
    t.created AS bought
  FROM node n
    JOIN program_deposit p ON n.program = p.program
    JOIN transfer t ON t.node = n.id AND t.type = 'buy' AND p.currency = t.currency;

CREATE OR REPLACE VIEW node_summary AS
  WITH a AS (
      SELECT
        node,
        sum(amount) AS profit,
        count(*)    AS accrues
      FROM transfer
      WHERE type = 'accrue'
      GROUP BY node
  )
  SELECT
    n.id,
    n.program,
    p.name,
    p.currency,
    n."user",
    b.amount,
    coalesce(a.profit, 0)                                                AS profit,
    coalesce(a.accrues, 0)                                               AS accrues,
    n.created,
    b.created                                                            AS bought,
    coalesce(l.created, n.created) + (p.quantity + 1) * INTERVAL '1 day' AS closing,
    CASE WHEN n.created > CURRENT_TIMESTAMP
      THEN 'reserved' :: NODE_STATUS
    WHEN l.id IS NOT NULL
      THEN 'closed' :: NODE_STATUS
    ELSE 'opened' :: NODE_STATUS END                                     AS status,
    n.created + (p.quantity + 1) * INTERVAL '1 day' < CURRENT_TIMESTAMP  AS closable
  FROM node n
    JOIN program_deposit p ON n.program = p.program
    JOIN transfer b ON b.node = n.id AND b.type = 'buy' AND p.currency = b.currency
    LEFT JOIN a ON a.node = n.id
    LEFT JOIN transfer l ON l.node = n.id AND l.type = 'loan';

CREATE OR REPLACE VIEW user_node_summary AS
  SELECT
    n.*,
    u.nick
  FROM node_summary n
    JOIN "user" u ON n."user" = u.id;

CREATE OR REPLACE VIEW last_limit AS
  SELECT
    program,
    currency,
    date,
    amount
  FROM "limit" l
  WHERE l.date = (SELECT max(ll.date)
                  FROM "limit" ll
                  WHERE ll.program = l.program AND ll.currency = l.currency);

CREATE OR REPLACE VIEW date_limit AS
  WITH a AS (SELECT date
             FROM "limit"
             GROUP BY date)
  SELECT
    a.date                      AS id,
    (SELECT array_to_json(array_agg(t))
     FROM (SELECT
             d.currency,
             d.program,
             d.amount
           FROM "limit" d
           WHERE
             d.date = a.date
           ORDER BY program) t) AS data
  FROM a;

CREATE OR REPLACE VIEW date_amount AS
  SELECT
    n.created :: DATE AS date,
    n.currency,
    n.program,
    sum(n.amount)     AS amount
  FROM node_program n
  GROUP BY n.created :: DATE, n.currency, n.program;

CREATE OR REPLACE VIEW date_amount_limited AS
  SELECT
    l.date,
    l.currency,
    l.program,
    coalesce(d.amount, 0) AS invested,
    l.amount              AS max
  FROM date_amount d RIGHT JOIN "limit" l ON d.date = l.date AND d.program = l.program AND d.currency = l.currency;

CREATE OR REPLACE VIEW program_deposit_limited AS
  SELECT
    p.*,
    coalesce(
        (SELECT sum(n.amount)
         FROM node_program n
         WHERE n.program = p.program AND n.currency = p.currency AND n.created :: DATE = current_date),
        0)   AS invested,
    coalesce(
        (SELECT amount
         FROM "limit" l
         WHERE l.program = p.program AND l.currency = p.currency AND l.date = current_date)
        --         (SELECT amount
        --          FROM last_limit ll
        --          WHERE ll.program = p.id AND ll.currency = p.currency)
        , 0) AS max
  FROM program_deposit p;

CREATE OR REPLACE VIEW future_deposit_limited AS
  SELECT
    d.date,
    d.program,
    d.currency,
    d.invested,
    d.max,
    p.range,
    p.name
  FROM date_amount_limited d
    JOIN program_deposit p ON d.currency = p.currency AND d.program = p.program
  WHERE d.date >= current_date;

CREATE OR REPLACE VIEW plan AS
  WITH t AS (
      SELECT
        number,
        n.id                                           AS node,
        n.user,
        n.currency,
        floor(n.amount * n.accrue + 0.01) :: BIGINT    AS amount,
        (SELECT n.created + number * INTERVAL '1 day') AS time,
        n.created                                      AS node_created,
        n.quantity,
        n.name
      FROM node_program n CROSS JOIN generate_series(1, n.quantity) number
  )
  SELECT
    "number",
    node,
    'accrue' :: "TRANSFER_TYPE"            AS type,
    "user",
    currency,
    amount,
    time,
    node_created,
    'Accrue {number}/{quantity} of {name}' AS text,
    json_build_object(
        'number', "number",
        'quantity', quantity,
        'income', amount * "number",
        'name', name
    )                                      AS vars
  FROM t
  UNION ALL
  SELECT
    n.quantity + 1                                  AS number,
    n.id                                            AS node,
    'loan' :: "TRANSFER_TYPE"                       AS type,
    n.user,
    n.currency,
    n.amount,
    n.created + (n.quantity + 1) * INTERVAL '1 day' AS time,
    n.created                                       AS node_created,
    'Refund of deposit {name}'                      AS text,
    json_build_object('name', n.name)               AS vars
  FROM node_program n;

CREATE OR REPLACE VIEW need AS
  SELECT
    p.node,
    'accrue' :: "TRANSFER_TYPE" AS type,
    p.user,
    p.currency,
    p.amount,
    p.node_created,
    p.number,
    p.text,
    p.vars
  FROM plan p
  WHERE p.time <= CURRENT_TIMESTAMP
        AND p.number >
            (SELECT count(*)
             FROM transfer t
             WHERE t.node = p.node AND type IN ('accrue', 'loan'));

CREATE OR REPLACE VIEW need_reward AS
  SELECT
    s.id                            AS "user",
    coalesce(ur.percent, w.percent) AS percent,
    coalesce(ur.level, s.level)     AS level,
    s.root
  FROM sponsor s
    JOIN reward w ON s.level = w.level
    LEFT JOIN user_reward ur ON s.root = ur."user" AND w.level = ur.level
  WHERE s.type <> 'special';

CREATE OR REPLACE VIEW need_deposit AS
  SELECT
    *,
    sum(amount)
    OVER (
      ORDER BY n.bought )
  FROM node_program n
  WHERE n.created IS NULL;

CREATE OR REPLACE VIEW turnover AS
  SELECT
    r.root        AS "user",
    b.currency,
    sum(b.amount) AS amount
  FROM transfer b
    LEFT JOIN transfer l ON b.node = l.node AND b.type = 'buy' AND l.type = 'loan'
    JOIN referral r ON r.id = b."from"
    JOIN reward w ON r.level = w.level
  GROUP BY r.root, b.currency;

CREATE OR REPLACE VIEW daily AS
  SELECT
    "to"          AS "user",
    currency,
    sum(b.amount) AS amount
  FROM transfer b
  WHERE CURRENT_TIMESTAMP - b.created < INTERVAL '1 day' AND type IN ('accrue', 'bonus')
  GROUP BY "to", b.currency;

CREATE OR REPLACE VIEW user_log AS
  SELECT
    l.id,
    l."user",
    u.nick,
    l.entity,
    l.action,
    l.ip,
    l.data
  FROM log l LEFT JOIN "user" u ON l."user" = u.id;

CREATE OR REPLACE VIEW future AS
  SELECT
    time :: DATE AS date,
    currency,
    sum(amount)  AS amount,
    count(*)
  FROM plan
  WHERE time >= CURRENT_TIMESTAMP
  GROUP BY time :: DATE, currency;

CREATE OR REPLACE VIEW future_rated AS
  SELECT
    date,
    currency,
    amount / rate AS amount,
    count
  FROM future f
    JOIN exchange x ON currency = x."from" AND currency = x."to";

CREATE OR REPLACE VIEW withdrawable AS
  SELECT
    t.id,
    (CASE WHEN b.amount - t.amount >= 0
      THEN 'success'
     ELSE 'canceled' END) AS will,
    t.amount,
    t.wallet,
    t.system,
    t.currency,
    t."from",
    u.nick                AS "from_nick",
    b.amount              AS balance,
    t.txid,
    b.amount - t.amount   AS remain,
    t.ip,
    t.text,
    t.vars,
    t.created
  FROM transfer t
    JOIN balance b ON t."from" = b."user" AND t.currency = b.currency
    JOIN "user" u ON t."from" = u.id
  WHERE t.type = 'withdraw' AND t.status = 'created' AND t.wallet IS NOT NULL
        AND t.txid IS NULL AND t.node IS NULL AND t.amount > 0 AND t.ip IS NOT NULL;

CREATE OR REPLACE VIEW withdraw AS
  SELECT
    id,
    status,
    "from",
    amount,
    currency,
    system,
    created
  FROM transfer
  WHERE type = 'withdraw'
  ORDER BY id DESC;

CREATE OR REPLACE VIEW special_reward AS
  SELECT
    u.id,
    coalesce(ur.level, r.level)     AS level,
    coalesce(ur.percent, r.percent) AS percent
  FROM "user" u
    CROSS JOIN reward r
    LEFT JOIN user_reward ur ON u.id = ur."user" AND r.level = ur.level;

CREATE OR REPLACE VIEW transfer_statistics AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW transfer_week AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 week' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW transfer_day AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 day' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW all_balance AS
  SELECT
    currency,
    sum(amount) AS amount
  FROM balance
  WHERE currency IS NOT NULL
  GROUP BY currency;

CREATE OR REPLACE VIEW future_json AS
  SELECT
    date as id,
    (SELECT json_build_object('amount', amount, 'count', count)
     FROM future eth
     WHERE eth.date = f.date AND eth.currency = 'ETH') as eth,
    (SELECT json_build_object('amount', amount, 'count', count)
     FROM future btc
     WHERE btc.date = f.date AND btc.currency = 'BTC') as btc,
    (SELECT json_build_object('amount', amount, 'count', count)
     FROM future usd
     WHERE usd.date = f.date AND usd.currency = 'USD') as usd
  FROM future f;
