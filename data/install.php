<?php

$config = require __DIR__ . '/../config/common.php';
$dsn = $config['components']['db']['dsn'];
$username = $config['components']['db']['username'];
$password = $config['components']['db']['password'];
$host = null;
$dbname = null;
preg_match('/host=([^;]+)/', $dsn, $host);
preg_match('/dbname=([^;]+)/', $dsn, $dbname);

if ($host && $dbname) {
    $host = $host[1];
    $dbname = $dbname[1];
}
else {
    echo 'Database name undefined';
    exit();
}

try {
    $postgres = new PDO("pgsql:host=$host;dbname=postgres");
    $postgres->exec("DROP DATABASE \"$dbname\" IF EXISTS");
    $postgres->exec("DROP USER \"$username\" IF EXISTS");
    $postgres->exec("CREATE USER \"$username\" PASSWORD '$password'");
    $postgres->exec("CREATE DATABASE \"$dbname\" OWNER \"$username\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

$pdo = new PDO($dsn, $username, $password);
$pdo->exec(file_get_contents(__DIR__ . '/tables.sql'));
$pdo->exec(file_get_contents(__DIR__ . '/views.sql'));
$pdo->exec(file_get_contents(__DIR__ . '/procedures.sql'));
