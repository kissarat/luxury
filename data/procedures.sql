CREATE OR REPLACE FUNCTION buy(user_id INT, _amount BIGINT, _currency CURRENCY, _created TIMESTAMP)
  RETURNS INT AS $$
DECLARE
  _program RECORD;
  _node_id INT;
  _nick    TEXT;
BEGIN
  SELECT *
  FROM future_deposit_limited p
  WHERE _amount <@ p.range AND currency = _currency AND date = _created :: DATE AND invested + _amount <= p.max
  INTO _program;

  IF _program IS NULL
  THEN
    RETURN -404;
  END IF;

  IF (SELECT count(*) > 0
      FROM transfer
      WHERE type = 'withdraw' AND status = 'created' AND "from" = user_id
      LIMIT 1)
  THEN
    RETURN -409;
  END IF;

  IF coalesce((SELECT amount - _amount < 0
               FROM balance
               WHERE "user" = user_id AND currency = _currency), TRUE)
  THEN
    RETURN -402;
  END IF;

  SELECT nick
  FROM "user"
  WHERE id = user_id
  INTO _nick;

  INSERT INTO node ("user", "program", created)
  VALUES (user_id, _program.program,
          CASE WHEN _created :: DATE = CURRENT_DATE
            THEN _created
          ELSE _created :: DATE END)
  RETURNING id
    INTO _node_id;

  _created = CURRENT_TIMESTAMP;

  INSERT INTO transfer ("type", "status", "from", amount, created, node, "text", vars, currency)
  VALUES (
    'buy',
    'success',
    user_id,
    _amount,
    _created,
    _node_id,
    'Opening {name}',
    json_build_object('name', _program.name),
    _currency
  );

  INSERT INTO transfer ("type", "status", "to", amount, currency, created, node, "text", vars)
    SELECT
      'bonus',
      'success',
      r."user",
      floor(r.percent * _amount),
      _currency,
      _created,
      _node_id,
      'Bonus from {from_nick} for opening {name} at {level} level',
      json_build_object(
          'name', _program.name,
          'from', user_id,
          'from_nick', _nick,
          'level', level
      )
    FROM need_reward r
    WHERE r.root = user_id
    ORDER BY "level";

  RETURN _node_id;
END
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION accrue()
  RETURNS VOID AS $$
DECLARE
  _time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
BEGIN
  INSERT INTO transfer ("type", "status", "to", amount, created, node, currency, "text", vars)
    SELECT
      "type",
      'success',
      "user",
      amount,
      _time,
      node,
      currency,
      "text",
      vars
    FROM need
    ORDER BY node_created, number;
END
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION randomize()
  RETURNS VOID AS $$
BEGIN
  UPDATE transfer
  SET created = to_timestamp(extract(EPOCH FROM NOW()) - random() * 3600 * 24);
END
$$ LANGUAGE plpgsql VOLATILE;

CREATE TYPE HISTOGRAM AS (time TIMESTAMP, amount INT8);

CREATE OR REPLACE FUNCTION histogram(
  start     TIMESTAMP,
  step      INTERVAL,
  _currency CURRENCY DEFAULT 'USD',
  _type     "TRANSFER_TYPE" DEFAULT 'accrue',
  _to       INT DEFAULT NULL,
  _from     INT DEFAULT NULL,
  stop      TIMESTAMP DEFAULT NOW()
)
  RETURNS SETOF HISTOGRAM AS $$
BEGIN
  RETURN QUERY
  SELECT
    s AS time,
    sum(coalesce(amount, 0))
  FROM generate_series(start, stop, step) s LEFT JOIN transfer t
      ON
        t.created >= s
        AND t.created < s + step
        AND currency = _currency
        AND type = _type
        AND (_from IS NULL OR "from" = _from)
        AND (_to IS NULL OR "to" = _to)
  GROUP BY s;
END
$$ LANGUAGE plpgsql IMMUTABLE;

CREATE OR REPLACE FUNCTION internal(_from INT, _to INT, _amount INT8, _currency CURRENCY, _ip INET)
  RETURNS INT AS $$
DECLARE
  _id INT;
BEGIN
  IF coalesce((SELECT amount - _amount
          FROM balance
          WHERE "user" = _from AND currency = _currency
          LIMIT 1), -1) < 0
  THEN
    RETURN -402;
  END IF;

  IF (SELECT count(*) <= 0
      FROM node
      WHERE "user" = _from
      LIMIT 1)
  THEN
    RETURN -403;
  END IF;

  INSERT INTO transfer ("from", "to", amount, currency, type, status, ip)
  VALUES (_from, _to, _amount, _currency, 'internal', 'success', _ip)
  RETURNING id INTO _id;

  RETURN _id;
END
$$ LANGUAGE plpgsql VOLATILE;
