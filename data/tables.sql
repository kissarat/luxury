CREATE TYPE LANGUAGE AS ENUM (
  'en',
  'ru',
  'uk',
  'de',
  'fr',
  'es',
  'it'
);

CREATE TYPE TOKEN_TYPE AS ENUM (
  'server',
  'browser',
  'app',
  'code'
);

CREATE TYPE USER_TYPE AS ENUM ('new', 'native', 'blocked', 'special', 'leader');

CREATE TYPE TRANSLATION_CATEGORY AS ENUM ('app');

CREATE TABLE source_message (
  id       INTEGER                                                    NOT NULL,
  category TRANSLATION_CATEGORY DEFAULT 'app' :: TRANSLATION_CATEGORY NOT NULL,
  message  TEXT                                                       NOT NULL
);

CREATE TABLE target_message (
  id          INTEGER                           NOT NULL,
  language    LANGUAGE DEFAULT 'ru' :: LANGUAGE NOT NULL,
  translation TEXT                              NOT NULL
);

CREATE TABLE "user" (
  id         SERIAL PRIMARY KEY                              NOT NULL,
  nick       CHARACTER VARYING(24)                           NOT NULL,
  email      CHARACTER VARYING(48)                           NOT NULL,
  type       USER_TYPE DEFAULT 'new' :: USER_TYPE            NOT NULL,
  parent     INTEGER,
  secret     CHARACTER(60),
  admin      BOOLEAN DEFAULT FALSE                           NOT NULL,
  surname    CHARACTER VARYING(48),
  forename   CHARACTER VARYING(48),
  avatar     CHARACTER VARYING(192),
  skype      CHARACTER VARYING(32),
  phone      CHARACTER VARYING(32),
  telegram   CHARACTER VARYING(32),
  country    CHARACTER VARYING(64),
  settlement CHARACTER VARYING(128),
  address    CHARACTER VARYING(128),
  perfect    CHARACTER VARYING(10),
  blockio    CHARACTER VARYING(40),
  ethereum   CHARACTER VARYING(42),
  geo        BOOLEAN                                         NOT NULL DEFAULT FALSE,
  created    TIMESTAMP(0) WITHOUT TIME ZONE                           DEFAULT now(),
  "time"     TIMESTAMP(0) WITHOUT TIME ZONE
);


CREATE TABLE token (
  id        CHARACTER VARYING(192)                                                                      NOT NULL,
  "user"    INTEGER,
  type      TOKEN_TYPE                                                                                  NOT NULL DEFAULT 'browser',
  expires   TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT '2030-01-01 00:00:00' :: TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  data      JSON,
  ip        INET,
  handshake TIMESTAMP,
  name      CHARACTER VARYING(240),
  created   TIMESTAMP(0) WITHOUT TIME ZONE                                                                       DEFAULT now(),
  "time"    TIMESTAMP(0) WITHOUT TIME ZONE
);

CREATE TABLE session (
  id     CHAR(40) NOT NULL PRIMARY KEY,
  expire INTEGER,
  data   BYTEA
);

CREATE TABLE cache (
  id     CHAR(128) NOT NULL PRIMARY KEY,
  expire INT,
  data   BYTEA
);

CREATE TABLE "visit" (
  "id"      SERIAL PRIMARY KEY,
  "token"   VARCHAR(240),
  "url"     VARCHAR(240) NOT NULL,
  "spend"   INT,
  ip        INET,
  agent     VARCHAR(240),
  "created" TIMESTAMP DEFAULT current_timestamp
);

CREATE TYPE "TRANSFER_TYPE" AS ENUM ('internal', 'payment', 'withdraw', 'accrue', 'buy', 'earn', 'support',
  'write-off', 'bonus', 'loan');
CREATE TYPE "TRANSFER_STATUS" AS ENUM ('created', 'success', 'fail', 'canceled', 'pending', 'virtual');
CREATE TYPE "TRANSFER_SYSTEM" AS ENUM ('internal', 'bitcoin', 'payeer', 'perfect', 'ethereum');
-- CREATE TYPE CURRENCY AS ENUM ('USD', 'EUR', 'GBP', 'JPY', 'CNY', 'RUB', 'PLN', 'UAH', 'KZT', 'BTC', 'ETH');
CREATE TYPE CURRENCY AS ENUM ('USD', 'BTC', 'ETH');

CREATE TABLE transfer (
  id       SERIAL PRIMARY KEY,
  "from"   INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"     INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  amount   INT8              NOT NULL,
  "type"   "TRANSFER_TYPE"   NOT NULL,
  "status" "TRANSFER_STATUS" NOT NULL,
  node     INT,
  text     VARCHAR(192),
  vars     JSON,
  wallet   VARCHAR(64),
  txid     VARCHAR(130),
  currency CURRENCY,
  system   "TRANSFER_SYSTEM",
  ip       INET,
  created  TIMESTAMP         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time     TIMESTAMP
);

CREATE TABLE exchange (
  "from" CURRENCY  NOT NULL,
  "to"   CURRENCY  NOT NULL,
  "rate" FLOAT     NOT NULL DEFAULT 1 CHECK ("rate" > 0),
  time   TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:00',
  UNIQUE ("from", "to")
);

INSERT INTO exchange ("from", "to", "rate")
VALUES
  ('USD', 'USD', 100),
  ('BTC', 'BTC', 1000 * 1000),
  ('ETH', 'ETH', 100 * 1000);

INSERT INTO exchange ("from", "to", "rate")
VALUES
  ('USD', 'BTC', 3000.0),
  ('USD', 'ETH', 300.0),
  ('BTC', 'USD', 1.0 / 3000),
  ('BTC', 'ETH', 1.0 / 10),
  ('ETH', 'BTC', 10.0),
  ('ETH', 'USD', 1.0 / 300);

CREATE TABLE program (
  id       SMALLINT PRIMARY KEY,
  name     VARCHAR(32) NOT NULL,
  quantity SMALLINT    NOT NULL,
  accrue   FLOAT4      NOT NULL,
  active   BOOLEAN     NOT NULL DEFAULT TRUE
);

INSERT INTO program (id, name, quantity, accrue) VALUES
  (2, 'Start', 25, 0.01),
  (3, 'Normal', 30, 0.012),
  (4, 'Medium', 35, 0.0135),
  (5, 'VIP', 50, 0.015);

CREATE TABLE deposit (
  program  SMALLINT  NOT NULL REFERENCES program (id),
  currency CURRENCY  NOT NULL,
  range    INT8RANGE NOT NULL,
  UNIQUE (program, currency)
);

INSERT INTO deposit (program, range, currency) VALUES
  (2, '[2500,50000)', 'USD'),
  (3, '[50000,250000)', 'USD'),
  (4, '[250000,750000)', 'USD'),
  (5, '[750000,5000000)', 'USD'),
  (2, '[5000,76000)', 'BTC'),
  (3, '[76000,380000)', 'BTC'),
  (4, '[380000,1150000)', 'BTC'),
  (5, '[1150000,5000000000)', 'BTC'),
  (2, '[10000,180000)', 'ETH'),
  (3, '[180000,840000)', 'ETH'),
  (4, '[840000,2500000)', 'ETH'),
  (5, '[2500000,500000000)', 'ETH');

CREATE TABLE "limit" (
  program  SMALLINT NOT NULL REFERENCES program (id),
  currency CURRENCY NOT NULL,
  date     DATE     NOT NULL,
  amount   INT8     NOT NULL,
  UNIQUE (program, currency, date)
);

INSERT INTO "limit" (program, amount, currency, date) VALUES
  (2, 50000, 'USD', CURRENT_DATE),
  (3, 250000, 'USD', CURRENT_DATE),
  (4, 750000, 'USD', CURRENT_DATE),
  (5, 5000000, 'USD', CURRENT_DATE),
  (2, 125000, 'BTC', CURRENT_DATE),
  (3, 625000, 'BTC', CURRENT_DATE),
  (4, 1875000, 'BTC', CURRENT_DATE),
  (5, 10000000, 'BTC', CURRENT_DATE),
  (2, 180000, 'ETH', CURRENT_DATE),
  (3, 840000, 'ETH', CURRENT_DATE),
  (4, 2500000, 'ETH', CURRENT_DATE),
  (5, 50000000, 'ETH', CURRENT_DATE);

CREATE TABLE reward (
  level   SMALLINT PRIMARY KEY,
  percent FLOAT NOT NULL
);

INSERT INTO reward VALUES
  (1, 0.10),
  (2, 0.05),
  (3, 0.02);

CREATE TABLE user_reward (
  "user"  INT REFERENCES "user" (id),
  level   SMALLINT NOT NULL,
  percent FLOAT    NOT NULL
);

CREATE TABLE node (
  id      SERIAL PRIMARY KEY,
  "user"  INT NOT NULL REFERENCES "user" (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  program INT NOT NULL REFERENCES program (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  created TIMESTAMP,
  time    TIMESTAMP
);

CREATE TYPE REQUEST_METHOD AS ENUM ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT');

CREATE TABLE request (
  id     BIGSERIAL PRIMARY KEY,
  time   TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip     INET,
  url    VARCHAR(4096)  NOT NULL,
  status SMALLINT,
  method REQUEST_METHOD NOT NULL,
  type   VARCHAR(64),
  agent  VARCHAR(512),
  "data" TEXT,
  guid   CHAR(32),
  token  VARCHAR(240),
  "user" INT
);

CREATE TABLE log (
  id     BIGINT PRIMARY KEY,
  entity VARCHAR(16) NOT NULL,
  action VARCHAR(32) NOT NULL,
  ip     INET,
  "user" INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  token  CHARACTER VARYING(192),
  data   JSON
);

CREATE TABLE coordinate (
  id                SERIAL PRIMARY KEY,
  time              TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "user"            INT,
  ip                INET,
  latitude          FLOAT8    NOT NULL,
  longitude         FLOAT8    NOT NULL,
  altitude          FLOAT8,
  accuracy          SMALLINT,
  altitude_accuracy SMALLINT,
  heading           FLOAT8,
  speed             FLOAT8
);

CREATE TABLE wallet (
  id      VARCHAR(64) PRIMARY KEY,
  system  "TRANSFER_SYSTEM" NOT NULL,
  name    VARCHAR(48),
  amount  INT8              NOT NULL,
  "user"  INT REFERENCES "user" (id),
  expires TIMESTAMP,
  time    TIMESTAMP         NOT NULL,
  created TIMESTAMP         NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE NODE_STATUS AS ENUM ('reserved', 'opened', 'closed');

CREATE TABLE email (
  id      BIGSERIAL PRIMARY KEY,
  "to"    VARCHAR(192),
  subject VARCHAR(192),
  content TEXT,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sent    TIMESTAMP
);

CREATE TABLE setting (
  key   VARCHAR(64),
  value TEXT
);

INSERT INTO setting VALUES
  ('eth.block.number', '0x1c992a');

CREATE TABLE ip (
  id      INET PRIMARY KEY,
  text    VARCHAR(80),
  editor  INT       NOT NULL,
  ip      INET      NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time    TIMESTAMP
);

INSERT INTO ip (id, text, editor, ip, created, time) VALUES
  ('127.0.0.1', 'localhost', 7, '185.53.170.57', '2017-10-21 18:25:58.592000', NULL),
  ('185.53.170.57', 'rootwelt', 7, '185.53.170.57', '2017-10-21 18:24:58.823879', NULL),
  ('188.163.32.208', 'viktor', 7, '185.53.170.57', '2017-10-21 18:26:27.925000', NULL),
  ('188.115.169.61', 'rostik', 7, '185.53.170.57', '2017-10-21 18:26:27.925000', NULL);


CREATE TYPE BLOCKCHAIN_NETWORK AS ENUM ('BTC', 'BTCTEST');

CREATE TABLE blockchain_transaction (
  id            CHAR(64)           NOT NULL,
  amount        DECIMAL(15, 8)     NOT NULL,
  network       BLOCKCHAIN_NETWORK NOT NULL,
  fee           DECIMAL(15, 8)     NOT NULL,
  created       TIMESTAMP          NOT NULL,
  confirmations INT,
  time          TIMESTAMP
);
