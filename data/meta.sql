CREATE TABLE meta.attribute (
    name name,
    attributes json
);

COPY meta.attribute (name, attributes) FROM stdin;
balance	{ "id" : "integer", "balance" : "integer" }
cache	{ "id" : "character(128)", "expire" : "integer", "data" : "bytea" }
identity	{ "email" : "character varying(48)", "expires" : "timestamp(0) without time zone", "type" : "token_type", "user" : "json", "ip" : "inet", "id" : "character varying(192)", "created" : "timestamp(0) without time zone", "time" : "timestamp(0) without time zone", "parent" : "integer", "avatar" : "character varying(192)", "admin" : "boolean", "nick" : "character varying(24)" }
informer	{ "withdraw" : "bigint", "parent" : "json", "email" : "character varying(48)", "avatar" : "character varying(192)", "skype" : "character varying(32)", "forename" : "character varying(48)", "surname" : "character varying(48)", "type" : "user_type", "nick" : "character varying(24)", "day" : "double precision", "turnover" : "bigint", "node" : "bigint", "referral" : "bigint", "invited" : "bigint", "visit" : "bigint", "earn" : "bigint", "accrue" : "bigint", "payment" : "bigint", "balance" : "integer", "id" : "integer" }
log	{ "data" : "json", "user" : "integer", "ip" : "inet", "id" : "bigint", "entity" : "character varying(16)", "action" : "character varying(32)" }
migration	{ "version" : "character varying(180)", "apply_time" : "integer" }
need	{ "id" : "integer", "user" : "integer", "amount" : "double precision" }
node	{ "id" : "integer", "time" : "timestamp without time zone", "created" : "timestamp without time zone", "amount" : "integer", "program" : "integer", "user" : "integer" }
program	{ "id" : "smallint", "name" : "character varying(32)", "active" : "boolean", "quantity" : "smallint", "percent" : "double precision", "price" : "int4range", "period" : "interval" }
referral	{ "level" : "integer", "nick" : "character varying(24)", "parent" : "integer", "email" : "character varying(48)", "root" : "integer", "id" : "integer", "forename" : "character varying(48)", "surname" : "character varying(48)", "skype" : "character varying(32)", "avatar" : "character varying(192)", "created" : "timestamp(0) without time zone" }
referral_sponsor	{ "level" : "integer", "root" : "integer", "email" : "character varying(48)", "parent" : "integer", "sponsor" : "character varying(24)", "created" : "timestamp(0) without time zone", "avatar" : "character varying(192)", "skype" : "character varying(32)", "surname" : "character varying(48)", "nick" : "character varying(24)", "id" : "integer", "forename" : "character varying(48)" }
request	{ "time" : "timestamp without time zone", "url" : "character varying(4096)", "method" : "request_method", "type" : "character varying(64)", "token" : "character varying(240)", "user" : "integer", "agent" : "character varying(512)", "data" : "text", "guid" : "character(32)", "id" : "bigint", "ip" : "inet" }
reward	{ "level" : "smallint", "percent" : "double precision" }
session	{ "expire" : "integer", "data" : "bytea", "id" : "character(40)" }
source_message	{ "message" : "text", "category" : "translation_category", "id" : "integer" }
sponsor	{ "parent" : "integer", "level" : "integer", "root" : "integer", "type" : "user_type", "nick" : "character varying(24)", "id" : "integer" }
sponsor_reward	{ "parent" : "integer", "type" : "user_type", "nick" : "character varying(24)", "id" : "integer", "percent" : "double precision", "level" : "integer", "root" : "integer" }
statistics	{ "buy" : "bigint", "visitors" : "bigint", "visitors_day" : "bigint", "new_visitors_day" : "bigint", "activities_day" : "bigint", "balance" : "bigint", "referral_day" : "bigint", "users_day" : "bigint", "payment" : "bigint", "accrue" : "bigint", "withdraw" : "bigint", "pending" : "bigint", "users" : "bigint" }
target_message	{ "translation" : "text", "language" : "language", "id" : "integer" }
token	{ "handshake" : "timestamp without time zone", "time" : "timestamp(0) without time zone", "id" : "character varying(192)", "user" : "integer", "type" : "token_type", "expires" : "timestamp(0) without time zone", "data" : "json", "ip" : "inet", "name" : "character varying(240)", "created" : "timestamp(0) without time zone" }
token_user	{ "id" : "character varying(192)", "created" : "timestamp(0) without time zone", "user" : "json", "parent" : "integer", "avatar" : "character varying(192)", "admin" : "boolean", "nick" : "character varying(24)", "email" : "character varying(48)", "expires" : "timestamp(0) without time zone", "type" : "token_type", "time" : "timestamp(0) without time zone", "ip" : "inet" }
top_referrals	{ "nick" : "text", "count" : "bigint" }
transfer	{ "text" : "character varying(192)", "vars" : "json", "wallet" : "character varying(64)", "txid" : "character varying(64)", "ip" : "inet", "created" : "timestamp without time zone", "time" : "timestamp without time zone", "type" : "transfer_type", "amount" : "integer", "to" : "integer", "from" : "integer", "id" : "integer", "status" : "transfer_status", "node" : "integer" }
transfer_user	{ "to_nick" : "character varying(24)", "from_nick" : "character varying(24)", "txid" : "character varying(64)", "to" : "integer", "from" : "integer", "ip" : "inet", "wallet" : "character varying(64)", "status" : "transfer_status", "type" : "transfer_type", "amount" : "integer", "time" : "timestamp without time zone", "created" : "timestamp without time zone", "id" : "integer" }
user	{ "avatar" : "character varying(192)", "perfect" : "character varying(10)", "time" : "timestamp(0) without time zone", "created" : "timestamp(0) without time zone", "secret" : "character(60)", "admin" : "boolean", "surname" : "character varying(48)", "forename" : "character varying(48)", "id" : "integer", "skype" : "character varying(32)", "parent" : "integer", "type" : "user_type", "email" : "character varying(48)", "nick" : "character varying(24)" }
user_about	{ "surname" : "character varying(48)", "email" : "character varying(48)", "skype" : "character varying(32)", "admin" : "boolean", "nick" : "character varying(24)", "type" : "user_type", "id" : "integer", "forename" : "character varying(48)" }
visit	{ "ip" : "inet", "agent" : "character varying(240)", "created" : "timestamp without time zone", "id" : "integer", "token" : "character varying(240)", "url" : "character varying(240)", "spend" : "integer" }
\.
