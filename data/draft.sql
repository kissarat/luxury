SELECT sum(amount)
FROM consolidated_transfer
WHERE type = 'payment' AND status = 'success';

SELECT
  currency,
  sum(amount) / x.rate
FROM transfer t
  JOIN exchange x ON t.currency = x."from" AND t.currency = x."to"
WHERE type = 'payment'
GROUP BY currency, x.rate;

SELECT
  type,
  status,
  currency,
  system
FROM transfer
WHERE type IN ('payment', 'withdraw') AND status = 'success';

SELECT *
FROM future;

SELECT *
FROM transfer
WHERE type = 'buy' AND status = 'success' AND currency = 'USD'
ORDER BY amount DESC;

SELECT u.nick, n.*
FROM node n JOIN "user" u ON n."user" = u.id
WHERE n.id NOT IN (SELECT node
                   FROM transfer
                   WHERE node IS NOT NULL
                   GROUP BY node);

SELECT * FROM balance WHERE balance."user" < 10;
SELECT * FROM transfer WHERE type = 'withdraw' AND status = 'success' AND currency = 'BTC';
SELECT geo FROM identity WHERE identity."user" = 4;

SELECT
  t.id,
  (CASE WHEN b.amount - t.amount >= 0
    THEN 'success'
   ELSE 'canceled' END) AS will,
  t.amount,
  t.wallet,
  t."from",
  u.nick                AS "from_nick",
  b.amount              AS balance,
  t.txid,
  b.amount - t.amount   AS remain,
  t.ip,
  t.text,
  t.vars,
  t.system,
  t.created
FROM transfer t
  JOIN balance b ON t."from" = b."user" AND t.currency = b.currency
  JOIN "user" u ON t."from" = u.id
WHERE t.type = 'withdraw' AND t.status = 'created';

SELECT * FROM balance WHERE balance."user" = 4;
SELECT * FROM transfer WHERE "from" = 4 ;
SELECT * FROM transfer WHERE status = 'created';

SELECT id, nick, geo, surname, forename, skype, telegram FROM "user" WHERE skype IS NOT NULL OR telegram IS NOT NULL ORDER BY id;
UPDATE "user" SET geo = true WHERE nick in ('mviktor', 'klikun') OR admin OR nick like 'test%' OR id <= 16;
ALTER TABLE coordinate ADD COLUMN ip INET;

SELECT id, nick FROM "user" WHERE nick = 'klikun';
SELECT * FROM log WHERE "user" = 94;

UPDATE transfer SET system = 'perfect' WHERE id IN (SELECT id FROM withdraw WHERE status = 'created' AND currency = 'USD');
UPDATE transfer SET system = 'blockio' WHERE id IN (SELECT id FROM withdraw WHERE status = 'created' AND currency = 'BTC');

SELECT * FROM "user" WHERE nick = 'honestinvestor';

SELECT
  s.id AS "user",
  coalesce(ur.percent, w.percent) as percent,
  coalesce(ur.level, s.level) as level,
  s.root
FROM sponsor s
  JOIN reward w ON s.level = w.level
  LEFT JOIN user_reward ur ON s.root = ur."user" AND w.level = ur.level
WHERE root = 124;

SELECT * FROM referral_sponsor WHERE root = 20;
