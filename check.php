<?php

$config = json_decode(file_get_contents(__DIR__ . '/check-config.json'));

foreach ($config->urls as $hostname => $url) {
    @file_get_contents($url);
    $available = false;
    foreach ($http_response_header as $header) {
        if ('HTTP/1.1 200 OK' === $header) {
            $available = true;
            break;
        }
    }

    if (!$available) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://api.telegram.org/bot$config->token/sendMessage");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, [
            'chat_id' => -1001140453648,
            'text' => $hostname
        ]);
        curl_exec($curl);
    }
}
