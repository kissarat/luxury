<?php

namespace app\console\migrations;

use app\console\Migration;

class m170523_023506_user extends Migration {
    public function safeUp() {
        $this->createTable('cache', [
            'id' => $this->char(128)->notNull()->unique(),
            'expire' => $this->integer(),
            'data' => 'BYTEA'
        ]);
        $this->addPrimaryKey('cache_pkey', 'cache', 'id');

        $this->addEnum('translation_category', ['app']);
        $this->createTable('source_message', [
            'id' => $this->primaryKey(),
            'category' => "translation_category NOT NULL DEFAULT 'app'",
            'message' => $this->text()->notNull()
        ]);
        $this->addEnum('language', ['en', 'ru', 'uk', 'de', 'fr', 'es', 'it']);
        $this->createTable('target_message', [
            'id' => $this->integer(),
            'language' => "language NOT NULL DEFAULT 'ru'",
            'translation' => $this->text()->notNull()
        ]);
        $this->addPrimaryKey('target_message_pkey', 'target_message', ['id', 'language']);

        $this->addEnum('user_type', ['new', 'native', 'special']);
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'nick' => $this->string(24)->unique()->notNull(),
            'email' => $this->string(48)->unique()->notNull(),
            'type' => "user_type NOT NULL DEFAULT 'new'",
            'parent' => $this->integer('parent'),
            'secret' => $this->char(60),
            'admin' => $this->boolean()->notNull()->defaultValue(false),
            'surname' => $this->string(48),
            'forename' => $this->string(48),
            'avatar' => $this->string(192),
            'skype' => $this->string(32),
            'perfect' => $this->string(10),
        ]);
        $this->addTimestamps('user');

        $this->addEnum('token_type', ['server', 'browser', 'app', 'code']);
        $this->createTable('token', [
            'id' => 'VARCHAR(192) PRIMARY KEY',
            'user' => $this->integer(),
            'type' => 'token_type NOT NULL',
            'expires' => $this->timestamp()->notNull()->defaultValue('2030-01-01'),
            'data' => 'JSON',
            'ip' => 'INET',
            'name' => $this->string(240),
        ]);
        $this->addTimestamps('token');

        $this->addReferences([
            'target_message' => ['id' => ['source_message' => 'id']],
            'user' => ['parent' => ['user' => 'id']],
            'token' => ['user' => ['user' => 'id']],
        ]);
        return true;
    }

    public function safeDown() {
        $this->dropTable('token');
        $this->dropType('token_type');
        $this->dropTable('user');
        $this->dropType('user_type');
        $this->dropTable('target_message');
        $this->dropType('language');
        $this->dropTable('source_message');
        $this->dropType('translation_category');
        $this->dropTable('cache');
        return true;
    }
}
