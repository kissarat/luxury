<?php
defined('ROOT') or define('ROOT', __DIR__ . '/..');

$config = require ROOT . '/config/console.php';
define('VENDOR', ROOT . '/vendor');

require VENDOR . '/yiisoft/yii2/Yii.php';
require VENDOR . '/autoload.php';

$app = new yii\console\Application($config);
$app->setVendorPath(VENDOR);
