<?php

namespace app\console;

class Migration extends \yii\db\Migration {
    protected function addDefaultForeignKey(string $table, string $column, string $refTable, string $refColumn) {
        $this->addForeignKey("${table}_${column}_fkey", $table, $column,
            $refTable, $refColumn, 'cascade', 'cascade');
    }

    protected function addReferences($references) {
        foreach ($references as $table => $columns) {
            foreach ($columns as $column => $ref) {
                foreach ($ref as $refTable => $refColumn) {
                    $this->addDefaultForeignKey($table, $column, $refTable, $refColumn);
                }
            }
        }
    }

    protected function addConstraint($table, $name, $expression) {
        return $this->execute(`ALTER TABLE "$table" ADD CONSTRAINT "$name" $expression`);
    }

    protected function addTimestamps(string $table) {
        $this->addColumn($table, 'created', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn($table, 'time', $this->timestamp());
    }

    protected function addEnum($name, $values) {
        $values = array_map(function ($value) {
            return "'$value'";
        }, $values);
        $values = implode(', ', $values);
        return $this->execute("CREATE TYPE \"$name\" AS ENUM ($values)");
    }

    protected function dropType($name) {
        return $this->execute("DROP TYPE \"$name\"");
    }
}
