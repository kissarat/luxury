<?php

namespace app\console\controllers;


use app\helpers\Utils;
use Yii;
use yii\console\Controller;

class LocaleController extends Controller
{
    protected function writeScript($locale, $messages) {
        $json = json_encode($messages, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        $script = "const _ICO_messages = $json; if ('function' === typeof ICO_setLocale) {ICO_setLocale(_ICO_messages)}";
//        file_put_contents(Yii::getAlias("@app/web/locale/$locale.json"), $json);
        file_put_contents(Yii::getAlias("@app/web/locale/$locale.js"), $script);
    }

    public function actionGenerateScript() {
        $root = Yii::getAlias("@app/web/locale");
        if (!file_exists($root)) {
            mkdir($root);
        }
        foreach (['de', 'it', 'ru'] as $locale) {
            $this->writeScript($locale, Utils::getLocale($locale));
        }
        Utils::generateWidget();
    }

    protected function getStrings($dir) {
        $handle = opendir($dir);
        $strings = [];
        while (false !== ($entry = readdir($handle))) {
            if ('.' === $entry[0]) {
                continue;
            }
            $filename = $dir . '/' . $entry;
            if (is_dir($filename)) {
                $new = $this->getStrings($filename);
                if (count($new) > 0) {
                    $strings = array_merge($strings, $new);
                }
            }
            else {
                $subject = file_get_contents($filename);
                $matches = [];
                preg_match_all("/'app'\\s*,\\s*.([^'а-я]+)/m", $subject, $matches);
                for($i = 0; $i < count($matches[1]); $i++) {
                    $strings[] = $matches[1][$i];
                }

            }
        }
        return $strings;
    }

    public function actionStrings($dir) {
        $exiting = Utils::getLocale('ru');
        $strings = $this->getStrings($dir);
        $strings = array_diff($strings, $exiting);
        sort($strings, SORT_ASC);
        $array = [];
        foreach($strings as $string) {
            $array[$string] = $string;
        }
        foreach ($array as $string) {
            echo "$string; $string\n";
        }
    }
}
