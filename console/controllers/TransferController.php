<?php

namespace app\console\controllers;


use app\helpers\SQL;
use app\models\Limit;
use app\models\ProgramDeposit;
use app\models\User;
use Yii;
use yii\console\Controller;

class TransferController extends Controller {
    public function actionInvoice($quantity) {
        $types = array_flip(['payment', 'withdraw']);
        $balances = array_flip(User::find()->select('id')->column());
        $currencies = array_flip(['USD', 'BTC', 'ETH']);
        $invoices = [];
        for ($i = 0; $i < $quantity; $i++) {
            $id = array_rand($balances);
            $type = array_rand($types);
            $amount = rand(1, 10 * 1000 * 100);
            $invoices[] = [
                'withdraw' === $type ? $id : null,
                'payment' === $type ? $id : null,
                $amount,
                $type,
                'success',
                array_rand($currencies)
            ];
        }
        $columns = ['from', 'to', 'amount', 'type', 'status', 'currency'];
        Yii::$app->db
            ->createCommand()
            ->batchInsert('transfer', $columns, $invoices)
            ->execute();
        SQL::queryAll('INSERT INTO transfer("to", type, status, amount, currency)  '
            . "SELECT \"user\", 'payment', 'success', -amount * 2, currency FROM balance WHERE amount < 0");
    }

    public function actionLimit($date) {
        $programs = ProgramDeposit::find()->all();
        foreach ($programs as $program) {
            $limit = new Limit([
                'program' => $program->program,
                'currency' => $program->currency,
                'date' => $date,
                'amount' => $program->getMax() + 1]);
//        var_dump($limit);
            $limit->save();
        }
    }
}
