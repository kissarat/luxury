<?php

namespace app\console\controllers;


use app\models\Log;
use Yii;
use yii\console\Controller;

class EthereumController extends Controller
{
    public function actionAccounts() {
        $accounts = Yii::$app->ethereum->getListAccounts();
        Log::log('ethereum', 'accounts', ['accounts' => $accounts]);
        echo json_encode($accounts);
    }

    public function actionBalance($address) {
        $balance = Yii::$app->ethereum->getBalance($address);
        Log::log('ethereum', 'balance', ['balance' => $balance]);
        echo $balance;
    }

    public function actionBalances() {
        $addresses = Yii::$app->ethereum->getListAccounts();
        foreach($addresses as $address) {
            echo $address . ' ' . Yii::$app->ethereum->getBalance($address) . "\n";
        }
    }
}
