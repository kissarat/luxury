<?php

namespace app\console\controllers;


use app\helpers\Utils;
use Yii;
use yii\console\Controller;

class LogController extends Controller {
    public function actionWidget() {
        Utils::generateWidget();
    }

    private function insertLog($rows) {
        $inserts = [];
        foreach($rows as $row) {
            for($i = 0; $i < count($row); $i++) {
                $row[$i] = '-' === $row[$i] ? null : $row[$i];
            }
            $inserts[] = array_slice($row, 0, 5);
        }
        Yii::$app->db
            ->createCommand()
            ->batchInsert('request', ['time', 'ip', 'status', 'method', 'url'], $inserts)
            ->execute();
    }

    public function actionLoad($logfile) {
        $h = fopen($logfile, "r");
        if ($h) {
            $rows = [];
            for ($i = 0; ($line = fgets($h)) !== false; $i++) {
                $row = preg_split('/\s+/', $line);
                $rows[] = $row;
                if ($i >= 1000) {
                    $this->insertLog($rows);
                    $rows = [];
                }
            }
            if (count($rows) > 0) {
                $this->insertLog($rows);
            }
            fclose($h);
        } else {
            die('Cannot open file ' . $logfile);
        }
    }
}
