<?php

namespace app\console\controllers;


use app\models\Node;
use app\models\Program;
use app\models\ProgramDeposit;
use app\models\User;
use Yii;
use yii\console\Controller;

class NodeController extends Controller {
    public function actionSeed($quantity) {
        $users = array_flip(User::find()->select('id')->column());
        $programs = Program::find()->indexBy('id')->all();
        $nodes = [];
        for ($i = 0; $i < $quantity; $i++) {
            $nodes[] = [
                array_rand($users),
                array_rand($programs),
            ];
        }
        Yii::$app->db
            ->createCommand()
            ->batchInsert('node', ['user', 'program'], $nodes)
            ->execute();
        /**
         * @var Node[] $nodes
         */
        $nodes = Node::find()->all();
        $transfers = [];
        $programs = ProgramDeposit::find()->indexBy('id')->where(['currency' => 'USD'])->all();
        foreach ($nodes as $node) {
//            var_dump($node);
            $program = $programs[$node->program];
            $transfers[] = [
                $node->user,
                rand($program->getMin(), $program->getMax()),
                $node->created,
                $program->currency,
                $node->id,
                'buy',
                'success'
            ];
        }
        Yii::$app->db
            ->createCommand()
            ->batchInsert('transfer', ['from', 'amount', 'created', 'currency', 'node', 'type', 'status'], $transfers)
            ->execute();
    }
}
