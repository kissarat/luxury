<?php

namespace app\console\controllers;


use app\models\User;
use Faker\Factory;
use Yii;
use yii\console\Controller;

class UserController extends Controller {
    public function actionSeed() {
        $array = json_decode(file_get_contents(__DIR__ . '/../seeds/user.json'), JSON_OBJECT_AS_ARRAY);
        $loaded = array_keys($array);
        $found = User::find()
            ->where(['in', 'nick', $loaded])
            ->select(['nick'])
            ->column();
        $news = array_diff($loaded, $found);
        foreach ($news as $nick) {
            $data = $array[$nick];
            $data['nick'] = $nick;
            $user = new User($data);
            $user->generateSecret();
            if (!$user->save()) {
                echo json_encode([
                    'nick' => $nick,
                    'errors' => $user->getErrors()
                ],
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * @param $nick string Root user
     * @param int $depth integer Depth of structure
     * @param int $width integer Maximum width
     */
    public function actionChildren($nick, $depth = 1, $width = 5) {
        $factory = Factory::create();
        $parent = User::findOne(['nick' => $nick]);
        $size = rand(0, $width);
        $children = [];
        for ($i = 0; $i < $size; $i++) {
            $userName = $factory->userName;
            $children[] = [
                $userName,
                $factory->email,
                $factory->firstName,
                $factory->lastName,
                $userName,
                $parent->id,
                'native',
                '$2a$10$mqSyMb..y5aMEzEm9ROKs.ZHZHfH5Rwznkc1g2TbBbD.hp1LcJZ3y'
            ];
        }
        Yii::$app->db
            ->createCommand()
            ->batchInsert('user', ['nick', 'email', 'forename', 'surname', 'skype', 'parent', 'type', 'secret'], $children)
            ->execute();
        if ($depth > 1) {
            foreach ($children as $child) {
                $this->actionChildren($child[0], $depth - 1);
            }
        }
    }
}
