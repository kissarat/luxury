<?php

namespace app\assets;


use yii\web\AssetBundle;

class OutdoorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/main.css'
    ];
    public $js = [
        '/js/exit.js',
        '/js/sponsor.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
