<?php

namespace app\assets;


use yii\web\AssetBundle;

class CabinetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/main.css'
    ];
    public $js = [
//        '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js',
//        '/js/cabinet.js',
//        '/js/materialize.min.js',
//        '/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js',
//        '/js/plugins/chartist-js/chartist.min.js',
//        '/js/plugins/chartjs/chart.min.js',
//        '/js/plugins/chartjs/chart-script.js',
//        '/js/plugins/sparkline/jquery.sparkline.min.js',
//        '/js/plugins/sparkline/sparkline-script.js',
//        '/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
//        '/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
//        '/js/plugins/jvectormap/vectormap-script.js',
//        '/js/structure.js',
//        '/js/histogram.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
