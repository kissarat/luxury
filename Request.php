<?php

namespace app;

function isPrivateNetwork($ip) {
    $masks = [
        167772160 => 4278190080,
        1681915904 => 4290772992,
        2886729728 => 4293918720,
        3232235520 => 4294901760
    ];
    $ip = ip2long($ip);
    foreach ($masks as $network => $mask) {
        if (+$network === ($ip & $mask)) {
            return true;
        }
    }
    return false;
}

class Request extends \yii\web\Request
{
    public $mirror = false;

    public function getUserIP() {
        foreach (['HTTP_CF_CONNECTING_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_IP', 'HTTP_XIP', 'SERVER_ADDR'] as $name) {
            if (isset($_SERVER[$name]) && '127.0.0.1' !== $_SERVER[$name] && !isPrivateNetwork($_SERVER[$name])) {
                return $_SERVER[$name];
            }
        }
        return parent::getUserIP();
    }
}
