<?php

namespace app;

use Yii;
use yii\bootstrap\Widget;

class Alert extends Widget
{
    public $alertTypes = [
        'error' => 'alert-danger',
        'danger' => 'alert-danger',
        'success' => 'alert-success',
        'info' => 'alert-info',
        'warning' => 'alert-warning'
    ];

    public $closeButton = [];

    public function run() {
        $session = Yii::$app->getSession();
        $flashes = $session->getAllFlashes();
        $appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        $strings = [];
        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array)$data;
                foreach ($data as $i => $message) {
                    /* initialize css class for each alert box */
                    $this->options['class'] = $this->alertTypes[$type] . $appendCss;

                    /* assign unique id to each alert box */
                    $this->options['id'] = $this->getId() . '-' . $type . '-' . $i;
                    $strings[] = \yii\bootstrap\Alert::widget([
                        'body' => $message,
                        'closeButton' => $this->closeButton,
                        'options' => [
                            'class' => 'card green lighten-5 card-content green-text',
                            'id' => 'card-alert',
                        ],
                    ]);
                }

                $session->removeFlash($type);
            }
        }

        $strings =implode("\n", $strings);
        return "<div id=\"alert-container\">$strings</div>";
    }
}
