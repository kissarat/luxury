<?php

namespace app\traits;


use app\helpers\Utils;

trait BroadcastModelTrait
{
    public function afterSave($insert, $changedAttributes) {
        $this->broadcast([
            'method' => $insert ? 'POST' : 'PUT',
            'params' => ['id' => $this->getPrimaryKey()],
            'data' => $changedAttributes
        ]);
    }

    public function afterDelete() {
        $this->broadcast([
            'method' => 'DELETE',
            'params' => ['id' => $this->getPrimaryKey()]
        ]);
    }

    public function beforeSave($insert) {
        if ($insert && isset($this->created)) {
            $this->created = Utils::timestamp();
        }
        elseif (isset($this->time)) {
            $this->time = Utils::timestamp();
        }
        return true;
    }

    public function broadcast($object) {
//        $tableName = static::tableName();
//        $id = $this->id;
//        file_put_contents("/tmp/$tableName-$id.json", json_encode($object, JSON_PRETTY_PRINT));
    }
}
