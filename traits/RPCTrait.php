<?php

namespace app\traits;

trait RPCTrait {
    use HTTPTrait;

    public function getRequestId() {
        return microtime(true) * 1000000 - 1507205042627000;
    }

    public function request($method, $params = []) {
        $options = [
            'jsonrpc' => '2.0',
            'id' => $this->getRequestId(),
            'method' => $method,
            'params' => $params,
        ];
        $d = $this->client->post('https://gw.softroom.pro/teiP3riegei4ZaFo0yaShooL', $options, $this->headers ?: [])
            ->send()
            ->getData();
        return isset($d['result']) ? $d['result'] : $d;
    }
}
