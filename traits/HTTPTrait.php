<?php

namespace app\traits;


use yii\httpclient\Client;

/**
 * @property Client $client
 */
trait HTTPTrait
{
    public $client;
    public $baseUrl;
    public $headers;

    public function init() {
        $options = [
            'requestConfig' => [
                'format' => Client::FORMAT_JSON
            ],
            'responseConfig' => [
                'format' => Client::FORMAT_JSON
            ],
        ];
        if ($this->baseUrl) {
            $options['baseUrl'] = $this->baseUrl;
        }
        $this->client = new Client($options);
    }

    function __call($method, $params) {
        return $this->request($method, $params);
    }
}
