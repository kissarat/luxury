<?php
use yarcode\advcash\Merchant;

require_once 'functions.php';

return local('common', [
    'basePath' => ROOT,
    'bootstrap' => [],
    'language' => 'en',
    'charset' => 'utf-8',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'app\components\JsonMessageSource'
                ]
            ]
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'port' => 587,
                'encryption' => 'tls',
            ],
        ],

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=luxury',
            'username' => 'luxury',
            'password' => 'luxury',
            'charset' => 'utf8'
        ],

        'blockio' => [
            'class' => 'app\components\Blockio'
        ],

        'ethereum' => [
            'class' => 'app\components\Ethereum',
            'baseUrl' => 'https://gw.softroom.pro/teiP3riegei4ZaFo0yaShooL',
            'headers' => [
                'authorization' => 'Basic emJ5c3plazplaWYzQWh3M2VpbmdhaGRldWwzb29UaGFlMFVvamFpZ29vbmk='
            ]
        ],

        'perfect' => [
            'class' => 'app\components\Perfect'
        ],

        'telegram' => [
            'class' => 'app\components\Telegram',
            'enabled' => false
        ],

        'advcash' => [
            'class' => '\yarcode\advcash\Merchant',
            'sciCurrency' => 'USD',
            'sciCheckSign' => true,
            'successUrl' => 'advcash/success',
            'failureUrl' => 'advcash/failure',
            'resultUrl' => 'advcash/result',
            'successUrlMethod' => 'GET',
            'failureUrlMethod' => 'GET',
            'resultUrlMethod' => 'GET'
        ]
    ],
    'params' => [
        'referralLinkOrigin' => 'http://localhost',
        'checkLoginAttempt' => true
    ],
]);
