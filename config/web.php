<?php
require_once 'functions.php';

return extend('web', 'common', [
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/default',

    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'login' => 'user/login',
                'register' => 'user/signup',
                'login/<sponsor:[\w_]+>' => 'user/login',
                'sponsor/<sponsor:[\w_]+>' => 'user/signup',
                'programs' => 'program/embed',
                'newbie/<nick>' => 'home/newbie',
                'contact/<id>' => 'user/contact',
                'profile/<nick>' => 'user/profile',
                '<type>-history/<nick>' => 'transfer/index',
                'history/<nick>/<id>' => 'transfer/index',
                'history/<nick>' => 'transfer/index',
                'pay/<nick>' => 'transfer/pay',
                'withdraw/<nick>' => 'transfer/withdraw',
                'structure/<nick>/<page>' => 'structure/index',
                'children/<id>' => 'structure/children',
                'settings/<nick>/profile' => 'settings/profile',
                'settings/<nick>/wallets' => 'settings/wallets',
                'settings/<nick>/password' => 'settings/password',
                'logout/<nick>' => 'user/logout',
                'deposits/<nick>' => 'node/index',
                'newbie' => 'user/visit',
                'success-perfect-money/<id>' => 'perfect/success',
                'status-perfect-money/<id>' => 'perfect/status',
                'fail-perfect-money/<id>' => 'perfect/fail',
                'under-construction' => 'home/construction',
                'advertise' => 'home/advertise',
                'social' => 'home/social',
                'pdf' => 'home/pdf',
                'status' => 'home/status',
                'landing' => 'home/landing',
                'banners' => 'home/banners',
                'address/<nick>/blockio' => 'blockio/address',
                'address/<nick>/ethereum' => 'ethereum/address',
                'reset/<code>' => 'user/reset',
                '~c' => 'home/coordinate',
                '~v/<uid>/<url>/<spend>' => 'home/visit',
                'admin' => 'home/admin',
                'advcash-merchant' => 'advcash/billing',
            ],
        ],

        'request' => [
            'class' => 'app\Request',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],

        'cache' => [
            'class' => 'yii\caching\DbCache'
        ],

        'errorHandler' => [
            'errorAction' => 'home/error',
        ],

        'session' => [
            'class' => 'yii\web\DbSession',
            'readCallback' => function ($fields) {
                $fields['id'] = session_id();
                return $fields;
            }

        ],

        'user' => [
            'identityClass' => 'app\models\Identity',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'idParam' => 'id',
            'autoRenewCookie' => false,
            'authTimeout' => 30 * 24 * 3600
        ],
    ]
]);
