<?php
$config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['127.0.0.1', '::1'],
];
$config['modules']['gii'] = 'yii\gii\Module';

$config['bootstrap'][] = 'debug';
