<?php

require_once 'local/define.php';
defined('ROOT') or define('ROOT', realpath(__DIR__ . '/..'));

function local($name, array $defaults) {
    $filename = __DIR__ . "/local/$name.php";
    if (file_exists($filename)) {
        return array_replace_recursive($defaults, require $filename);
    }
    return $defaults;
}

function extend($name, $baseName, array $defaults) {
    return local($name, array_replace_recursive(require __DIR__ . "/$baseName.php", $defaults));
}
