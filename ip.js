const fs = require('fs')

const data = fs.readFileSync(process.argv[process.argv.length - 1]).toString('utf8')
const ips = []
for(const line of data.split('\n')) {
  const s = line.split(/\s+/)
  if (ips.indexOf(s[0]) < 0) {
    ips.push(s[0])
  }
}

ips.sort()
console.log(ips.join('\n'))
