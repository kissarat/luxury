#!/usr/bin/env bash

test -f web/index.html && mv web/index.html web/_index_pull_backup.html
ln -s web/maintenance.html web/index.html
git pull
./yii locale/generate-script
chmod a+w web/ui/jsonp.js
rm web/index.html
test -f web/_index_pull_backup.html && mv web/_index_pull_backup.html web/index.html
