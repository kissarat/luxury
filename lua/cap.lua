local client = require('http.client')
local json = require('json')
local fiber = require('fiber')

box.cfg { listen = 3301 }

function run(limit)
    local r = client.get('https://api.coinmarketcap.com/v1/ticker/?limit=' .. limit)
    for key, row in next, json.decode(r.body), nil do
        if not box.space[row.symbol] then
            local s = box.schema.space.create(row.symbol)
            s:create_index('primary', {
                type = 'hash',
                unique = true,
                parts = { 1, 'unsigned' }
            })
        end
        local id = tonumber(row.last_updated)
        print(row.symbol, id, row.price_usd, row.market_cap_usd)
        local s = box.space[row.symbol]
        --box.space[row.symbol]:upsert({ id }, {
        --    { '=', 2, tonumber(row.price_usd) },
        --    { '=', 3, tonumber(row.market_cap_usd) },
        --} )
        s:delete { id }
        s:insert { id, tonumber(row.price_usd), tonumber(row.market_cap_usd) }
    end
end

local f = fiber.create(function()
    while true do
        run(20)
        fiber.sleep(10)
    end
end)

local param = ...
local function getCurrencies(self)
    local r = {}
    for currency in pairs(box.space) do
        if type(currency) == 'string' and string.match(currency, '^[A-Z]+$') then
            local last
            local count = 0
            for _, record in pairs(box.space[currency]:select()) do
                count = count + 1
                last = record
            end
            if count > 0 then
                r[currency] = {
                    time = last[1],
                    usd = last[2],
                    cap = last[3],
                    count = count
                }
            end
        end
    end
    return self:render { json = { result = r } }
end

local function getHistory(self)
    local currency = self:stash('currency')
    local r = {}
    local s = box.space[currency]
    if not s then
        return {status = 404}
    end
    local count = 0
    for _, record in pairs(s:select()) do
        count = count + 1
        r[record[1]] = {record[2], record[3]}
    end
    return self:render {json = {
        count = count,
        result = r
    }}
end

if 'api' == param then
    local server = require('http.server').new(nil, 8080)
    server:route({ path = '/' }, getCurrencies)
    server:route({ path = '/history/:currency' }, getHistory)
    server:start()
end
