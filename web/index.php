<?php

$config = require '../config/web.php';

require '../vendor/yiisoft/yii2/Yii.php';
require '../vendor/autoload.php';

if (YII_DEBUG) {
    require '../config/debug.php';
}

$config['language'] = empty($_COOKIE['locale']) ? 'en' : $_COOKIE['locale'];

$app = new yii\web\Application($config);

if (version_compare(PHP_VERSION, '7.0.0', '<')) {
    // ensure code works on php < 7.0.0 to not break BC
    class_alias('yii\base\BaseObject', 'yii\base\Object', false);
}

foreach (require '../config/class.php' as $class => $config) {
    \Yii::$container->set($class, $config);
}

$app->run();
