const systems = {
  ethereum: {image: '/images/pay/eth.png', title: 'Ethereum', crypto: true, wallet: /^0x[a-fA-F0-9]{40}$/},
  blockio: {image: '/images/pay/btc.png', title: 'Bitcoin', crypto: true, wallet: /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/},
  advcash: {image: '/images/pay/cash.png', title: 'AdvCash'},
  perfect: {image: '/images/pay/pm.png', title: 'Perfect Money', wallet: /^U\w{7,8}$/},
}

const invoiceData = {
  system: 'perfect',
  amount: '',
  wallet: '',
  text: '',
  action: /pay$/.test(location.pathname) ? 'payment' : 'withdraw',
  systems,
  wallets
}

addEventListener('load', function () {
  for(const name of ['amount', 'wallet', 'text', 'system']) {
    const input = document.querySelector(`[name="w[${name}]"]`)
    if (input && input.value) {
      invoiceData[name] = input.value
    }
  }

  new Vue({
    el: '#app',
    data: invoiceData,

    computed: {
      validAmount() {
        return this.amount > 0
      },

      validWallet() {
        return systems[this.system].wallet.test(this.wallet.trim())
      },

      valid() {
        return this.validAmount && this.validWallet
      }
    },

    methods: {
      async fetchAddress(system) {
        this.wallets[system] = await fetchJSON(`/address/${config.nick}/${system}`)
      }
    }
  })
})
