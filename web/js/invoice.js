const systems = {
  ethereum: {image: '/images/pay/eth.png', title: 'Ethereum', crypto: true, wallet: /^0x[a-fA-F0-9]{40}$/},
  blockio: {image: '/images/pay/btc.png', title: 'Bitcoin', crypto: true, wallet: /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/},
  advcash: {image: '/images/pay/cash.png', title: 'AdvCash', wallet: /^U\w{12}$/},
  perfect: {image: '/images/pay/pm.png', title: 'Perfect Money', wallet: /^U\w{7,8}$/},
}

const invoiceData = {
  system: 'perfect',
  amount: '',
  wallet: '',
  text: '',
  action: /pay$/.test(location.pathname) ? 'payment' : 'withdraw',
  systems: systems,
  wallets: wallets
}

addEventListener('load', function () {
  ['amount', 'wallet', 'text', 'system'].forEach(function () {
    const input = document.querySelector('[name="w[' + name + '"]')
    if (input && input.value) {
      invoiceData[name] = input.value
    }
  })

  // window.invoice = new Vue({
  new Vue({
    el: '#app',
    data: invoiceData,

    computed: {
      validAmount: function () {
        return this.amount > 0
      },

      validWallet: function () {
        return systems[this.system].wallet.test(this.wallet.trim().replace(/\s/g, ''))
      },

      valid: function () {
        return this.validAmount && this.validWallet
      }
    },

    methods: {
      fetchAddress: function (system) {
        const self = this
        fetchJSON('/address/' + config.nick + '/' + system)
            .then(function (r) {
              self.wallets[system] = r
            })
      }
    }
  })
})
