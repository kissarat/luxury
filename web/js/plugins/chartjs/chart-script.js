const tooltipTitleFontFamily = "'Roboto','Helvetica Neue', 'Helvetica', 'Arial', sans-serif";

function showTrendingLineChart(labels, income, outcome) {
  var data = {
    labels,
    datasets: [
      {
        label: t('Income'),
        fillColor: "rgba(128, 222, 234, 0.6)",
        strokeColor: "#ffffff",
        pointColor: "#00bcd4",
        pointStrokeColor: "#ffffff",
        pointHighlightFill: "#ffffff",
        pointHighlightStroke: "#ffffff",
        // tooltipFormat: $.spformat('{{value}}', 'tooltip-class'),
        data: income
      }
      /* {
       label: t('Outcome'),
       fillColor: "rgba(128, 222, 234, 0.9)",
       strokeColor: "#ffdeea",
       pointColor: "#fb8c00",
       pointStrokeColor: "#ffdeea",
       pointHighlightFill: "#ffdeea",
       pointHighlightStroke: "#ffdeea",
       data: outcome
       } */
    ]
  };

  var trendingLineChart = document.getElementById("trending-line-chart").getContext("2d");
  window.trendingLineChart = new Chart(trendingLineChart).Line(data, {
    scaleShowGridLines: true,///Boolean - Whether grid lines are shown across the chart
    scaleGridLineColor: "rgba(255,255,255,0.4)",//String - Colour of the grid lines
    scaleGridLineWidth: 1,//Number - Width of the grid lines
    scaleShowHorizontalLines: true,//Boolean - Whether to show horizontal lines (except X axis)
    scaleShowVerticalLines: false,//Boolean - Whether to show vertical lines (except Y axis)
    bezierCurve: true,//Boolean - Whether the line is curved between points
    bezierCurveTension: 0.4,//Number - Tension of the bezier curve between points
    pointDot: true,//Boolean - Whether to show a dot for each point
    pointDotRadius: 5,//Number - Radius of each point dot in pixels
    pointDotStrokeWidth: 2,//Number - Pixel width of point dot stroke
    pointHitDetectionRadius: 20,//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    datasetStroke: true,//Boolean - Whether to show a stroke for datasets
    datasetStrokeWidth: 3,//Number - Pixel width of dataset stroke
    datasetFill: true,//Boolean - Whether to fill the dataset with a colour
    animationSteps: 60,// Number - Number of animation steps
    animationEasing: "easeOutQuart",// String - Animation easing effect
    scaleFontSize: 12,// Number - Scale label font size in pixels
    scaleFontStyle: "normal",// String - Scale label font weight style
    scaleFontColor: "#fff",// String - Scale label font colour
    tooltipEvents: ["mousemove", "touchstart", "touchmove"],// Array - Array of string names to attach tooltip events
    tooltipFillColor: "rgba(255,255,255,0.8)",// String - Tooltip background colour
    tooltipFontSize: 12,// Number - Tooltip label font size in pixels
    tooltipFontColor: "#000",// String - Tooltip label font colour
    tooltipTitleFontSize: 14,// Number - Tooltip title font size in pixels
    tooltipTitleFontStyle: "bold",// String - Tooltip title font weight style
    tooltipTitleFontColor: "#000",// String - Tooltip title font colour
    tooltipYPadding: 8,// Number - pixel width of padding around tooltip text
    tooltipXPadding: 16,// Number - pixel width of padding around tooltip text
    tooltipCaretSize: 10,// Number - Size of the caret on the tooltip
    tooltipCornerRadius: 6,// Number - Pixel radius of the tooltip border
    tooltipXOffset: 10,// Number - Pixel offset from point x to tooltip edge
    responsive: true,
    tooltipFormat: $.spformat('{{value}}', 'tooltip-class'),
    // tooltipFormatter() {
    //   console.log(...arguments)
    // },
    tooltipTitleFontFamily
  });
}

function showDoughnutData1(accrue, bonus) {
  var doughnutData = [
    {
      value: accrue,
      color: "#F7464A",
      highlight: "#FF5A5E",
      label: t('Deposit')
    },
    {
      value: bonus,
      color: "#46BFBD",
      highlight: "#5AD3D1",
      label: t('Partners')
    }

  ];

  var doughnutChart = document.getElementById("doughnut-chart").getContext("2d");
  window.myDoughnut = new Chart(doughnutChart).Doughnut(doughnutData, {
    segmentStrokeColor: "#fff",
    percentageInnerCutout: 50,
    animationSteps: 100,
    segmentStrokeWidth: 4,
    animateScale: true,
    responsive: true,
    tooltipTitleFontFamily
  });
}

function showDoughnutData2(labels, data) {
  var dataBarChart = {
    labels,
    datasets: [
      {
        label: "Bar dataset",
        fillColor: "#46BFBD",
        strokeColor: "#46BFBD",
        highlightFill: "rgba(70, 191, 189, 0.4)",
        highlightStroke: "rgba(70, 191, 189, 0.9)",
        data
      }
    ]
  };

  var trendingBarChart = document.getElementById("trending-bar-chart").getContext("2d");
  window.trendingBarChart = new Chart(trendingBarChart).Bar(dataBarChart, {
    scaleShowGridLines: false,///Boolean - Whether grid lines are shown across the chart
    showScale: true,
    responsive: true,
    // tooltipFormat: $.spformat('{{value}}', 'tooltip-class'),
    // tooltipFormatter() {
    //   console.log(...arguments)
    // },
    tooltipTitleFontFamily
  });
}
