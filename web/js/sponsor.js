(function () {
  const sponsorCookieRegex = /sponsor=([\w_]+)/
  const sponsor = /r=([\w_~]+)/.exec(location.search) || /\/sponsor\/([\w_]+)/.exec(location.pathname)
  if (sponsor) {
    document.cookie = 'sponsor=' + sponsor[1] + '; path=/; max-age=315360000'
    let ico = /ico=(\w+)/.exec(document.cookie)
    const isNew = !!ico
    if (ico && ico[1].length > 5 && ico[1].indexOf(' ') < 0) {
      ico = ico[1]
    }
    else {
      ico = Date.now().toString(36)
          + '_' + Math.round(Math.random() * 36 * 36 * 36 - 1).toString(36)
          + '~' + sponsor[1]
      document.cookie = 'ico=' + ico + '; path=/; max-age=' + (3600 * 24 * 365 * 10)
    }
    if (location.hostname.indexOf('office.') < 0 && isNew) {
      const script = document.createElement('script')
      script.src = location.protocol + '//office.' + location.hostname + '/newbie/' + sponsor[1]
          + '?callback=icov&ico=' + ico
      document.head.appendChild(script)
    }
  }
  document.addEventListener('DOMContentLoaded', function () {
    const sponsor = sponsorCookieRegex.exec(document.cookie)
    if (sponsor) {
      [].forEach.call(document.querySelectorAll('[href$="/register"]'), function (a) {
        a.href = a.href.replace('/register', '/sponsor/' + sponsor[1])
      });
      [].forEach.call(document.querySelectorAll('[href$="/login"]'), function (a) {
        a.href = a.href.replace('/login', '/login/' + sponsor[1])
      });
    }
  })

  window.icov = Function()
})();
