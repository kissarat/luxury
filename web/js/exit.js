function main({ip}) {
  // document.body.classList.add('outdoor-body')
  // document.body.classList.add(location.pathname.replace('/user/', ''))
  $('[id$="-ip"]').val(ip)
  let sponsor = /(sponsor|login)\/([^?&]+)/.exec(location.search)
  if (sponsor) {
    sponsor = sponsor[1]
  }
  else if (localStorage.sponsor) {
    sponsor = localStorage.sponsor
  }
  const el = document.querySelector('[name="sponsor"]')
  if (el) {
    el.value = sponsor
  }

  setTimeout(function () {
    if ($('.signup-form').length > 0) {
      $('#username').focus()
    }
    else {
      $('#password').focus()
    }
  }, 100)
}

function signup(nick, sponsor) {
  $('#user-nick').val(nick)
  $('#user-email').val(nick + '@yopmail.com')
  $('#user-password').val(nick)
  $('#user-sponsor').val(sponsor)
  $('[action="/user/signup"]').submit()
}
