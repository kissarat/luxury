if (localStorage.getItem('program-pro') > 0) {
  document.getElementById('program-pro').style.removeProperty('display')
}

// if (!(localStorage.getItem('program-pro') > 0)) {
//   document.getElementById('program-pro').remove()
// }

function ICO_PRO_render(program) {
  const rates = {
    USD: 100,
    BTC: 1000000,
    ETH: 100000
  }

  ICO.proComponent = new Vue({
    el: '#program-pro',
    data: Object.assign({
      available: false,
      rates: rates,
      amount: 0,
      currency: '',
      alert: {},
      currencies: ['USD', 'BTC', 'ETH']
    }, program),

    computed: {
      valid: function() {
        return this.currency in rates
            && this.amount > 0
            && this.amount * rates[this.currency] >= this.min[this.currency]
      },

      today: function() {
        return moment().format('YYYY-MM-DD')
      },

      availableToday: function() {
        return this.amount * rates[this.currency] <= this.available[this.currency]
      },

      availableAnytime: function() {
        for(var date in this.reservation[this.currency]) {
          if (this.reservation[this.currency][date] >= this.amount) {
            return true
          }
        }
        return false
      }
    },

    methods: {
      select: function(currency) {
        this.amount = this.min[currency] / rates[currency]
        this.currency = currency
      },

      reserve: function(date) {
        return ICO.calculator.component.reserve(date, ICO_PRO, this.amount * rates[this.currency], this.currency)
      }
    },
  })
}
