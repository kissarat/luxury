document.addEventListener('DOMContentLoaded', function () {
    setupChildren(document.body)
})

function setupChildren(context, level = 1) {
  $('[data-time]', context).each(function (i, el) {
    el.innerHTML = new Date(1000 * el.getAttribute('data-time')).toLocaleString();
  })
  $('[data-date]', context).each(function (i, el) {
    el.innerHTML = new Date(1000 * el.getAttribute('data-date')).toLocaleDateString();
  })

  if (level >= 3) {
    $('thead tr th:first-child', context).remove();
  }

  for (const currency of ['usd', 'btc', 'eth']) {
    const rate = getRate(currency.toUpperCase())
    $(`.${currency}:not(.rated)`)
        .addClass('rated')
        .each(function (i, el) {
          el.innerHTML /= rate
        })
  }

  const rows = $('[data-id] > [data-children]', context)
  rows.each(function (i, td) {
    const sponsorRow = td.parentNode;
    const id = +sponsorRow.getAttribute('data-id')
    const level = +sponsorRow.querySelector('.level').innerHTML
    sponsorRow.setAttribute('data-level', level)
    const childrenCount = +td.getAttribute('data-children')
    if (level >= 3) {
      // td.remove();
    }
    else if (id > 0 && childrenCount > 0) {
      td.parentNode.addEventListener('click', function () {
        const next = $(sponsorRow.nextElementSibling)
        const loaded = next.hasClass('children')
        const children = loaded
            ? next
            : $('<tr class="children"></tr>')
                .append('<td colspan="11"></td>')
                .insertAfter(sponsorRow)
        if (loaded) {
          children.toggle();
          sponsorRow.classList.toggle('expanded')
        }
        else {
          const fragment = children.find('td')
          fragment
              .css('z-index', rows.length - i)
              .load('/children/' + id, function () {
                fragment.find('.level').each(function (i, levelCell) {
                  levelCell.innerHTML = level + +levelCell.innerHTML;
                })
                // fragment.find('thead').remove()
                setupChildren(fragment, level + 1)
                children.removeClass('loading')
                sponsorRow.classList.add('expanded')
              })
        }
      })
    }
  })
}
