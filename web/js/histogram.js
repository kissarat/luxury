const format = Object.freeze({
  date: 'YYYY-MM-DD',
  minute: 'HH:mm',
  hour: 'HH',
  datetime: 'YYYY-MM-DD HH:mm:ss',
  queryHour: 'YYYY-MM-DD HH:00:00',
  queryDay: 'YYYY-MM-DD 00:00:00',
  queryMonth: 'YYYY-MM-01 00:00:00',
})

const CONSOLIDATED_CURRENCY = 'ETH'

function getRate(from, to = from) {
  return config.exchange[from][to]
}

function getConsolidated(amount) {
  let result = 0
  for (const currency in amount) {
    result += CONSOLIDATED_CURRENCY === currency
        ? amount[currency]
        : amount[currency] * getRate(CONSOLIDATED_CURRENCY, currency)
  }
  return result / getRate(CONSOLIDATED_CURRENCY)
}

const start = new Date()
const tzMinutes = start.getTimezoneOffset()
const tzSeconds = tzMinutes * 60
const tz = tzSeconds * 1000

async function getIncomeHistogram(params) {
  if (!params.types) {
    params.types = ['accrue', 'bonus']
  }
  const types = params.types
  params.types = types.join(',')
  // params.currencies = 'USD,BTC,ETH'
  params.currencies = 'ETH'
  params.tz = tzMinutes
  const options = {credentials: 'include'}
  if (config.ip) {
    options.headers = {xip: config.ip}
  }
  const r = await fetch('/transfer/histogram?' + $.param(params), options)
  const {result} = await r.json()
  const labels = Object.keys(result)
  const series = {}
  for (const type of types) {
    const init = {}
    for (const label of labels) {
      init[label] = 0
    }
    series[type] = init
  }
  for (const time in result) {
    for (const t of result[time]) {
      let amount = t.amount / getRate(t.currency)
      if (CONSOLIDATED_CURRENCY !== t.currency) {
        amount *= getRate(t.currency, CONSOLIDATED_CURRENCY)
      }
      series[t.type][time] += amount
    }
  }
  return {labels, series}
}

function arraySum(first) {
  const result = new Array(first.length)
  result.fill(0)
  for (let i = 0; i < arguments.length; i++) {
    for (let j = 0; j < arguments[i].length; j++) {
      result[j] += +arguments[i][j]
    }
  }
  return result
}

function sumIncome(series) {
  return arraySum(Object.values(series.accrue), Object.values(series.bonus))
}

async function getHistogramValues(url, params) {
  const r = await fetch(url + '?' + $.param(params), {credentials: 'include'})
  const {result} = await r.json()
  return Object.values(result)
}

async function showHistograms(user) {
  let {labels, series} = await getIncomeHistogram({
    user: config.user,
    start: moment().add(tzMinutes, 'minute').add(-1, 'day').format(format.queryHour),
    stop: moment().format(format.queryHour),
    step: '3 hour'
  })

  new Chartist.Line('#ct2-chart', {
    labels: labels.map(time => moment(time).format(format.hour)),
    series: [sumIncome(series)]
  }, {
    low: 0,
    showArea: true
  });

  const clientsBar = $("#clients-bar");
  const days = Math.min(config.days, 24);
  if (user > 0 && clientsBar.length > 0) {
    // Freshman
    let series = await getHistogramValues('/structure/histogram', {
      user,
      start: moment().add(-(days || 1), 'day').format(format.queryDay),
      stop: moment().format(format.queryDay),
      step: '1 day'
    })

    clientsBar.sparkline(series, {
      type: 'bar',
      height: '25',
      width: '100%',
      barWidth: 10,
      barSpacing: 4,
      barColor: '#C7FCC9',
      negBarColor: '#81d4fa',
      zeroColor: '#81d4fa',
      //tooltipFormat: $.spformat('{{value}}', 'tooltip-class')
    });

    // Income
    let r = await getIncomeHistogram({
      user,
      start: moment().add(-(days || 1), 'day').format(format.queryDay),
      stop: moment().format(format.queryDay),
      step: '1 day',
      types: ['bonus']
    })

    $('#sales-compositebar').sparkline(Object.values(r.series.bonus), {
      type: 'bar',
      barColor: '#F6CAFD',
      height: '25',
      width: '100%',
      barWidth: '10',
      barSpacing: 2,
      //tooltipFormat: $.spformat('{{value}}', 'tooltip-class')
    });

    /*
     $('#sales-compositebar').sparkline([4, 1, 5, 7, 9, 9, 8, 8, 4, 2, 5, 6, 7], {
     composite: true,
     type: 'line',
     width: '100%',
     lineWidth: 2,
     lineColor: '#fff3e0',
     fillColor: 'rgba(153,114,181,0.3)',
     highlightSpotColor: '#fff3e0',
     highlightLineColor: '#fff3e0',
     minSpotColor: '#f44336',
     maxSpotColor: '#4caf50',
     spotColor: '#fff3e0',
     spotRadius: 4,
     //tooltipFormat: $.spformat('{{value}}', 'tooltip-class')
     });
     */

    r = await getIncomeHistogram({
          user,
          start: moment().add(-Math.min(config.days || 30, 30), 'days').format(format.queryDay),
          stop: moment().format(format.queryDay),
          step: '1 day'
        })

    const incomeSeries = sumIncome(r.series)
    showTrendingLineChart(
        r.labels.map(time => moment(time).format('DD')),
        incomeSeries,
        // Object.values(r.series.buy),
    )

    const accrue = getConsolidated(userProfile.accrue)
    const bonus = getConsolidated(userProfile.bonus)
    const summaryIncome = accrue + bonus
    if (summaryIncome >= 0) {
      document.getElementById('user-earned').innerHTML = summaryIncome.toFixed(2)
      showDoughnutData1(accrue, bonus)
    }


    r = await getIncomeHistogram({
      user,
      start: moment().add(-Math.min(config.months, 6), 'month').format(format.queryMonth),
      stop: moment().format(format.queryMonth),
      step: '1 month'
    })

    showDoughnutData2(
        r.labels.map(time => moment(time).format('MMM')),
        sumIncome(r.series).map(a => a.toFixed(2))
    )

    let rows = [];
    const cons = CONSOLIDATED_CURRENCY.toLocaleLowerCase()
    for (const time of r.labels) {
      const month = moment(time).format('MMMM')
      const accrue = r.series.accrue[time]
      const bonus = r.series.bonus[time]
      const income = accrue + bonus
      rows.push(`<tr><td>${month}</td><td class="${cons}">${accrue}</td><td class="${cons}">${bonus}</td><td class="${cons}">${income}</td></tr>`)
    }
    document.querySelector('#monthly-income tbody').innerHTML = rows.join('\n')

    const transfers = await fetchJSON('/node/index', {limit: 6, nick: config.nick})
    rows = []
    const incomeEl = document.querySelector('#income')
    if (transfers.length > 0) {
      incomeEl.style.removeProperty('display')
      for (const t of transfers) {
        const curr = t.currency.toLocaleLowerCase()
        if ('string' === typeof t.vars) {
          t.vars = JSON.parse(t.vars)
        }
        const rate = getRate(t.currency)
        const bought = new Date(t.bought).toLocaleDateString()
        const closing = new Date(t.closing).toLocaleDateString()
        const amount = t.amount / rate
        const profit = t.profit / rate
        const n = t.vars && t.vars.number >= 0 && t.vars.quantity >= 0 ? t.vars.number + ' / ' + t.vars.quantity : ''
        rows.push(`<tr><td>${bought}</td><td>${t.name}</td><td class="${curr}">${amount}</td><td>${t.accrues}</td>
            <td>${profit}</td><td>${closing}</td></tr>`)
      }
      document.querySelector('#income tbody').innerHTML = rows.join('\n')
    }
    else {
      incomeEl.innerHTML = ''
    }


    // Tristate chart (Today Profit)
    $("#profit-tristate").sparkline(incomeSeries.slice(-24), {
      type: 'tristate',
      width: '100%',
      height: '25',
      posBarColor: '#B9DBEC',
      negBarColor: '#C7EBFC',
      barWidth: 10,
      barSpacing: 4,
      zeroAxis: false,
      // tooltipFormat: $.spformat('{{value}}', 'tooltip-class')
    })
  }
}
