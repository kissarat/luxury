function getReserved() {
    const li = document.querySelector('.program-index li[data-currency]')
    const currency = li.getAttribute('data-currency').toLocaleLowerCase()
    return +li.querySelector('.available.' + currency).innerHTML.trim()
}

function changeProgramAmount() {
    const program = document.querySelector('.program-index li[data-currency]')
    const amount = program.querySelector('[name=amount]').value
    const isAvailable = amount < getReserved()
    program.classList.toggle('reserve', !isAvailable)
}

document.addEventListener('DOMContentLoaded', function () {
    const form = $(`.program-index li form`)
    $('.program-index li .select-currency').click(function () {
        $('.program-index li[data-currency]').removeAttr('data-currency')
        const program = this.getAttribute('data-program')
        const currency = this.getAttribute('data-currency')
        const min = +this.querySelector('.min').innerHTML.trim()
        const max = +this.querySelector('.max').innerHTML.trim()
        $(`.program-index li[data-program="${program}"]`).attr('data-currency', currency)
        const form = $(`.program-index li[data-program="${program}"] form`)
        form.find('[name="currency"]').val(currency)
        form.find('[name="amount"]')
            .attr('min', min)
            .attr('max', max)
            .keyup(changeProgramAmount)
        changeProgramAmount()
    })

    form.find('[type=radio]').click(function () {
        form.find('button.reserve').prop('disabled', false)
    })
    form.submit(function (e) {
        // e.preventDefault()
    })
    $(`.program-index [name="amount"]`).change(changeProgramAmount)
})
