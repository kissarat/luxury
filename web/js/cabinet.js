const cryptocurrencies = {blockio: 'BTC', ethereum: 'ETH'}

function t(message) {
  return locale[message] || message
}

HTMLElement.prototype.getBackgroundImage = function () {
  return /url\("([^"]+)"\)/.exec(this.style.backgroundImage)[1]
}

const ICO_internal = {}

function fetchJSON(pathname, params) {
  const headers = {
    accept: 'application/json',
    nick: config.nick,
    before: document.referrer,
    cores: navigator.hardwareConcurrency
  }
  if (config.ip) {
    headers.xip = config.ip
  }
  if (navigator.languages instanceof Array) {
    headers.languages = navigator.languages.join(', ')
  }
  if (navigator.connection && navigator.connection.downlink) {
    headers.downlink = navigator.connection.downlink
    headers.rtt = navigator.connection.rtt
  }
  if (params) {
    pathname += '?' + $.param(params)
  }
  return fetch(pathname, {
    credentials: 'include',
    headers: headers
  })
      .then(function (r) {
        return r.json()
      })
      .then(function (r) {
        return r.result
      })
}

function addAlert(type, message) {
  const container = document.getElementById('alert-container')
  if ('error' === type) {
    type = 'danger'
  }
  if (container) {
    const div = document.createElement('div')
    div.setAttribute('class', 'alert alert-' + type)
    div.innerHTML = message
    container.appendChild(div)
  }
  else {
    console.error('No alert container', type, message)
  }
}

function getRate(from, to) {
  if (!to) {
    to = from
  }
  return config.exchange[from][to]
}

window['_' + config.nick] = function (iparg) {
  const ip = iparg.ip
  config.ip = ip
  $('[id$="-ip"]').val(ip)

  // const langRegex = /locale=(\w{2})/.exec(document.cookie)
  // if (langRegex) {
  //   ICO_internal.locale = langRegex[1]
  //   if ('ru' === ICO_internal.locale) {
  //     moment = moment.lang('ru')
  //   }
  // }

  document.addEventListener('DOMContentLoaded', function () {
    if ('function' === typeof showHistograms) {
      void showHistograms('undefined' === typeof userProfile ? null : userProfile.id)
    }
  })

  if ('undefined' !== typeof _ICO_messages) {
    window.locale = _ICO_messages
  }

  const started = new Date()
  if ('undefined' === typeof config) {
    console.error('Config not found')
  }
  else if (config.user > 0) {
    const created = new Date(config.user_created)
    config.days = Math.floor((started.getTime() - created.getTime()) / (24 * 3600 * 1000))
    config.months = Math.ceil(moment(started).diff(moment(created), 'months', true))
  }
  else {
    console.error('User is not authorized', config.guest)
  }

  $('.autosubmit').submit()

  $('#user-avatar_file').change(function (e) {
    const file = e.originalEvent.target.files[0]
    if (file) {
      const url = URL.createObjectURL(file)
      $('.settings-profile .preview')
          .addClass('active')
          .css('background-image', 'url("' + url + '")')
    }
  })

  $('[name="referral"]').click(function () {
    if (window.clipboardData && clipboardData.setData instanceof Function) {
      clipboardData.setData('URL', this)
    }
    else if (document.execCommand instanceof Function) {
      this.select()
      document.execCommand('copy')
    }
    $('.referral-copy-text').text('Copied')
  })

  $('#upload_avatar .btn')
      .click(function () {
        $('#user-avatar_file').click()
      })

  $('#payment-system').change(function (e) {
    const v = e.target.value
    $('.transfer-pay').toggleClass('cc', v in cryptocurrencies)
  })

  const receiveAddressButton = $('.get-cc-address')
  receiveAddressButton.click(function () {
    receiveAddressButton.text('Wait, please...')
    receiveAddressButton.prop('disabled', true)
    receiveAddressButton.remove()
    const cc = $('#payment-system')[0]
    fetchJSON('/address/' + config.nick + '/' + cc.value)
        .then(function (address) {
          // console.log(address)
          $('.cc-address .value').text(address)
        })
        .catch(function (err) {
          $('.cc-address .value').text(t("Can't get cryptocurrency address") + ': ' + err.message)
        })
  })

  $('<div class="overlay">' +
      '<img src="/images/under-construction.png" /><div class="text">Under Construction</div></div>')
      .appendTo('.under-construction')

  $('[data-construction]').each(function (i, c) {
    c.addEventListener(c.getAttribute('data-construction'), function (e) {
      e.preventDefault()
      alert(t('Sorry. This function is under construction'))
    })
  })

  $('[data-locale]').click(function (e) {
    document.cookie = 'locale=' + e.currentTarget.dataset.locale + '; path=/; max-age=' + 3600 * 365
    location.reload()
  })

  const overlay = $('#overlay')
  overlay.find('.close').click(function () {
    overlay.hide();
  })
  window.addEventListener('keyup', function (e) {
    if ('Escape' === e.key) {
      overlay.removeClass('visible');
    }
  })
  $('.avatar').click(function () {
    overlay.find('img').attr('src', this.getBackgroundImage())
    overlay.addClass('visible');
  })

  if ('function' === typeof Element.prototype.webkitRequestFullscreen) {
    Element.prototype.requestFullscreen = Element.prototype.webkitRequestFullscreen
  }
  if ('function' === typeof Element.prototype.requestFullscreen) {
    $('.toggle-fullscreen')
        .css('visibility', 'visible')
        .click(function () {
          if (document.isFullScreen || document.webkitIsFullScreen) {
            (document.webkitExitFullscreen || document.exitFullscreen)()
          }
          else {
            document.body.requestFullscreen()
          }
        })
  }

  $('.page.banners textarea').each(function (i, el) {
    el.value = el.value
        .replace(/\s+/g, ' ')
        .replace(/>\s*</g, '>\n<')
    // .replace('<img', '\t<img')
  })

  $('.pay-button-home .withdraw').click(function (e) {
    if (!userProfile.is_investor) {
      e.preventDefault();
      addAlert('error', t('You are not investor of ICO Holding'))
    }
    if (userProfile.has_unapproved_withdraw) {
      e.preventDefault();
      addAlert('error', t('Previous withdrawal is not yet approved'))
    }
  })

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('/background.js', {
          scope: '/'
        })
        .then(function (registration) {
          let serviceWorker;
          if (registration.installing) {
            serviceWorker = registration.installing;
            // console.log('installing');
          } else if (registration.waiting) {
            serviceWorker = registration.waiting;
            // console.log('waiting');
          } else if (registration.active) {
            serviceWorker = registration.active;
            // console.log('active');
          }
          if (serviceWorker) {
            // console.log('worker', serviceWorker.state);
            serviceWorker.addEventListener('statechange', function (e) {
              // console.log('state', e.target.state);
            });
          }
        }).catch(function (error) {
      // console.error(error)
    });
  }

  if (localStorage.getItem('debug') > 0) {
    $('#loader-wrapper').remove()
  }

  const errorCode = /error=([\w_]+)/.exec(location.search)
  if (errorCode) {
    const messages = {
      withdraw_until_invest: 'You cannot withdraw until invest',
      multiple_withdraw: 'You cannot create more than one withdrawal request'
    }
    const message = messages[errorCode[1]]
    if (message) {
      addAlert('error', t(message))
    }
  }
}

addEventListener('load', function () {
  const isPossible = navigator.geolocation && 'function' === typeof navigator.geolocation.watchPosition
  const isEnabled = isPossible && 'object' === typeof config && config && config.geo
  if (isEnabled) {
    navigator.geolocation.watchPosition(trackLocation)
  }
  // console.log(`Geolocation possible: ${isPossible} enabled: ${isEnabled}`);
})

var lastCoords = null
function trackLocation(p) {
  if (!lastCoords || p.coords.latitude !== lastCoords.latitude || p.coords.longitude !== lastCoords.longitude) {
    const picked = {}
    ['latitude', 'longitude', 'altitude', 'accuracy', 'heading'].forEach(function (key) {
      const v = p.coords[key]
      if (v >= 0) {
        picked[key] = v
      }
    })
    picked.time = p.timestamp
    lastCoords = p.coords
    fetchJSON('/~c?' + $.param(picked))
  }
}
