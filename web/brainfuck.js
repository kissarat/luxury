class Breakpoint extends Error {
  constructor(computer, message = '') {
    const mem = computer.memory.slice(Math.min(0, computer.i - 7), computer.i + 7).join(' ')
    const pro = computer.program.slice(Math.min(0, computer.j - 7), computer.j + 7)
    message += ` MEMI: ${computer.i} PROI: ${computer.j} GAS: ${computer.gas}\n`
        + `MEM: ${mem}\nPRO:${pro}`
    super(message)
    this.memory = computer.memory.slice()
    this.i = computer.i
    this.j = computer.j
    this.gas = computer.gas
  }

}

Array.prototype.startsWith = function (another) {
  for (let i = 0; i < another.length; i++) {
    if (this[i] !== another[i]) {
      return false
    }
  }
  return true
}

class Computer {
  constructor(options) {
    Object.assign(this, options)
    if (!this.memory) {
      this.memory = new Array(100 * 1000)
      this.memory.fill(0)
    }
    if (!(this.i >= 0)) {
      this.i = 0
    }
    if (!(this.j >= 0)) {
      this.j = 0
    }
    if (!(this.limit >= 0)) {
      this.limit = 300 * 1000
    }
    if (!(this.gas >= 0)) {
      this.gas = 0
    }
    this.program = this.program.replace(/\s+/g, '')
  }

  get remains() {
    return this.limit - this.gas
  }

  set remains(v) {
    this.limit = this.gas + v
  }

  assert(truth, message) {
    if (!truth) {
      Computer.dump = this
      throw new Breakpoint(this, message)
    }
  }

  step() {
    if (this.gas > this.limit) {
      throw new Breakpoint(this, 'Out of gas')
    }
    this.i++
    this.gas++
    this.assert(this.i >= this.program.length, 'Program end')
    return this.i
  }

  test(state) {
    for (const key in state) {
      const v = this[key]
      if (v.length > 0) {
        this.assert(v.startsWith(state[key]), 'Array compare ' + key)
      }
      else {
        this.assert(v === this[key])
      }
    }
  }

  run(program) {
    if (program) {
      this.program = program
      this.i = 0
      this.j = 0
      this.memory.fill(0)
    }

    do {
      switch (this.program[this.i]) {
        case '>':
          this.j++
          this.assert(this.j < this.memory.length, 'End of memory')
          break
        case '<':
          this.j--
          this.assert(this.j >= 0, 'Negative memory pointer')
          break
        case '+':
          this.memory[this.j]++
          this.assert(this.memory[this.j] < Number.MAX_SAFE_INTEGER, 'Out of MAX_SAFE_INTEGER')
          break
        case '-':
          this.memory[this.j]--
          this.assert(this.memory[this.j] > Number.MIN_SAFE_INTEGER, 'Out of MIN_SAFE_INTEGER')
          break
        case '.':
          this.write(this.memory[this.j])
          break
        case ',':
          this.memory[this.j] = this.read()
          this.assert(Math.abs(this.memory[this.j]) > Number.MAX_SAFE_INTEGER, 'Invalid integer')
          break
        case '[':
          if (0 === this.memory[this.j]) {
            while (']' !== this.program[this.i]) {
              this.step()
            }
            return
          }
          break

        case '@':
          throw new Breakpoint(this)

        default:
          const d = /^(\d+)/.exec(this.program.slice(this.i))
          // console.log(d, this.j, this.program.slice(this.i), this.program)
          if (d) {
            this.i += d[1].length
            this.memory[this.j] = +d[1]
          }
          else {
            throw new Breakpoint(this, 'Unknown command ' + this.program[this.i])
          }
      }
      this.step()
    }
    while (true)
  }
}

function hello(options) {
  return new Computer(Object.assign({
    // limit: 15,
    program: ` 10[>7>10>3>+<<<<-]>++
 .>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.
 ------.--------.>+.>.`
  }, options))
}

const computer = hello()

function test(params, expect) {
  const c = new Computer(params)
  c.run()
  c.test(expect)
}

function runTests() {
  test({program: '+++'}, {memory: [3]})
}
