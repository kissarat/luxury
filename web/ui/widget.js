if ('function' !== typeof Promise && 'function' !== typeof fetch) {
  throw Error('ICO.Widget is not support your browser. You can use one of these browser http://caniuse.com/#search=fetch\n'
      + 'Your browser is "' + navigator.userAgent + '"')
}

const ICO_BASE_DEFAULT = '//office.ico-holding.com/ui'

const ICO_START = 2
const ICO_NORMAL = 3
const ICO_MEDIUM = 4
const ICO_VIP = 5
const ICO_PRO = 6

const ICO = {
  v: 1.1,
  base: ICO_BASE_DEFAULT,
  libs: {
    // materialize: '/css/materialize.css',
    vue: '/lib/vue/vue.min.js',
    moment: '/lib/moment/moment.min.js'
  },
  debug: false,
  instances: [],
  jsonpNumber: 0,
  ALERT: Object.freeze({
    SUCCESS: 'success',
    ERROR: 'danger'
  }),
  CONTENT_TYPE: Object.freeze({
    JSON: 'application/json; charset=utf-8',
    QUERYSTRING: 'application/x-www-form-urlencoded; charset=utf-8',
  }),

  get major() {
    return Math.floor(ICO.v)
  },

  get minor() {
    return +ICO.v.toString().split('.')[1] || 0
  },

  get origin() {
    return ICO.parseOrigin(this.base)
  },

  get host() {
    return ICO.parseHost(this.base)
  },

  get locale() {
    var lang = /(locale|language)=(\w+)/.exec(document.cookie)
    lang = lang ? lang[2] : navigator.language.slice(0, 2)
    return ['en', 'ru'].indexOf(lang) >= 0 ? lang : 'en'
  },

  get tz() {
    return ICO.started.getTimezoneOffset()
  }
}

ICO.assign = function assign(options) {
  Object.assign(this, options)
}

ICO.translate = function t(message, vars) {
  return (ICO.messages[message] || message).replace(/{([\w_]+)}/g, function (s, name) {
    return vars[name] || name
  })
}

ICO.deep = function deep(target, source, i) {
  if (!(i >= 0)) {
    i = 0
  }
  if (!(i < 512)) {
    throw new Error('Object too deep: ' + i)
  }
  for (var key in source) {
    const value = source[key]
    switch (typeof value) {
      case 'object':
        if (value) {
          target[key] = ICO.deep(target[key] || Object.create(null), value, 1 + i)
          break
        }
      default:
        target[key] = value
    }
  }
  return target
}

Object.defineProperty(ICO, 'internal', {
  enumerable: false,
  writable: false,
  value: {}
})

ICO.getRate = function getRate(currency) {
  return {
    USD: 100,
    BTC: 1000 * 1000,
    ETH: 100 * 1000,
  }[currency]
}

ICO.throwError = function throwError(self, message) {
  throw Error(message.replace('{widget}', 'ICO.' + (self.constructor ? self.constructor.name || self.name : 'Widget')))
}

ICO.parseOrigin = function parseOrigin(url) {
  try {
    return /^((\w+:)?\/\/[^\/]+)/.exec(url)[1]
  }
  catch (ex) {
    ICO.throwError(this, 'Cannot get origin of url ' + url + ' in {widget}')
  }
}

ICO.parseHost = function parseHost(url) {
  try {
    return /^(\w+:)?\/\/([^\/]+)/.exec(url)[2]
  }
  catch (ex) {
    ICO.throwError(this, 'Cannot get origin of url ' + url + ' in {widget}')
  }
}

ICO.params = function params(o) {
  const array = []
  for (var key in o) {
    array.push(encodeURIComponent(key) + '=' + encodeURIComponent(o[key]))
  }
  return array.join('&')
}

ICO.getScript = function getScript(url) {
  const script = document.createElement('script')
  script.src = url
  document.head.appendChild(script)
  return script
}

ICO.input = function input(name, value, type) {
  if (!type) {
    type = 'hidden'
  }
  const input = document.createElement('input')
  input.setAttribute('name', name)
  input.value = value
  input.setAttribute('type', type)
  return input
}

ICO.form = function (action, o) {
  const form = document.createElement('form')
  form.setAttribute('action', action)
  for (var name in o) {
    form.appendChild(ICO.input(name, o[name]))
  }
  document.body.appendChild(form)
  return form
}

ICO.alert = function ICO_alert(type, message) {
  if (ICO.ALERT.ERROR === type) {
    message = 'Error: ' + message
  }
  alert(message)
}

ICO.internal.promise = function loadPromise(element) {
  const programs = this.programs
  return new Promise(function (resolve, reject) {
    if (programs && 'object' === typeof programs) {
      resolve(programs)
    }
    else {
      element.addEventListener('load', resolve)
      element.addEventListener('error', reject)
    }
  })
}

// UID:eid2aigohxiero8eiMooH6ci
ICO.internal.track = function track() {
  const uid = /ico=([\w_~]+)/.exec(document.cookie)
  if (uid && uid[1].length > 5 && uid[1].indexOf(' ') < 0) {
    ICO.uid = uid[1]
  }
  else {
    ICO.uid = ICO.started.getTime().toString(36)
        + '_' + Math.round(Math.random() * 36 * 36 * 36 - 1).toString(36)
    if (ICO.sponsor) {
      ICO.uid += '~' + ICO.sponsor
    }
    document.cookie = 'ico=' + ICO.uid + '; path=/; max-age=' + (3600 * 24 * 365 * 10)
  }

  if ('function' === typeof navigator.sendBeacon) {
    addEventListener('beforeunload', ICO.internal.report)
  }
  // else {
  //   setTimeout(ICO.internal.report, 50)
  // }
}

ICO.internal.report = function report() {
  const spend = Date.now() - ICO.started.getTime()
  const url = btoa(location.href.replace(/https?:/, ''))
  navigator.sendBeacon((/\.local$/.test(location.hostname) ? location.origin : ICO.origin)
      + '/~v/' + ICO.uid + '/' + url + '/' + spend)
}

ICO.started = new Date()
if (!navigator.doNotTrack) {
  setTimeout(ICO.internal.track, 0)
}

ICO.internal.getURL = function getURL(url, params) {
  url = this.origin + url
  if (params) {
    url += '?' + ICO.params(params)
  }
  return url
}

ICO.internal.fetch = function internalFetch(url, options) {
  const defaults = {
    credentials: 'include',
    headers: {
      Accept: ICO.CONTENT_TYPE.JSON
    }
  }
  if (options) {
    options = ICO.deep(defaults, options)
    if (options.body) {
      if (!options.method) {
        options.method = 'POST'
      }
      if (options.body) {
        // options.body = ICO.params(options.body)
        // options.headers['content-type'] = ICO.CONTENT_TYPE.QUERYSTRING
        options.body = JSON.stringify(options.body)
        options.headers['content-type'] = ICO.CONTENT_TYPE.JSON
      }
    }
  }
  return fetch(ICO.internal.getURL.call(this, url), options)
      .then(function (r) {
        return r.json()
      })
}

ICO.loadLibraries = function loadLibraries(libs) {
  const self = this
  if ('string' === typeof libs) {
    libs = libs.split(',')
  }
  else if (false !== libs) {
    libs = Object.keys(ICO.libs)
  }
  if (libs instanceof Array) {
    const o = {}
    libs.forEach(function (lib) {
      o[lib] = self.origin + ICO.libs[lib]
    })
    libs = o
  }

  if (libs) {
    const promises = []
    for (var lib in libs) {
      const url = libs[lib]
      const ext = /\.(\w+)$/.exec(url)[1]
      switch (ext) {
        case 'js':
          if (!document.querySelector('script[src="' + url + '"]')) {
            promises.push(ICO.internal.promise(ICO.getScript(url)))
          }
          break

        case 'css':
          if (!document.querySelector('link[href="' + url + '"]')) {
            const link = document.createElement('link')
            link.href = url
            link.setAttribute('rel', 'stylesheet')
            link.setAttribute('type', 'text/css')
            link.setAttribute('media', 'screen,projection')
            document.head.appendChild(link)
          }
          break

        default:
          ICO.throwError(this, 'Resource ' + lib + ' with unknown type ' + url + ' in {widget}')
      }
    }
    return Promise.all(promises)
  }
}

ICO.jsonp = function jsonp(url, params) {
  const self = this
  if (!params) {
    params = {}
  }
  if (!params.callback) {
    params.callback = 'ico' + ++ICO.jsonpNumber
  }
  return new Promise(function (resolve, reject) {
    window[params.callback] = function icoJSONP() {
      resolve.apply(window, arguments)
      const desc = Object.getOwnPropertyDescriptor(window, params.callback)
      if (desc && desc.configurable) {
        delete window[params.callback]
      }
    }
    const script = ICO.getScript(ICO.internal.getURL.call(self, url, params))
    script.addEventListener('error', reject)
  })
}

ICO.internal.init = function init(options) {
  if ('string' === typeof options.el) {
    options.el = document.querySelector(options.el)
  }
  if (options.host) {
    options.base = '//' + options.host + '/ui'
  }
  if (!options.base) {
    options.base = ICO.base
  }
  Object.assign(this, options)
  return ICO.loadLibraries.call(this, options.libs)
}

ICO.internal.debugInvoke = function (o) {
  // for (var key in o) {
  //   const old = o[key]
  //   o[key] = function () {
  //     console.log('CALL: ' + key)
  //     try {
  //       return old.apply(this, arguments)
  //     }
  //     catch (ex) {
  //       console.error(ex)
  //     }
  //   }
  // }
  return o
}

function Widget(options, defaults) {
  const self = this
  if (defaults) {
    options = Object.assign(defaults, options)
  }
  // console.log(this, options, defaults)
  ICO.internal.init.call(this, options)
      .then(function () {
        return self.load()
      })
      .then(function () {
        if (self.debug) {
          ICO.instances.push(self)
        }

        [].forEach.call(document.forms, function (form) {
          form.addEventListener('submit', function (e) {
            e.preventDefault()
          })
        })
      })
      .catch(function (err) {
        if (err instanceof Event && err.target instanceof HTMLScriptElement) {
          self.alert(ICO.ALERT.ERROR, 'Cannot load: ' + err.target.src)
        }
        console.error(err)
      })
}

Widget.create = function create(options) {
  return new ICO.widgets[options.type](options)
}

Widget.prototype = {
  set sponsor(v) {
    ICO.sponsor = v
  },

  get sponsor() {
    return ICO.sponsor
  },

  get debug() {
    return ICO.debug
  },

  set debug(value) {
    ICO.debug = ICO.debug || !!value
  },

  get html() {
    return ICO.html[this.type]
  },

  get isJSONP() {
    return this.html.length > 100
  },

  get origin() {
    return ICO.parseOrigin(this.base)
  },

  fetch: function () {
    return ICO.internal.fetch.apply(this, arguments)
  },

  setup: function (options) {
    return this.component = new Vue(options)
  },

  alert: function (type, message) {
    ICO.alert(type, message)
  },

  catch: function (ex) {
    this.alert(ICO.ALERT.ERROR, ex.message || ex.toString())
  },

  postLoad: function () {
    if (!ICO.messages) {
      ICO.messages = ICO.locales[ICO.locale] || {}
    }
    const style = this.el.querySelector('style')
    if (style.id) {
      if (document.head.querySelector('#' + style.id)) {
        style.remove()
      }
      else {
        document.head.appendChild(style)
      }
    }
  },

  load: function () {
    const self = this
    if (this.isJSONP) {
      this.el.innerHTML = this.html
      this.postLoad()
      return new Promise.resolve()
    }
    else {
      return fetch(this.base + '/' + this.type + '.html')
          .then(function (r) {
            return r.text()
          })
          .then(function (r) {
            self.el.innerHTML = r
            if (!ICO.locales[ICO.locale]) {
              if ('en' !== ICO.locale) {
                ICO.locales[ICO.locale] = 'undefined' === typeof _ICO_messages
                    ? ICO.jsonp.call(self, '/locale/' + ICO.locale + '.js', {callback: 'ICO_setLocale'})
                    : _ICO_messages
              }
            }
            self.postLoad()
          })
    }
  }
}

ICO.Widget = Widget

// ---------- Calculator ----------

function Calculator(options) {
  options.type = 'calculator'
  Widget.call(this, options)
  ICO.calculator = this
}

Calculator.prototype = Object.create(ICO.Widget.prototype)
Object.assign(Calculator.prototype, {
  loadPrograms: function () {
    const self = this
    return ICO.jsonp.call(this, '/program/index').then(function (r) {
      self.programs = r.result
      return r.result
    })
  },

  alert: function (type, message) {
    if (this.component) {
      if (type) {
        this.component.alert = {type: type, message: message}
      }
      else {
        this.component.alert = {type: '', message: ''}
      }
    }
    else {
      Widget.prototype.alert.call(this, type, message)
    }
  },

  load: function () {
    const self = this
    Widget.prototype.load.call(this)
        .then(function () {
          return self.loadPrograms()
        })
        .then(function (result) {
          self._load(result)
        })
  },

  _load: function (result) {
    const widget = this
    for (var i in result) {
      const program = result[i]
      if (!program.reservation) {
        program.reservation = false
      }
    }

    if ('function' === typeof ICO_PRO_render) {
      ICO_PRO_render(result[ICO_PRO])
    }
    delete result[ICO_PRO]

    const data = {
      programs: result,
      amount: 0,
      day: 1,
      buy: this.buy || false,
      active: {
        program: 0,
        currency: '',
      },
      alert: {
        type: '',
        message: ''
      }
    }
    if (this.currency && this.program > 0) {
      data.active.currency = this.currency
      data.active.program = this.program
      data.amount = result[this.program].min[this.currency] / ICO.getRate(this.currency)
    }

    return this.setup({
      el: '#ico-calc',
      data: data,
      methods: ICO.internal.debugInvoke({
        formatAmount: function (value) {
          value = +(value.replace(',', '.'))
          if (isFinite(value)) {
            this.amount = +value.toFixed(3)
            if (!this.validAmount) {
              const id = this.appropriateProgram
              if (id > 0) {
                this.select(id)
              }
            }
          }
        },

        select: function (program, currency) {
          this.active.program = program
          if (currency) {
            this.active.currency = currency
          }
          if (program && this.active.currency) {
            this.day = this.selected.quantity
            if (!this.validAmount) {
              this.amount = this.min / this.rate
            }
          }
        },

        selectDate: function (day) {
          if (day >= 0) {
            this.day = day
          }
        },

        getCurrencies: function (program) {
          const self = this
          const currencies = [];
          ['ETH', 'BTC', 'USD'].forEach(function (currency) {
            const dates = self.programs[program].reservation[currency]
            if (dates) {
              for (var date in dates) {
                if (dates[date] > 0) {
                  currencies.push(currency)
                  return
                }
              }
            }
          })
          return currencies
        },

        hasReserve: function (program, currency) {
          if (this.programs[program].reservation) {
            const dates = this.programs[program].reservation[currency]
            if (dates) {
              for (var date in dates) {
                if (dates[date] > 0) {
                  return true
                }
              }
            }
          }
          return false
        },

        t: function (message, vars) {
          return ICO.translate.call(this, message, vars)
        },

        reserve: function (date, program, amount, currency) {
          if (!(program > 0)) {
            program = this.active.program
          }
          if (!(amount > 0)) {
            amount = this.cent
          }
          if (!currency) {
            currency = this.active.currency
          }
          widget.fetch('/program/buy', {
            method: 'POST',
            body: {
              program: program,
              amount: amount,
              currency: currency,
              date: date,
              tz: ICO.tz
            }
          })
              .then(function (r) {
                if (402 === r.status) {
                  location.href = '/pay/' + r.nick + '?' + ICO.params({currency: currency, amount: r.need})
                }
                else if (r.success && r.id > 0) {
                  widget.alert(ICO.ALERT.SUCCESS, r.message)
                  location.href = '/history/' + r.nick + '?id=' + r.id
                }
                else if (r.error && 'string' === typeof r.error.message) {
                  widget.alert(ICO.ALERT.ERROR, r.error.message)
                }
                else {
                  var m = (r.message || 'Unknown server response')
                      + '. Report the support about problem, please'
                  if (r['stack-trace'] instanceof Array) {
                    m += '\n' + r['stack-trace'].join('\n')
                  }
                  widget.alert(ICO.ALERT.ERROR, m)
                }
              })
              .catch(function (ex) {
                widget.catch(ex)
              })
        }
      }),

      computed: ICO.internal.debugInvoke({
        selected: function () {
          return this.active.currency && this.programs[this.active.program]
        },

        reservation: function () {
          return this.programs[this.active.program].reservation[this.active.currency]
        },

        accrue: function () {
          const rate = this.rate
          const accrue = this.amount * this.selected.accrue * this.day
          const fixed = accrue.toFixed(Math.log10(rate))
          return 'USD' === this.active.currency ? fixed : +fixed
        },

        rate: function () {
          return ICO.getRate(this.active.currency)
        },

        cent: function () {
          return this.amount * this.rate
        },

        min: function () {
          return this.selected.min[this.active.currency]
        },

        max: function () {
          return this.selected.max[this.active.currency]
        },

        start: function () {
          return moment()
        },

        end: function () {
          const m = this.start.clone()
          m.add(this.selected.quantity - 1, 'day')
          return m
        },

        refundDay: function () {
          const m = this.start.clone()
          m.add(this.selected.quantity, 'day')
          return m.format('YYYY-MM-DD')
        },

        hasRefund: function () {
          return this.day >= this.selected.quantity
        },

        validAmount: function () {
          const amount = this.cent
          return isFinite(amount) && this.min <= amount && amount < this.max
        },

        appropriateProgram: function () {
          const amount = this.cent
          for (var id in this.programs) {
            const p = this.programs[id]
            if (p.min[this.active.currency] <= amount && amount < p.max[this.active.currency]) {
              return id
            }
          }
        },

        today: function () {
          return this.start.format('YYYY-MM-DD')
        },

        date: function () {
          const m = this.start.clone()
          m.add(this.day, 'day')
          return m.format('YYYY-MM-DD')
        },

        profitMessage: function () {
          return this.t(
              this.hasRefund
                  ? 'The profit for {day} days is {profit}, the return of the deposit amount is {refund} on the {refund_day} day. Total income {total}.'
                  : 'The profit for {day} days is {profit}, the return of the deposit amount is {refund} on the {refund_day} day.', {
                day: this.day,
                refund_day: this.selected.quantity + 1,
                profit: this.accrue,
                refund: this.amount,
                total: this.summary,
              })
              .replace(/(\d+)/g, '<span class="number integer"><span class="colors-1">$1</span></span>')
              .replace(/(\d+)\.(\d+)/g, '<span class="number rational"><span class="colors-1">$1</span><span class="colors-1">$2</span></span>')
              .replace(/(.+),(.+)/g, '<div>$1,</div><div>$2</div>')
              .replace(/(.+)\.(.+)/g, '<div>$1.</div><div>$2</div>')
        },

        summary: function () {
          return +(+this.amount + +this.accrue).toFixed(6)
        },

        months: function () {
          const array = {}
          const start = this.start.clone().add(1, 'day')
          const first = new Array(start.date() - 1)
          first.fill(['00', '00', 'empty'])
          array[moment.months(start.month())] = first
          const end = start.clone().add(this.selected.quantity - 1, 'day')
          const scale = Math.log10(this.rate)
          const amount = +this.amount
          var accrue = 0
          for (var i = start; i.isBefore(end) || i.isSame(end); i.add(1, 'day')) {
            const month = moment.months(i.month())
            if (!array[month]) {
              array[month] = []
            }
            accrue += amount * this.selected.accrue
            array[month].push([
              i.format('DD'),
              accrue.toFixed(scale),
              i.format('YYYY-MM-DD'),
              -this.start.diff(i, 'day')
            ])
          }
          return array
        }
      })
    })
  }
})

ICO.Calculator = Calculator

ICO.widgets = {
  calculator: ICO.Calculator
}

ICO.html = {
  calculator: "<calculator>"
}

function ICO_setLocale(messages) {
  ICO.messages = messages
}

ICO.locales = {}
