<?php

namespace app\models\traits;

trait DepositTrait {
    private $_range;

    protected function getIntegerRange() {
        if (!$this->_range) {
            preg_match('|^.(\d+),(\d+).$|', $this->range, $this->_range);
        }
        return $this->_range;
    }

    public function getMin() {
        return $this->getIntegerRange()[1];
    }

    public function getMax() {
        return ($this->getIntegerRange()[2]) - 1;
    }
}
