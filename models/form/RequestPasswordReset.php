<?php

namespace app\models\form;


use app\models\User;
use Yii;
use yii\base\Model;

class RequestPasswordReset extends Model {
    public $email;
    public $nick;

    public function attributeLabels() {
        return [
            'nick' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email')
        ];
    }

    public function rules() {
        return [
            [['nick', 'email'], 'exist',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'User not found')],
        ];
    }

    /**
     * @return bool|User
     */
    public function getUser() {
        if ($this->email) {
            return User::findOne(['email' => $this->email]);
        }
        elseif ($this->nick) {
            return User::findOne(['nick' => $this->nick]);
        }
        return false;
    }
}
