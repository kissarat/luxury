<?php

namespace app\models\form;


use Yii;
use yii\base\Model;

class Internal extends Model
{
    public function rules() {
        return [
            [['amount', 'nick', 'currency'], 'required'],
            ['sponsor', 'exist',
                'targetClass' => 'app\models\User',
                'targetAttribute' => 'nick',
                'message' => Yii::t('app', 'Receiver not found')],
        ];
    }

    public function attributeLabels() {
        return [
            'amount' => Yii::t('app', 'Amount'),
            'currency' => Yii::t('app', 'Currency'),
            'nick' => Yii::t('app', 'Receiver'),
        ];
    }
}
