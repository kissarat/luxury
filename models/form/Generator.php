<?php

namespace app\models\form;


use app\models\ProgramDeposit;
use app\models\Transfer;
use app\models\User;
use Faker\Factory;
use yii\base\Model;

class Generator extends Model {
    public $sponsor;
    public $count;
    public $usd;
    public $btc;
    public $eth;
    public $created;

    public function rules() {
        return [
            [['sponsor', 'count'], 'required'],
            [['usd', 'btc', 'eth'], 'number'],
            ['created', 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    public function generate() {
        $factory = Factory::create();
        $parent = User::findOne(['nick' => $this->sponsor]);
        $users = [];
        for ($i = 0; $i < $this->count; $i++) {
            $user = new User([
                'nick' => $factory->userName,
                'email' => $factory->email,
                'forename' => $factory->firstName,
                'surname' => $factory->lastName,
                'parent' => $parent->id,
                'type' => 'native',
                'secret' => '$2a$10$SVKR9IIDXZtYgOAG01XQy.VW6amNKhySHeKrB/bMWto88oikeQ7l6'
            ]);
            if ($user->save()) {
                $users[$user->id] = $user;
            } else {
                $errors = $user->getErrors();
                foreach ($errors as $name => $error) {
                    $errors[$name] = [
                        'value' => $user->$name,
                        'message' => $error
                    ];
                }
            }
        }
        $rates = Transfer::getExchange();
        $results = [];
        foreach ($users as $id => $user) {
            foreach (['usd', 'btc', 'eth'] as $currency) {
                $c = strtoupper($currency);
                if ($this->$currency > 0) {
                    $amount = $this->$currency * $rates[$c][$c];
                    $transfer = new Transfer([
                        'to' => $id,
                        'type' => 'payment',
                        'status' => 'success',
                        'amount' => $amount,
                        'currency' => $c
                    ]);
                    if ($transfer->save()) {
                        if ($this->created) {
                            $result = ProgramDeposit::buy($id, $amount, $c, $this->created);
                            if ($result <= 0) {
                                $results[$user->nick][$c] = [
                                    'status' => $result,
                                    'amount' => $amount
                                ];
                            }
                        }
                    } else {
                        return $transfer->getErrors();
                    }
                }
            }
        }
        if (count($results) > 0) {
            return $results;
        }
    }
}
