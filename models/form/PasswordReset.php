<?php

namespace app\models\form;


use Yii;
use yii\base\Model;

class PasswordReset extends Model {
    public $password;
    public $repeat;
    public $ip;

    public function rules() {
        return [
            [['password', 'repeat'], 'required'],
            ['password', 'compare', 'compareAttribute' => 'repeat'],
            ['ip', 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'password' => Yii::t('app', 'Password'),
            'repeat' => Yii::t('app', 'Repeat Password')
        ];
    }
}
