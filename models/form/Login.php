<?php

namespace app\models\form;


use app\models\Log;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * @property string $login
 * @property string $password
 * @property User $user
 */
class Login extends Model
{
    public $login;
    public $password;
    public $ip;

//    public $remember;

    public function rules() {
        return [
            [['login', 'password'], 'required'],
            ['login', 'string', 'min' => 4, 'max' => 48],
            ['password', 'string', 'min' => 1],
            ['ip', 'string'],
//            ['remember', 'boolean'],
//            ['remember', 'default', 'value' => true],
        ];
    }

    public function attributeLabels() {
        return [
            'login' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
//            'remember' => Yii::t('app', 'Remember Me'),
        ];
    }

    /**
     * @return User
     */
    public function getUser() {
        return User::findOne(filter_var($this->login, FILTER_VALIDATE_EMAIL)
            ? ['email' => $this->login]
            : ['nick' => $this->login]);
//            ->select(['id', 'secret']);
    }

    public static function checkLoginAttempt($where) {
        $count = Log::find()
            ->where($where)
            ->where(['>', 'id', Log::nanotime() - 15 * 60 * 1000000000])
            ->where([
                'entity' => 'auth',
                'action' => 'invalid'
            ])
            ->count();
        if ($count > 30) {
            Yii::$app->session->addFlash('error',
                Yii::t('app', 'You have exceeded the number of allowed login attempts. Please try again later.'));
            return false;
        }
        return true;
    }
}
