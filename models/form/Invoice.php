<?php

namespace app\models\form;


use app\models\Transfer;
use Yii;
use yii\base\Model;

class Invoice extends Model {
    public $system;
    public $amount;
    public $ip;

    public function rules() {
        return [
            ['amount', 'required'],
            ['system', 'string'],
            ['amount', 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            ['ip', 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'system' => Yii::t('app', 'Payment system')
        ];
    }

    public function getRate() {
        $currency = Transfer::$currencies[$this->system];
        return Transfer::getExchange()[$currency][$currency];
    }
}
