<?php

namespace app\models\form;


use app\models\User;

class Signup extends User
{
    public function rules() {
        $rules = parent::rules();
        $rules[] = ['password', 'required'];
        return $rules;
    }
}