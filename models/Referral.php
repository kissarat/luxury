<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

class Referral extends ActiveRecord {
    public function rules() {
        return [
            [['children', 'level'], 'integer'],
            [['nick', 'surname', 'forename', 'skype', 'email', 'telegram', 'eth', 'sponsor'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'nick' => Yii::t('app', 'Username'),
            'forename' => Yii::t('app', 'First Name'),
            'surname' => Yii::t('app', 'Last Name'),
            'children' => Yii::t('app', 'Team'),
            'sponsor' => Yii::t('app', 'Sponsor'),
            'eth' => Yii::t('app', 'Invested {currency}', ['currency' => 'ETH']),
            'btc' => Yii::t('app', 'Invested {currency}', ['currency' => 'BTC']),
            'usd' => Yii::t('app', 'Invested {currency}', ['currency' => 'USD']),
            'telegram' => Yii::t('app', 'Telegram'),
            'created' => Yii::t('app', 'Registered'),
        ];
    }

    public static function tableName() {
        return 'referral_sponsor';
    }
}
