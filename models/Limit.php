<?php

namespace app\models;


use app\traits\BroadcastModelTrait;
use yii\db\ActiveRecord;

class Limit extends ActiveRecord
{
    use BroadcastModelTrait;
}
