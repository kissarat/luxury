<?php

namespace app\models;


use app\traits\BroadcastModelTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Node
 * @property int $id
 * @property int|Program $program
 * @property int $money
 * @property int $user
 * @package app\models
 */
class Node extends ActiveRecord
{
    use BroadcastModelTrait;

    public static function tableName() {
        return 'node';
    }

    public function getProgram() {
        return $this->hasOne(Program::class, ['id' => $this->program]);
    }

    public function getCurrencyProgram($currency) {
        return $this->hasOne(ProgramDeposit::class, [
            'id' => $this->program,
            'currency' => $currency
        ]);
    }

    public function getBuy() {
        return $this->hasOne(Transfer::class, [
            'node' => $this->id,
            'type' => 'buy'
        ]);
    }

    public function attributeLabels() {
        return [
            'amount' => Yii::t('app', 'Amount'),
            'accrues' => Yii::t('app', 'Accrues'),
            'profit' => Yii::t('app', 'Profit'),
            'currency' => Yii::t('app', 'Currency'),
            'closing' => Yii::t('app', 'Closing'),
            'created' => Yii::t('app', 'Invested')
        ];
    }
}
