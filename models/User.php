<?php

namespace app\models;


use app\helpers\SQL;
use app\traits\BroadcastModelTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class User
 * @property boolean $admin
 * @property integer $id
 * @property integer $parent
 * @property string $nick
 * @property string $type
 * @property string $surname
 * @property string $forename
 * @property string $email
 * @property string $skype
 * @property string $secret
 * @property string $password
 * @property string $phone
 * @property string $country
 * @property string $settlement
 * @property string $address
 * @property string $blockio
 * @property string $ethereum
 * @package app\models
 */
class User extends ActiveRecord
{
    use BroadcastModelTrait;

    public $password;
    public $sponsor;
    public $level;
    public $ip;
    public $avatar_file;

    public static function tableName() {
        return 'user';
    }

    public function rules() {
        return [
            ['id', 'integer'],
            [['nick', 'email'], 'required'],
            [['surname', 'forename'], 'string', 'min' => 2, 'max' => 48],
            ['nick', 'string', 'min' => 4, 'max' => 24],
            ['nick', 'match', 'pattern' => '/^[a-z][a-z0-9_]+$/i'],
            ['email', 'email'],
            ['ip', 'ip'],
            [['ethereum', 'telegram'], 'string'],
            ['perfect', 'match', 'pattern' => '/^U\d{7,8}$/', 'message' => 'Format must be U1234567'],
            ['skype', 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$/'],
            [['surname', 'forename', 'nick', 'email', 'skype', 'perfect', 'sponsor', 'password', 'phone',
                'country', 'settlement', 'address', 'avatar', 'telegram'], 'filter', 'filter' => 'trim'],
            [['surname', 'forename', 'nick', 'email', 'skype', 'perfect', 'sponsor', 'password', 'phone',
                'country', 'settlement', 'address', 'avatar', 'telegram'], 'filter', 'filter' => 'strip_tags'],
            [['surname', 'forename', 'nick', 'email', 'skype', 'perfect', 'sponsor', 'password', 'phone',
                'country', 'settlement', 'address', 'avatar', 'telegram'], 'filter', 'filter' => function ($s) {
                return $s ?: null;
            }],
            [['nick', 'email', 'skype', 'perfect'], 'unique',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'This value has already been taken')],
            ['sponsor', 'exist',
//                'targetClass' => 'app\models\User',
                'targetAttribute' => 'nick',
                'message' => Yii::t('app', 'Sponsor not found')],
        ];
    }

    public function attributeLabels() {
        return [
            'address' => Yii::t('app', 'Address'),
            'avatar_file' => Yii::t('app', 'Avatar'),
            'country' => Yii::t('app', 'Country'),
            'email' => Yii::t('app', 'Email'),
            'forename' => Yii::t('app', 'First Name'),
            'nick' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'perfect' => Yii::t('app', 'Perfect Money'),
            'phone' => Yii::t('app', 'Phone'),
            'telegram' => Yii::t('app', 'Telegram'),
            'settlement' => Yii::t('app', 'City'),
            'skype' => Yii::t('app', 'Skype'),
            'sponsor' => Yii::t('app', 'Sponsor'),
            'surname' => Yii::t('app', 'Last Name'),
        ];
    }

    /**
     * @param string $id
     * @return User
     */
    public static function findIdentity($id) {
        return parent::findOne(['id' => $id]);
    }

    public function validatePassword(string $password) {
        return password_verify($password, $this->secret);
    }

    public function generateAuthKey() {
        return $this->generateToken('browser', Yii::$app->security->generateRandomString(24));
    }

    public function generateCode() {
        return $this->generateToken('code', Yii::$app->security->generateRandomString(48));
    }

    public function generateSecret() {
        $this->secret = str_replace('$2y$', '$2a$', password_hash($this->password, PASSWORD_BCRYPT));
    }

    public static function getIdByNick($nick) {
        return static::find()
            ->where(['nick' => $nick])
            ->select(['id'])
            ->one()
            ->id;
    }

    public function generateToken(string $type, $id) {
        if (Yii::$app->db->createCommand()->insert('token', [
            'id' => $id,
            'type' => $type,
            'user' => $this->id,
        ])
            ->execute()
        ) {
            return $id;
        }
        return false;
    }

    public function isAdmin() {
        return $this->admin;
    }

    public function __toString() {
        return $this->nick;
    }

    public function sendEmail($template, $params) {
        $template = Yii::getAlias('@app') . "/mail/$template.php";
        return Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom(['zbyszek@yopmail.com' => 'admin'])
            ->setSubject($params['subject'])
            ->setHtmlBody(Yii::$app->view->renderFile($template, $params))
            ->send();
    }

    public static function getBalance($currency, $id = null) {
        if (null === $id) {
            $id = Yii::$app->user->identity->user;
        }
        return SQL::scalar('SELECT amount FROM balance WHERE "user" = :id AND currency = :currency', [
            ':id' => $id,
            ':currency' => $currency,
        ]);
    }

    public function canLogin() {
        return true;
//        return Record::find()->andWhere([
//            'object_id' => $this->id,
//            'event' => 'login_fail'
//        ])
//            ->andWhere('time > DATE_SUB(NOW(), INTERVAL 5 MINUTE)')
//            ->count() < 10;
    }
}
