<?php

namespace app\models;


use Yii;

class NodeSummary extends Node
{
    public static function tableName() {
         return 'node_summary';
    }

    public function attributeLabels() {
        return array_replace(parent::attributeLabels(), [
            'created' => Yii::t('app', 'Opening'),
            'name' => Yii::t('app', 'Program'),
        ]);
    }
}
