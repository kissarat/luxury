<?php

namespace app\models;


use app\helpers\SQL;
use app\traits\BroadcastModelTrait;
use Yii;
use yii\db\ActiveRecord;
use yii\db\mssql\PDO;
use yii\db\Query;

/**
 * Class Transfer
 * @property integer $id
 * @property integer $from
 * @property integer $to
 * @property integer $amount
 * @property string $type
 * @property string $status
 * @property integer $node
 * @property string $text
 * @property string $wallet
 * @property string $system
 * @property string $currency
 * @property string $txid
 * @property string $ip
 * @property string $created
 * @property string $time
 * @package app\models
 */
class Transfer extends ActiveRecord
{
    use BroadcastModelTrait;

    public static function tableName() {
        return 'transfer';
    }

    public static $systems = [
        'perfect' => 'Perfect Money',
        'blockio' => 'Bitcoin',
        'ethereum' => 'Ethereum',
    ];

    public static $currencies = [
        'perfect' => 'USD',
        'blockio' => 'BTC',
        'ethereum' => 'ETH',
    ];

    public static $types = ['buy', 'payment', 'withdraw', 'accrue', 'bonus'];

    public function rules() {
        return [
            ['from', 'integer'],
            ['to', 'integer'],
            ['amount', 'integer'],
            ['type', 'string'],
            ['status', 'string'],
            ['node', 'integer'],
            ['text', 'string'],
            ['wallet', 'string'],
            ['txid', 'string'],
            ['ip', 'string'],
            ['created', 'datetime'],
            ['time', 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'status' => Yii::t('app', 'Operation'),
            'created' => Yii::t('app', 'Time'),
            'amount' => Yii::t('app', 'Amount'),
            'type' => Yii::t('app', 'Type'),
            'text' => Yii::t('app', 'Comment'),
            'user' => Yii::t('app', 'User'),
        ];
    }

    public static function keyBy($rows, $key = 'currency') {
        $result = [];
        foreach ($rows as $row) {
            $result[$row[$key]] = $row['amount'];
        }
        return $result;
    }

    public static function getFinance($user) {
        $rows = (new Query())->from('finance')
            ->where(['user' => $user])
            ->select(['type', 'currency', 'amount'])
            ->all();
        $finance = [];
        $additional = ['balance', 'turnover', 'daily'];
        foreach (array_merge($additional, static::$types) as $type) {
            $finance[$type] = ['ETH' => 0, 'BTC' => 0, 'USD' => 0];
//            foreach (array_values(static::$currencies) as $currency) {
//                $finance[$type][$currency] = 0;
//            }
        }
        foreach ($rows as $row) {
            $finance[$row['type']][$row['currency']] = $row['amount'];
        }
        foreach ($additional as $key) {
            $amount = SQL::queryAll("SELECT currency, amount FROM $key WHERE currency IS NOT NULL AND \"user\" = :user",
                [':user' => $user], PDO::FETCH_KEY_PAIR);
            if (!empty($amount) ){
                $finance[$key] = $amount;
            }
        }
        return $finance;
    }

    public static function getExchange() {
        $exchange = [];
        foreach (SQL::queryAll('SELECT * FROM exchange') as $row) {
            $exchange[$row['from']][$row['to']] = +$row['rate'];
        }
        return $exchange;
    }
}
