<?php

namespace app\models;


use app\traits\BroadcastModelTrait;
use yii\db\ActiveRecord;

/**
 * Class Token
 * @property string $id
 * @property int $user
 * @property string $type
 * @property string $expires
 * @property array $data
 * @property string $ip
 * @property string $created
 * @property string $time
 * @package app\models
 */
class Token extends ActiveRecord {
    use BroadcastModelTrait;

    public static function tableName() {
        return 'token';
    }

    public static function primaryKey() {
        return ['id'];
    }

    public function rules() {
        return [
            [['id', 'type'], 'string', 'max' => 240],
            [['id', 'type'], 'required']
        ];
    }
}
