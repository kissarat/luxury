<?php

namespace app\models;


use app\helpers\SQL;
use app\models\traits\DepositTrait;
use yii\db\ActiveRecord;

/**
 * Class Program
 * @property int $id
 * @property string $name
 * @property string $price
 * @property float $percent
 * @property float $min
 * @property float $max
 * @property string $currency
 * @package app\models
 */
class ProgramDeposit extends ActiveRecord {
    public $amount;

    use DepositTrait;

    public static function primaryKey() {
        return ['program', 'currency'];
    }

    public static function tableName() {
        return 'program_deposit';
    }

    public function rules() {
        return [
            ['amount', 'required'],
            ['id', 'integer'],
            ['name', 'string'],
            ['price', 'string'],
            ['amount', 'integer', 'integerOnly' => true, 'min' => $this->getMin(), 'max' => $this->getMax()]
        ];
    }

    public static function buy($user, $amount, $currency, $date) {
//        header('Created: ' . $date);
        return SQL::scalar('SELECT buy(:user::INT, :amount::INT8, :currency::currency, :date::TIMESTAMP)', [
            ':user' => $user,
            ':amount' => $amount,
            ':currency' => $currency,
            ':date' => $date
        ]);
    }
}
