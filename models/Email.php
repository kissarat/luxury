<?php

namespace app\models;


use app\helpers\Utils;
use app\traits\BroadcastModelTrait;
use yii\db\ActiveRecord;

class Email extends ActiveRecord
{
    use BroadcastModelTrait;

    public static function tableName() {
        return 'email';
    }

    public static function send($to, $subject, $content) {
        $email = new Email([
            'to' => $to,
            'subject' => $subject,
            'content' => $content,
        ]);
        if ($email->save()) {
            Utils::mail($to, $subject, $content);
            $email->sent = Utils::timestamp();
            if ($email->save(false)) {
                return $email;
            }
        }
        return false;
    }
}
