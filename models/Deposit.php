<?php

namespace app\models;


use app\models\traits\DepositTrait;
use yii\base\Model;

class Deposit extends Model
{
    use DepositTrait;
}
