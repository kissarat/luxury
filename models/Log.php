<?php

namespace app\models;


use app\traits\BroadcastModelTrait;
use Yii;
use yii\db\ActiveRecord;

class Log extends ActiveRecord
{
    use BroadcastModelTrait;

    public static function tableName() {
        return 'log';
    }

    const IP = 'XIP';

    public static function nanotime() {
        return (int)floor(microtime(true) * 10000) * 100000;
    }

    public static function log($entity, $action, $data = null, $user = null, $ip = null) {
        $id = static::nanotime();
        $u = isset(Yii::$app->user) ? Yii::$app->user : null;
        if (!$ip && isset(Yii::$app->request->getUserIP)) {
            $ip = Yii::$app->request->getUserIP();
        }
        if (!empty($data) && isset($_SERVER[static::IP]) && $_SERVER[static::IP] != $ip) {
            $data['ip'] = $_SERVER[static::IP];
        }
        if (empty($data['ip']) || $data['ip'] === $ip) {
            unset($data['ip']);
        }
        $log = new Log([
            'id' => $id,
            'entity' => $entity,
            'action' => $action,
            'data' => empty($data) ? null : json_encode($data),
            'token' => isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : null,
            'user' => $user ?: ($u && $u->getIsGuest() ? null : $u->identity->user),
            'ip' => $ip
        ]);
        if ($log->save(false)) {
            return $log;
        }
        return false;
    }
}
