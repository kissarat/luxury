<?php

namespace app\models;


use app\traits\BroadcastModelTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Program
 * @property int $id
 * @property string $name
 * @property float $accrue
 * @property float $min
 * @property float $max
 * @property string $currency
 * @package app\models
 */
class Program extends ActiveRecord
{
    use BroadcastModelTrait;

    public $amount;

    public function attributeLabels() {
        return [
            'amount' => Yii::t('app', 'Investment amount')
        ];
    }

    public static function tableName() {
        return 'program';
    }
}
