<?php

namespace app\models;


use app\traits\BroadcastModelTrait;
use yii\db\ActiveRecord;

/**
 * Class Program
 * @property int $id
 * @property string $name
 * @property float $accrue
 * @property float $min
 * @property float $max
 * @property string $currency
 * @package app\models
 */
class Program extends ActiveRecord
{
    use BroadcastModelTrait;

    public static function tableName() {
        return 'program';
    }
}
