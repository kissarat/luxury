<?php

namespace app\models;


use app\traits\BroadcastModelTrait;
use yii\db\ActiveRecord;

class Coordinate extends ActiveRecord
{
    use BroadcastModelTrait;

    public static function tableName() {
        return 'coordinate';
    }
}
