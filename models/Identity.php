<?php

namespace app\models;


use app\helpers\Utils;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property string $authKey
 * @property string $id
 * @property int $user
 * @property string $type
 * @property string $expires
 * @property array $data
 * @property string $ip
 * @property string $created
 * @property string $time
 * @property string $email
 * @property string $nick
 * @property boolean $admin
 * @property string $avatar
 * @property int $parent
 */
class Identity extends ActiveRecord implements IdentityInterface {
    public static function primaryKey() {
        return ['id'];
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return Identity the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id) {
        $identity = static::findOne(['id' => $id]);
        if ($identity && $identity->user) {
            return $identity;
        }
        return null;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne([
            'id' => $token,
            'type' => $type
        ]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey() {
        return $this->id;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey) {
        return $this->id === $authKey;
    }

    public static function identify($id, $type = 'browser') {
        $identity = static::findOne(['id' => $id]);
        if ($identity) {
            return $identity;
        }
        Yii::$app->db
            ->createCommand()
            ->insert('token', [
                'id' => $id,
                'type' => $type,
                'ip' => Yii::$app->request->getUserIP(),
                'name' => Yii::$app->request->getUserAgent(),
                'expires' => date('Y-m-dTh:00:00.00Z', time() + 30 * 24 * 3600)
            ])
            ->execute();
        return static::findOne(['id' => $id]);
    }

    public static function identifyCookie() {
        $session = Yii::$app->session;
        $session->open();
        return static::identify($session->id);
    }

    public function getToken() {
        return $this->hasOne(Token::class, ['id' => $this->id]);
    }

    public function login($user) {
        Yii::$app->db
            ->createCommand()
            ->update('token', [
                'user' => $user,
                'time' => Utils::timestamp(),
                'ip' => Yii::$app->request->getUserIP(),
                'name' => Yii::$app->request->getUserAgent()
            ],
                ['id' => $this->getId()])
            ->execute();
        if ($this->refresh()) {
            return $this;
        }
        return null;
    }

    public function logout() {
        return $this->login(null);
    }

    public function isAdmin() {
        return $this->admin;
    }
}
