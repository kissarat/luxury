<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\form\Internal;
use app\models\form\Payment;
use app\models\form\Withdraw;
use app\models\Log;
use app\models\Transfer;
use app\models\User;
use Yii;

class InvoiceController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['pay', 'withdraw', 'internal']
            ]
        ];
    }

    public function actionPay() {
        $model = new Payment();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect([$model->system . '/pay', 'amount' => $model->amount * 100]);
        }
        $wallets = SQL::queryOne('SELECT ethereum, blockio FROM "user" WHERE id = :id', [
            ':id' => Yii::$app->user->identity->user
        ]);
        return $this->render('pay', [
            'wallets' => $wallets,
            'model' => $model
        ]);
    }


    public function actionWithdraw() {
        $user_id = Yii::$app->user->identity->user;
        $nick = Yii::$app->user->identity->nick;
        $transfer = Transfer::findOne([
            'from' => $user_id,
            'type' => 'withdraw',
            'status' => 'created'
        ]);
        if ($transfer) {
            Yii::$app->session->addFlash('error',
                Yii::t('app', 'You cannot create more than one withdrawal request'));
            Log::log('withdraw', 'second');
            return $this->redirect(['transfer/index', 'id' => $transfer->id, 'nick' => $nick]);
        }
        $model = new Withdraw();
        $isValid = $model->load(Yii::$app->request->post()) && $model->validate();
        $text = empty($model->text) ? null : $model->text;
        Log::log('transfer', 'withdraw', [
            'address' => $model->wallet,
            'amount' => $model->amount,
            'system' => $model->system,
            'text' => $text,
            'ip' => $model->ip
        ]);
        if ($isValid) {
            $amount = $model->amount * $model->getRate();
            $t = Yii::$app->db->beginTransaction();
            $currency = Transfer::$currencies[$model->system];
            $min = Withdraw::$minimal[$currency];
            if ($amount >= $min) {
                $balance = User::getBalance($currency);
                if (floor($balance - $amount) >= 0) {
                    $transfer = new Transfer([
                        'type' => 'withdraw',
                        'status' => 'created',
                        'system' => $model->system,
                        'currency' => $currency,
                        'from' => $user_id,
                        'amount' => $amount,
                        'wallet' => $model->wallet,
                        'text' => $text,
                        'ip' => $model->ip
                    ]);
                    if ($transfer->save(true)) {
                        $t->commit();
                        Yii::$app->telegram->sendText("Withdraw $model->amount $currency by $nick #$transfer->id");
                        Yii::$app->session->setFlash('success',
                            Yii::t('app', 'Withdrawal request created'));
                        return $this->redirect(['transfer/index', 'id' => $transfer->id]);
                    }
                } else {
                    Log::log('withdraw', 'insufficient', [
                        'balance' => $balance,
                        'amount' => $amount,
                        'wallet' => $model->wallet,
                        'currency' => $currency
                    ]);
                    $model->addError('amount', Yii::t('app', 'Insufficient funds'));
                }
                $t->rollBack();
            } else {
                $x = Transfer::getExchange()[$currency][$currency];
                $t->rollBack();
                $model->addError('amount', Yii::t('app', 'Minimal amount is {amount}', [
                    'amount' => ($min / $x) . ' ' . $currency
                ]));
            }
        }
        return $this->render('withdraw', [
            'model' => $model
        ]);
    }

    public function actionInternal() {
        $model = new Internal();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $id = SQL::scalar('SELECT internal("from", "to", "amount", "currency", "ip") VALUES (:from, :to, :amount, :currency, :ip)', [
                ':from' => Yii::$app->user->identity->user,
            ]);
        }
        return $this->render('internal', [
            'model' => $model
        ]);
    }
}
