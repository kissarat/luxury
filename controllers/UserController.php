<?php

namespace app\controllers;

use app\behaviors\Access;
use app\helpers\SQL;
use app\models\Email;
use app\models\form\Login;
use app\models\form\PasswordReset;
use app\models\form\RequestPasswordReset;
use app\models\form\Signup;
use app\models\Identity;
use app\models\Log;
use app\models\Token;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class UserController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['logout', 'profile'],
                'guest' => ['login', 'signup']
            ]
        ];
    }

    public function beforeAction($action) {
        if ('authorize' === $action->id) {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionSignup() {
        $this->layout = '@app/views/layouts/outdoor';
        $model = new Signup();
        $sponsor = Yii::$app->request->get('sponsor');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->parent = User::getIdByNick($model->sponsor ?: 'trader');
            $model->generateSecret();
            $model->save(false);
            Email::send($model->email, 'Welcome', "Dear, $model->nick \nThank you for registration at " . Yii::$app->name);
            $identity = Identity::identifyCookie();
            $identity->login($model->id);
            $o = $model->getAttributes(['nick', 'email', 'skype', 'sponsor']);
            if (!$model->sponsor && $sponsor) {
                $o['sponsor'] = $model->sponsor;
            }
            Log::log('user', 'signup', $o);
            Yii::$app->user->setIdentity($identity);
            return $this->redirect(['user/profile', 'nick' => $model->nick]);
        } else {
            $model->sponsor = $sponsor ?: 'trader';
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionLogin() {
        $this->layout = '@app/views/layouts/outdoor';
        $notCheck = !Yii::$app->params['checkLoginAttempt'];

        $model = new Login();
        if ($model->load(Yii::$app->request->post())) {
            $user = null;
            if ($notCheck || Login::checkLoginAttempt(['ip' => Yii::$app->request->getUserIP()])) {
                $user = $model->getUser();
                if ($user && ('special' !== $user->type || 'trader' === $user->nick)) {
                    if ('blocked' === $user->type) {
                        $model->addError('login', Yii::t('app', 'User is blocked'));
                    }
                    else if ($notCheck || Login::checkLoginAttempt(['user' => $user->id])) {
                        if ($user->validatePassword($model->password)) {
                            $identity = Identity::identifyCookie();
                            $identity->login($user->id);
                            Yii::$app->user->setIdentity($identity);
                            Log::log('auth', 'login', $model->getAttributes(['login', 'ip']));
                            return $this->redirect(['user/profile', 'nick' => $user->nick]);
                        } else {
                            $model->addError('password', Yii::t('app', 'Invalid password'));
                        }
                    }
                } else {
                    $model->addError('login', Yii::t('app', 'User does not exists'));
                }
            }
            Log::log('auth', 'invalid', $model->getAttributes(['login', 'ip']), $user ? $user->id : null);
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout() {
        Access::verifyNick();
        $id = Yii::$app->user->identity->user;
        Yii::$app->user->identity->logout(null);
        Yii::$app->user->setIdentity(null);
        Log::log('auth', 'logout', null, $id);
        return $this->redirect(['user/login']);
    }

    public function actionProfile($nick) {
        if ($nick !== Yii::$app->user->identity->nick) {
            throw new ForbiddenHttpException();
        }
        $informer = (new Query())
            ->from('informer')
            ->where(['nick' => $nick])
            ->one();
        $informer = array_merge($informer, Transfer::getFinance($informer['id']));

        if ($informer['parent']) {
            $informer['parent'] = json_decode($informer['parent']);
        }

        return $this->render('profile', ['model' => $informer]);
    }

    public function actionRequest() {
        $this->layout = '@app/views/layouts/outdoor';
        $model = new RequestPasswordReset();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (($user = $model->getUser())
                && ($code = $user->generateCode())
                && (Email::send($user->email, Yii::t('app', 'Password Recovery'),
                    'You can recover your password ' . Url::to(['user/reset', 'code' => $code], true)))
            ) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Check your email'));
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'User not found'));
            }
        }
        return $this->render('request', ['model' => $model]);
    }

    public function actionReset($code) {
        $this->layout = '@app/views/layouts/outdoor';
        $model = new PasswordReset();
        $token = Token::findOne(['id' => $code]);
        if ($token) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user = User::findOne(['id' => $token->user]);
                $user->password = $model->password;
                $user->generateSecret();
                if ($user->save(false) && $token->delete()) {
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Password changed'));
                }
                Log::log('auth', 'password', ['code' => $code], $user->id);
                return $this->redirect(['user/login']);
            }
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Password recovery code not found'));
            Log::log('auth', 'invalid', ['code' => $code]);
        }
        return $this->render('reset', ['model' => $model]);
    }

    public function actionAuthorize($nick) {
        if (empty($_REQUEST['token'])) {
            throw new BadRequestHttpException('Token not set');
        }
        $user = User::find()->where(['nick' => $nick])->select(['id', 'nick', 'admin'])->one();
        if ($user) {
            $isAdmin = SQL::scalar('SELECT count(*) FROM identity WHERE admin AND id = :id', ['id' => $_REQUEST['token']]) > 0;
            if ($isAdmin && !$user->admin) {
                Identity::identifyCookie()->login($user->id);
                Log::log('auth', 'token');
                return $this->redirect(['user/profile', 'nick' => $user->nick]);
            }
            throw new ForbiddenHttpException();
        }
        throw new BadRequestHttpException('User not found');
    }
/*
    public function actionContact($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $contact = SQL::queryOne(
            'SELECT id, nick, email, skype, avatar, surname, forename FROM "user" WHERE ("type" <> \'special\' OR id > 1) AND id = :id', [
            ':id' => $id
        ]);
        if ($contact) {
            return ['result' => $contact];
        }
        Yii::$app->response->statusCode = 404;
        return ['status' => 404];
    }
*/
}
