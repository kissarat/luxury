<?php

namespace app\controllers;


use Yii;
use yii\web\Controller;
use yii\web\Response;

abstract class RestController extends Controller {
    public function isApiCall() {
        return (YII_DEBUG && isset($_GET['format']) && 'json' === $_GET['format']) ||
            isset(Yii::$app->request->getAcceptableContentTypes()['application/json']);
    }

    public function render($view, $params = []) {
        if (isset($params['search']) && !isset($params['provider'])) {
            $params['provider'] = $params['search']->search($_GET);
        }
        if ($this->isApiCall()) {
            return $this->json($params);
        }
        return parent::render($view, $params);
    }

    public function json($params) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $r = isset($params['status']) ? $params : [];
//        $r['spend'] = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        if (!isset($params['status'])) {
            if (isset($params['provider'])) {
                $q = $params['provider'];
                $models = $q->getModels();
                $page = $q->getPagination();
                $sort = [];
                foreach ($q->sort->orders as $key => $order) {
                    $sort[$key] = $order === SORT_ASC ? 1 : -1;
                }
                if (count($sort) > 0) {
                    $r['sort'] = $sort;
                }
                $r['page'] = [
                    'offset' => $page->offset,
                    'limit' => $page->limit,
                    'total' => $page->totalCount
                ];
                $r['result'] = $models;
            } else if (isset($params['model'])) {
                $r['result'] = $params['model'];
            } else if (isset($params['models'])) {
                $r['result'] = $params['models'];
            }
        }
        return $r;
    }
}
