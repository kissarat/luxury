<?php

namespace app\controllers;


use app\behaviors\Access;
use app\models\NodeSummary;
use Yii;
use yii\data\ActiveDataProvider;

class NodeController extends RestController {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index']
            ]
        ];
    }

    public function actionIndex() {
        $user = Yii::$app->user->identity->user;
        $provider = new ActiveDataProvider([
            'query' => NodeSummary::find()
                ->where(['user' => $user])
        ]);
        return $this->render('index', [
            'provider' => $provider
        ]);
    }
}
