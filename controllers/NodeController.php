<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\NodeSummary;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\web\Response;

class NodeController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'promo', 'promotion']
            ]
        ];
    }

    public function actionIndex() {
        $user = Yii::$app->user->identity->user;
        $provider = new ActiveDataProvider([
            'query' => NodeSummary::find()
                ->where(['user' => $user])
        ]);
        return $this->render('index', [
            'provider' => $provider
        ]);
    }

    public function actionPromo() {
        return $this->render('promo', [
            'models' => SQL::queryAll('SELECT * FROM promo ORDER BY id DESC',
                null, PDO::FETCH_OBJ)
        ]);
    }

    public function actionPromotion() {
        $first = SQL::queryAll('SELECT currency, amount FROM turnover_level WHERE level = 1 AND "user" = :user', [
            ':user' => Yii::$app->user->identity->user
        ], PDO::FETCH_KEY_PAIR);
        $exchange = SQL::queryAll('SELECT currency, rate FROM fixed_exchange', [], PDO::FETCH_KEY_PAIR);
        return $this->render('_promotion', [
            'turnover' => $first,
            'exchange' => $exchange
        ]);
    }
}
