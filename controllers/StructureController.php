<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\Referral;
use app\models\search\ReferralSearch;
use app\models\User;
use app\models\Visit;
use PDO;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Response;

class StructureController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'children']
            ]
        ];
    }

    public function actionIndex() {
        $search = new ReferralSearch();
        $parent = Yii::$app->user->identity->parent;
        return $this->render('index', [
            'sponsor' => $parent > 0 ? User::findOne(['id' => $parent]) : null,
            'search' => $search,
            'provider' => $search->search($_GET, [
//                'sort' => [
//                    'defaultOrder' => [
//                        'level' => SORT_ASC,
//                        'id' => SORT_DESC
//                    ]
//                ]
            ])
        ]);
    }

    public function actionChildren($id) {
        $this->layout = false;
        return $this->render('children', [
            'provider' => new ArrayDataProvider([
                'models' => Referral::find()
                    ->where(['root' => $id, 'level' => 1])
                    ->all()
            ])
        ]);
    }

    public function actionVisit() {
        Yii::$app->response->format = Response::FORMAT_JSONP;
        $visit = new Visit([
            'url' => '/ref/' . Yii::$app->request->get('r'),
            'ip' => Yii::$app->request->getUserIP()
        ]);
        $visit->save();
        return [
            'callback' => Yii::$app->request->get('callback') ?: 'callback',
            'data' => ['ok' => 1]
        ];
    }
                
    public function actionHistogram($user, $start, $stop = '', $step = '1 day') {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!$stop) {
            $stop = date('Y-m-d');
        }
        $params = [
            ':user' => $user,
            ':start' => $start,
            ':stop' => $stop,
            ':step' => $step,
        ];
        $histogram = SQL::queryAll('SELECT
          s AS time,
          (SELECT count(*) FROM referral r
            WHERE
              r.created >= s AND
              r.created < s + :step :: INTERVAL
        AND root = :user AND level > 0) AS count
        FROM generate_series(:start :: TIMESTAMP, :stop, :step) s',
            $params, PDO::FETCH_KEY_PAIR);
        return ['result' => $histogram];
    }
}
