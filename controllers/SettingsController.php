<?php

namespace app\controllers;


use app\behaviors\Access;
use app\models\form\Password;
use app\models\Log;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class SettingsController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['profile', 'wallets', 'password']
            ]
        ];
    }

    public function actionProfile() {
        Access::verifyNick();
        $model = User::findOne(['id' => Yii::$app->user->identity->user]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Log::log('user', 'profile', $model->getDirtyAttributes([
                'ip', 'surname', 'forename', 'email',
                'skype', 'perfect', 'phone',
                'country', 'settlement', 'address', 'telegram'
            ]));
            $file = UploadedFile::getInstance($model, 'avatar_file');
            if ($file && !empty($file->tempName) && !empty($file->getExtension())) {
                $nick = Yii::$app->user->identity->nick;
                if ($model->getOldAttribute('avatar')) {
                    $old = Yii::getAlias('@app/web' . $model->getOldAttribute('avatar'));
                    if (file_exists($old)) {
                        unlink($old);
                    }
                }
                $md5 = md5_file($file->tempName);
//                header('FILE: ' . $file->tempName);
//                header('MD5: ' . $md5);
                $url = "/avatars/$nick-$md5." . $file->getExtension();
                move_uploaded_file($file->tempName, Yii::getAlias('@app/web' . $url));
                $model->avatar = $url;
            }
            $model->save(false);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
        }
        return $this->render('profile', [
            'model' => $model
        ]);
    }

    public function actionWallets() {
        Access::verifyNick();
        $model = User::findOne(['id' => Yii::$app->user->identity->user]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $can = true;
            foreach (['perfect'] as $name) {
                if (!empty($model->getOldAttribute($name)) && $model->isAttributeChanged($name)) {
                    $model->addError($name, 'Contact technical support for change');
                    $can = false;
                }
            }
            if ($can) {
                $model->save(false);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
            }
        }
        return $this->render('wallets', [
            'model' => $model
        ]);
    }

    public function actionPassword() {
        Access::verifyNick();
        $model = new Password();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Log::log('user', 'password', $model->getAttributes(['ip']));
            $user = User::findOne(['id' => Yii::$app->user->identity->user]);
            if ($user->validatePassword($model->current)) {
                $user->password = $model->new;
                $user->generateSecret();
                $user->save(false);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
            } else {
                $model->addError('current', Yii::t('app', 'Invalid password'));
            }
        }
        return $this->render('password', [
            'model' => $model
        ]);
    }
}
