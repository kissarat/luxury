<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\helpers\Utils;
use app\models\Coordinate;
use app\models\Visit;
use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class HomeController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'admin' => ['mail', 'telegram']
            ]
        ];
    }

    public function beforeAction($action) {
        Yii::$app->controller->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionError() {
        $exception = Yii::$app->getErrorHandler()->exception;
        if ($exception instanceof HttpException) {
            if ($exception instanceof UnauthorizedHttpException && !$this->isApiCall()) {
                return $this->redirect(['user/login']);
            }
            $this->view->title = Yii::t("app", $exception->getName());
            return $this->render('error', [
                'status' => $exception->statusCode,
                'error' => [
                    'message' => $exception->getMessage() ?: $exception->getName()
                ]
            ]);
        }
        return $this->render('exception', [
            'exception' => $exception
        ]);
    }

    public function actionSocial() {
        return $this->render('social');
    }

    public function actionAdvertise() {
        return $this->render('advertise');
    }

    public function actionPdf() {
        return $this->render('pdf');
    }


    public function actionLanding() {
        return $this->render('landing');
    }

    public function actionBanners() {
        return $this->render('banners');
    }

    public function actionConstruction() {
        return $this->render('construction');
    }

    public function actionAdmin() {
        return $this->render('admin');
    }

    public function actionWidgets() {
        return $this->render('widgets');
    }

    public function actionInstagram() {
        return $this->render('instagram');
    }

    public function actionCases() {
        return $this->render('cases');
    }

    public function actionCoordinate($time = null, $latitude, $longitude, $altitude = null, $accuracy = null) {
//        Yii::$app->response->headers->add('Cache-Control', 'max-age=3600, public');
        if ($time) {
            $time /= 1000;
        }
        else {
            $time = time();
        }
        $time = Utils::timestamp($time);
        $user = Yii::$app->user;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $coordinate = new Coordinate([
            'time' => $time,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'altitude' => $altitude,
            'accuracy' => $accuracy,
            'user' => $user->getIsGuest() ? null : $user->identity->user,
            'ip' => Yii::$app->request->getUserIP()
        ]);
        $coordinate->save();
        return [
            'result' => ['id' => $coordinate->id]
        ];
    }

    public function actionDefault() {
        return $this->redirect(Yii::$app->user->getIsGuest()
            ? ['user/login']
            : ['user/profile', 'nick' => Yii::$app->user->identity->nick]);
    }

    public function actionMail() {
        Utils::mail($_POST['to'], $_POST['subject'], $_POST['content']);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionVisit($uid, $url, $spend) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $visit = new Visit([
            'token' => $uid,
            'url' => base64_decode($url),
            'spend' => $spend,
            'ip' => Yii::$app->request->getUserIP(),
            'agent' => Yii::$app->request->getUserAgent(),
        ]);
        return Utils::jsonp(['success' => $visit->save()]);
    }

    public function actionNewbie($nick, $ico = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = null;
        if (isset($_COOKIE['PHPSESSID'])) {
            $token = $_COOKIE['PHPSESSID'];
        } else if (isset($_COOKIE['ico'])) {
            $token = $_COOKIE['ico'];
        } else if ($ico) {
            $token = $ico;
        }
        $last = null;
        if ($token) {
            $last = SQL::scalar('SELECT created FROM visit WHERE token = :token LIMIT 1', ['token' => $token]);
        }
        if (!$token || !$last) {
            SQL::execute('INSERT INTO visit(token, url, ip, agent) VALUES (:uid, :url, :ip, :agent)', [
                ':uid' => $token,
                ':url' => '/ref/' . $nick,
                ':ip' => Yii::$app->request->getUserIP(),
                ':agent' => Yii::$app->request->getUserAgent(),
            ]);
        }
        return Utils::jsonp([
            'success' => true,
            'last' => $last
        ]);
    }

    public function actionEthereumStatus() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->ethereum->request('web3_clientVersion')) {
            return ['success' => true];
        }
        Yii::$app->response->statusCode = 503;
        return ['success' => false];
    }

    public function actionStatus() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionTimezone() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'mongodb' => SQL::scalar('SHOW timezone'),
            'program' => date_default_timezone_get()
        ];
    }

    public function actionTelegram($text) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->telegram->sendText($text);
    }

    public function actionMirror() {
        if (Yii::$app->request->mirror) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $headers = [];
            foreach ($_SERVER as $key => $value) {
                if (strpos($key, 'HTTP_') === 0) {
                    $headers[str_replace('_', '-', strtolower(substr($key, 5)))] = $value;
                }
            }
            return [
                'uri' => $_SERVER['REQUEST_URI'],
                'ip' => $_SERVER['SERVER_ADDR'],
                'method' => $_SERVER['REQUEST_METHOD'],
                'query' => $_GET,
                'headers' => $headers,
                'data' => $_POST
            ];
        }
        else {
            throw new NotFoundHttpException();
        }
    }
}
