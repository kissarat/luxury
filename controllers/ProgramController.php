<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\helpers\Utils;
use app\models\ProgramDeposit;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\web\Response;

class ProgramController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['buy']
            ]
        ];
    }

    public function beforeAction($action) {
        if ('buy' === $action->id) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    protected function getPrograms() {
        $rows = SQL::queryAll('SELECT * FROM program_deposit_limited');
        $models = [];
        foreach ($rows as $row) {
            $p = $row['program'];
            if (empty($models[$p])) {
                $models[$p] = [];
            }
            $models[$p]['name'] = $row['name'];
            $models[$p]['accrue'] = +$row['accrue'];
            $models[$p]['quantity'] = $row['quantity'];
            $range = [];
            $c = $row['currency'];
            preg_match('|^.(\d+),(\d+).$|', $row['range'], $range);
            $models[$p]['min'][$c] = +$range[1];
            $models[$p]['max'][$c] = +$range[2] - 1;
            $models[$p]['available'][$c] = $row['max'] - $row['invested'];
        }

        $rows = SQL::queryAll('SELECT date, program, currency, max - invested AS available 
            FROM future_deposit_limited WHERE max - invested > 0 ORDER BY date');
        foreach ($rows as $row) {
            $models[$row['program']]['reservation'][$row['currency']][$row['date']] = $row['available'];
        }
        return $models;
    }

    public function actionIndex() {
        return Utils::jsonp($this->getPrograms());
    }

    public function actionEmbed() {
        return $this->render('embed', ['models' => $this->getPrograms()]);
    }

    public function actionBuy() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = [];
        if ('POST' === Yii::$app->request->getMethod()) {
            $currency = Yii::$app->request->post('currency');
            $amount = (int)Yii::$app->request->post('amount');
            $tz = (int)Yii::$app->request->post('tz');
            if (!(abs($tz) <= 720)) {
                Yii::$app->response->statusCode = 400;
                return ['error' => ['message' => 'Invalid timezone']];
            }
            $date = Yii::$app->request->post('date');
            $today = date('Y-m-d');
            if (strtotime($date) < time()) {
                $date = $today;
            }
            $isToday = $date === $today;
            if ($date && $tz) {
                $date = Utils::timestamp(strtotime($date) - $tz * 60);
            }
            $date = $isToday ? Utils::timestamp(time()) : $date;
            $result = ProgramDeposit::buy(Yii::$app->user->identity->user, $amount, $currency, $date);

            switch ($result) {
                case -402:
                    $balance = User::getBalance($currency);
                    $need = $amount - $balance;
                    $resp['status'] = 402;
                    $resp['need'] = $need;
                    $resp['balance'] = $balance;
                    $resp['currency'] = $currency;
                    $resp['error'] = ['message' => 'Insufficient funds'];
                    break;

                case -404:
                    $resp['status'] = 404;
                    $resp['error'] = ['message' => 'No reservation available'];
                    break;

                case -409:
                    $resp['status'] = 409;
                    $resp['error'] = ['message' => 'You can not open an investment until the withdrawal is approved'];
                    break;

                default:
                    $resp['status'] = 201;
                    $resp['success'] = true;
                    $resp['id'] = $result;
                    $resp['message'] = 'You have opened deposit successfully';
            }
        } else {
            $resp['status'] = 404;
        }
        Yii::$app->response->statusCode = $resp['status'];
        $resp['nick'] = Yii::$app->user->identity->nick;
        return $resp;
    }
}
