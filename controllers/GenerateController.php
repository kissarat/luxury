<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\Utils;
use app\models\form\Generator;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class GenerateController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'admin' => ['user', 'widget']
            ]
        ];
    }

    public function actionUser() {
        $model = new Generator();
        $errors = null;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            throw new \Exception(json_encode($model->attributes, JSON_PRETTY_PRINT));
            $errors = $model->generate();
        }
        return $this->render('user', ['model' => $model, 'errors' => $errors]);
    }

    public function actionWidget() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Utils::generateWidget();
        return ['success' => true];
    }
}
