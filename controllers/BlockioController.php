<?php

namespace app\controllers;


use app\behaviors\Access;
use app\models\Log;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class BlockioController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['address']
            ]
        ];
    }

    public function beforeAction($action) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Access::verifyNick();
        return parent::beforeAction($action);
    }

    public function actionAddress() {
        $address = Yii::$app->blockio->getUserAddress(Yii::$app->user->identity->nick);
        Log::log('blockio', 'address', ['address' => $address]);
        return ['result' => $address];
    }
}
