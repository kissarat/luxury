<?php

namespace app\controllers;

use app\behaviors\Access;
use app\models\Log;
use app\models\Transfer;
use yarcode\advcash\actions\ResultAction;
use yarcode\advcash\events\GatewayEvent;
use yarcode\advcash\Merchant;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class AdvcashController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['pay']
            ]
        ];
    }

    /** @inheritdoc */
    public $enableCsrfValidation = false;

    /** @var string Your component configuration name */
    public $componentName = 'advcash';

    /** @var Merchant */
    protected $component;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->component = Yii::$app->get($this->componentName);

        $this->component->on(GatewayEvent::EVENT_PAYMENT_REQUEST, [$this, 'handlePaymentRequest']);
//        $this->component->on(GatewayEvent::EVENT_PAYMENT_SUCCESS, [$this, 'handlePaymentSuccess']);
    }

    public function actions() {
        return [
            'result' => [
                'class' => ResultAction::className(),
                'componentName' => $this->componentName,
                'redirectUrl' => ['transfer/index'],
            ],
            'success' => [
                'class' => ResultAction::className(),
                'componentName' => $this->componentName,
                'redirectUrl' => ['transfer/index'],
                'silent' => true,
            ],
            'failure' => [
                'class' => ResultAction::className(),
                'componentName' => $this->componentName,
                'redirectUrl' => ['transfer/index'],
                'silent' => true,
            ]
        ];
    }

    /**
     * @param GatewayEvent $event
     * @return bool
     */
    public function handlePaymentRequest($event) {
        Log::log('advcash', 'merchant', $event->gatewayData);
        $transfer = Transfer::findOne(ArrayHelper::getValue($event->gatewayData, 'ac_order_id'));

        if ($transfer && ArrayHelper::getValue($event->gatewayData, 'ac_sci_name') == $this->component->merchantName) {
            $transfer->amount = ArrayHelper::getValue($event->gatewayData, 'ac_amount');
            $transfer->status = ArrayHelper::getValue($event->gatewayData, 'ac_transaction_status') == Merchant::TRANSACTION_STATUS_COMPLETED
                ? 'success' : 'fail';
            $transfer->save(false);
        }

        $event->handled = true;
    }

    public function actionPay($amount) {
        $transfer = new Transfer([
            'type' => 'payment',
            'status' => 'created',
            'system' => 'advcash',
            'currency' => 'USD',
            'to' => Yii::$app->user->identity->user,
            'ip' => Yii::$app->request->getUserIP(),
            'amount' => (int)$amount
        ]);
        $transfer->save();
        return $this->render('pay', [
            'id' => $transfer->id,
            'amount' => $amount / 100,
            'description' => ''
        ]);
    }
}
