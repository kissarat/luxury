<?php

namespace app\controllers;


use app\behaviors\Access;
use app\models\Transfer;
use app\models\User;
use DateTime;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PerfectController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['pay']
            ]
        ];
    }

    private function accept() {
        $data = 'GET' === Yii::$app->perfect->method ? $_GET : $_POST;
        $transfer = Transfer::findOne(['id' => (int)(isset($data['PAYMENT_ID'])
            ? $data['PAYMENT_ID'] : Yii::$app->request->get('id'))]);
        if (Yii::$app->perfect->verify($data)) {
            $transfer->wallet = $data['PAYER_ACCOUNT'];
            $transfer->txid = (int)$data['PAYMENT_BATCH_NUM'];
            $transfer->amount = ((float)$data['PAYMENT_AMOUNT']) * 100;
            $transfer->status = 'success';
            $transfer->currency = 'USD';
            $transfer->system = 'perfect';
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
            $nick = Yii::$app->user->identity->nick;
            $amount = $data['PAYMENT_AMOUNT'];
            Yii::$app->telegram->sendText("Payment from $nick \$$amount $transfer->wallet");
        } else {
            $transfer->status = 'fail';
        }
        $transfer->save(false);
        return $transfer;
    }

    private function redirectAccept() {
        $transfer = $this->accept();
        return $this->redirect(['transfer/index',
            'id' => $transfer->id,
            'nick' => Yii::$app->user->identity->nick
        ]);
    }

    public function actionSuccess() {
        return $this->redirectAccept();
    }

    public function actionFail($id) {
        $transfer = Transfer::findOne(['id' => $id]);
        if (!$transfer) {
            throw new NotFoundHttpException('Payment not found');
        }
        if ($transfer->to !== Yii::$app->user->identity->user) {
            throw new NotFoundHttpException('You can cancel only your payment');
        }
        $transfer->status = 'canceled';
        if ($transfer->save(false)) {
            return $this->redirect(['transfer/index',
                'id' => $transfer->id,
                'nick' => Yii::$app->user->identity->nick
            ]);
        }
    }

    public function actionStatus() {
        $transfer = $this->accept();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => 'success' === $transfer->status,
            'transfer' => $transfer
        ];
    }

    private function specialPage($page, $id) {
        return Url::to(["/perfect/$page", 'id' => $id], true);
    }

    public function actionPay($amount = 0) {
        if (!($amount > 0)) {
            throw new BadRequestHttpException('Amount must be positive integer');
        }
        $created = date(DateTime::ISO8601, time());
        $transfer = new Transfer([
            'type' => 'payment',
            'status' => 'created',
            'to' => Yii::$app->user->identity->user,
            'ip' => Yii::$app->request->getUserIP(),
            'amount' => (int)$amount,
            'created' => $created,
            'system' => 'perfect',
            'currency' => 'USD'
        ]);
        $transfer->save(false);
        $balance = User::getBalance('USD');
        $amount = $transfer->amount / 100;
        $nick = Yii::$app->user->identity->nick;
        $data = [
            'BAGGAGE_FIELDS' => 'USER USER_ID BALANCE CREATED IP EMAIL',
            'BALANCE' => $balance / 100,
            'EMAIL' => Yii::$app->user->identity->email,
            'IP' => $transfer->ip,
            'NOPAYMENT_URL' => $this->specialPage('fail', $transfer->id),
            'PAYEE_ACCOUNT' => Yii::$app->perfect->wallet,
            'PAYEE_NAME' => Yii::$app->name,
            'PAYMENT_AMOUNT' => $amount,
            'PAYMENT_ID' => $transfer->id,
            'PAYMENT_UNITS' => 'USD',
            'PAYMENT_URL' => $this->specialPage('success', $transfer->id),
            'PAYMENT_URL_METHOD' => Yii::$app->perfect->method,
            'STATUS_URL' => $this->specialPage('status', $transfer->id),
            'SUGGESTED_MEMO' => "$nick $$amount",
            'CREATED' => $created,
            'USER' => $nick,
            'USER_ID' => Yii::$app->user->identity->user,
        ];
        return $this->render('@app/views/transfer/autosubmit', [
            'url' => 'https://perfectmoney.is/api/step1.asp',
            'data' => $data
        ]);
    }
}
