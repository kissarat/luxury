<?php

namespace app\helpers;

class Html extends \yii\bootstrap\Html {
    public static function money($values) {
        $out = [];
        foreach ($values as $currency => $value) {
            if ($value != 0) {
                $out[] = Html::tag('span', $value, ['class' => strtolower($currency)]);
            }
        }
        return Html::tag('span', implode(' ', $out), ['class' => 'money ' . (empty($out) ? 'empty' : '')]);
    }

    public static function background($model, $property) {
        return isset($model->$property) && $model->$property ? "background-image: url('" . $model->$property . "')" : '';
    }

    public static function email($address) {
        return "<a href=\"mailto:$address\">$address</a>";
    }
}
