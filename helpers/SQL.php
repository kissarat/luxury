<?php

namespace app\helpers;


use PDO;
use Yii;

class SQL {
    public static function scalar($sql, $params = []) {
        return Yii::$app->db->createCommand($sql, $params)->queryScalar();
    }

    public static function queryAll($sql, $params = [], $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand($sql, $params)->queryAll($fetch_mode);
    }

    public static function queryOne($sql, $params = [], $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand($sql, $params)->queryOne($fetch_mode);
    }

    public static function execute($sql, $params = []) {
        return Yii::$app->db->createCommand($sql, $params)->execute();
    }
}
