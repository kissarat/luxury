<?php

use app\helpers\Html;
use yii\grid\GridView;

$focus = empty($_GET['id']) ? null : +$_GET['id'];
$nick = Yii::$app->user->identity->nick;

function param($key, $value = null, $text = null)
{
    $params = $_GET;
    if ($value) {
        $params[$key] = $value;
    } else {
        unset($params[$key]);
    }
    $params[0] = 'transfer/index';
    return Html::a(Yii::t("app", $text) ?: $value, $params);
}

function params($label, $key, $values)
{
    $result = [param($key, null, 'All')];
    foreach ($values as $value => $text) {
        $result[] = param($key, is_numeric($value) ? $text : $value, $text);
    }
    return Html::tag('span',
        Html::tag('span', Yii::t("app", $label), ['class' => 'label'])
        . Html::tag('span', implode(' | ', $result), ['class' => 'values ' . $key]), [
            'id' => 'filter-' . $key
        ]);
}

/**
 * @var yii\data\ArrayDataProvider $provider
 */
?>
<section class="structure-index" id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">
                        <?= Yii::t('app', 'History') ?>
                    </h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'History') ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div id="striped-table">
                <div class="row">
                    <div class="col l12 m12 s12">
                        <div class="page transfer-index">
                            <div class="control-panel">
                                <blockquote class="filters">
                                    <h5><?= Yii::t('app', 'Filters') ?></h5>
                                    <?= params('Currency: ', 'currency', ['USD', 'BTC', 'ETH']) ?>
                                    <?= params('Type: ', 'type', [
                                        'payment' => 'Payment',
                                        'withdraw' => 'Withdrawn',
                                        'accrue' => 'Accruals',
                                        'bonus' => 'Referral',
                                    ]) ?>
                                </blockquote>
                            </div>

                            <?= GridView::widget([
                                'dataProvider' => $provider,
                                'layout' => '<div class="table-control2">{errors}</div>{items}{pager}{summary}',
                                'tableOptions' => ['class' => 'striped bordered responsive-table hoverable'],
                                'emptyText' => 'Nothing found',
                                'rowOptions' => function ($model) use ($focus) {
                                    $class = $focus === $model->id ? 'focus' : 'normal';
                                    return ['class' => $class];
                                },
                                'columns' => [
//                                    'id',
                                    [
                                        'attribute' => 'amount',
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            $sign = $model->to === Yii::$app->user->identity->user ? '+' : '-';
                                            $currency = strtolower($model->currency);
                                            return "<span class=\"sign\">$sign</span><span class=\"$currency\">$model->amount</span>";
                                        }
                                    ],
                                    [
                                        'attribute' => 'text',
                                        'label' => Yii::t('app', 'Description'),
                                        'value' => function ($model) {
                                            if ($model->text) {
                                                if ($model->vars) {
                                                    return Yii::t("app", $model->text, json_decode($model->vars, JSON_OBJECT_AS_ARRAY));
                                                }
                                                return Yii::t("app", $model->text);
                                            }
                                            switch ($model->type) {
                                                case 'payment':
                                                    return Yii::t('app', 'Payment from {wallet}', ['wallet' => $model->wallet]);
                                                case 'withdraw':
                                                    return Yii::t('app', 'Withdrawal to {wallet}', ['wallet' => $model->wallet]);
                                                case 'support':
                                                    return Yii::t('app', 'Administrative');
                                                default:
                                                    return $model->type;
                                            }
                                        }
                                    ],
                                    [
                                        'attribute' => 'created',
                                        'label' => Yii::t('app', 'Time'),
                                        'format' => 'datetime',
                                        'contentOptions' => function ($model) {
                                            return ['data-time' => strtotime($model->created)];
                                        }
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'label' => Yii::t('app', 'Status'),
                                        'value' => function ($model) {
                                            switch ($model->status) {
                                                case 'success':
                                                    return "\u{2713}";

                                                case 'fail':
                                                    return "\u{274C}";

                                                case 'canceled':
                                                    return "Canceled";

                                                case 'created':
                                                    return 'Created';
                                                default:
                                                    return $model->status;
                                            }
                                        }
                                    ],
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>