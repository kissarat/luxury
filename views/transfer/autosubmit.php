<?php
use app\helpers\Html;

?>

<form action="<?= $url ?>" method="POST" class="autosubmit">
    <?= Html::tag('h1', Yii::t('app', 'Wait please...')) ?>
    <?php
    foreach ($data as $name => $value) {
        echo Html::hiddenInput($name, $value);
    }
    ?>
</form>
