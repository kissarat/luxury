<?php

use app\models\Transfer;
use yii\widgets\ActiveForm;

?>

<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Withdraw') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Withdraw') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">

           <!-- <p class="caption"><?= Yii::t('app', 'Carefully fill in all the fields for not to send money to another wallet') ?></p> -->

            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <div class="transfer-withdraw">
                            <?php $form = ActiveForm::begin() ?>
                            <?= $form->field($model, 'system')->dropDownList(Transfer::$systems) ?>

                     <!--       <?= Yii::t('app', 'The commission is approximately {amount} and may differ at the time of withdrawal of money', [
                                'amount' => '0.5 USD'
                            ]) ?> -->

                            <?= $form->field($model, 'amount', ['template' => '<div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-editor-attach-money  prefix"></i>{input}<label>' . Yii::t('app', 'Amount')  . ' </label>{error}{hint}</div></div>'
                            ])->textInput(['maxlength' => 12, 'id' => 'amount', 'class' => ''])
                            ?>

                            <?= $form->field($model, 'wallet', ['template' => '<div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-action-account-balance-wallet prefix"></i>{input}<label>' . Yii::t('app', 'Wallet')  . ' </label>{error}{hint}</div></div>'
                            ])->textInput(['maxlength' => 128, 'id' => 'wallet', 'class' => ''])
                            ?>

                            <?= $form->field($model, 'text', ['template' => '<div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-editor-insert-comment prefix"></i>{input}{label}{error}{hint}</div></div>'
                            ])->textInput(['maxlength' => 255, 'id' => 'text', 'class' => ''])
                            ?>

                            <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

                            <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Create') ?></button>
                            <?php ActiveForm::end() ?>

                        </div>
                    </div>
                </div>
            </div>
</section>
