<?php

use app\models\Transfer;
use yii\widgets\ActiveForm;

?>

<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Payment') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Payment') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <p class="caption">
                <?= Yii::t('app', 'Generate a personal address and transfer money to it using Ethereum.'
                    . ' <br>After 2 confirmations, the money will be deposited on the balance sheet and you will be able to use them.'
                    . ' <br>The ETH address is fixed to your profile forever.') ?>
            </p>

            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <div class="transfer-pay cc">
                            <?php $form = ActiveForm::begin() ?>
                            <?= $form->field($model, 'system', ['template' => '<div id="input-select" class="row">
        <div class="input-field col s12"> {label}<div class="select-wrapper initialized">
            {input}{error}{hint}</div></div> </div>'
                            ])->dropDownList(Transfer::$systems) ?>
                            <?= $form->field($model, 'amount', ['template' => '<div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-editor-attach-money prefix"></i>{input}{label}{error}{hint}</div></div>'
                            ])->textInput(['maxlength' => 255, 'id' => 'amount', 'class' => ''])
                            ?>

                            <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

                            <div class="cc-address center">
                                <div class="label caption" style="margin-bottom: 5px">
                                    <?= Yii::t('app', 'Personal Wallet {currency}', ['currency' => '']) ?>
                                </div>
                                <button type="button"
                                        class="get-cc-address btn light-blue lighten-1 waves-effect waves-light">
                                    <?= Yii::t('app', 'Get Address') ?>
                                </button>
                                <div class="value"></div>
                                <br>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button type="submit"
                                            class="btn light-blue lighten-1 waves-effect waves-light right">
                                        <i class="mdi-content-send right"></i>
                                        <?= Yii::t('app', 'Pay') ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
