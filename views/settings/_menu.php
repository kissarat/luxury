<?php
use app\helpers\Html;

$nick = Yii::$app->user->identity->nick;
?>

<div id="flat-button" class="section">
    <?php
    $menu = [
        'Profile' => ['settings/profile', 'nick' => $nick],
//        'Wallets' => ['settings/wallets', 'nick' => $nick],
        'Password' => ['settings/password', 'nick' => $nick],
    ];

    foreach ($menu as $label => $url) {
        echo Html::a(Yii::t("app", $label), $url, ['class' => 'waves-effect waves-light btn light-blue lighten-1 white-text']) . "\n";
    }

    ?>
</div>