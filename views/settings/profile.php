<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Profile');

?>

<section id="content">
    <?php require '_breadcrumbs.php' ?>
    <div class="container">
        <div class="section">
            <?php require '_menu.php' ?>
            <p class="caption"><?= Yii::t('app', 'Fill in the entire profile to use the cabinet for maximum power.') ?></p>
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header"><?= Yii::t('app', 'Change Profile Settings') ?></h4>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                        <div class="row">
                            <?= $form->field($model, 'forename', ['template' => '<div class="input-field col s6"><i class="mdi-action-perm-identity light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 48, 'id' => 'username', 'class' => '']) ?>

                            <?= $form->field($model, 'surname', ['template' => '<div class="input-field col s6"><i class="mdi-action-perm-identity light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 48, 'id' => 'surname', 'class' => '']) ?>
                        </div>
                        <div class="row">
                            <?= $form->field($model, 'email', ['template' => '<div class="input-field col s6"><i class="mdi-communication-quick-contacts-mail light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 48, 'id' => 'email', 'class' => '']) ?>

                            <?= $form->field($model, 'telegram', ['template' => '<div class="input-field col s6"><i class="fa fa-telegram light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 32, 'id' => 'telegram', 'class' => '']) ?>
                        </div>
                        <div class="row">
                            <?= $form->field($model, 'skype', ['template' => '<div class="input-field col s6"><i class="fa fa-skype light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 32, 'id' => 'skype', 'class' => '']) ?>

                            <?= $form->field($model, 'country', ['template' => '<div class="input-field col s6"><i class="mdi-social-public light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 64, 'id' => 'country', 'class' => '']) ?>
                        </div>
                        <div class="row">
                            <?= $form->field($model, 'settlement', ['template' => '<div class="input-field col s6"><i class="mdi-social-location-city light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 128, 'id' => 'settlement', 'class' => '']) ?>

                            <?= $form->field($model, 'address', ['template' => '<div class="input-field col s6"><i class="mdi-action-info-outline light-blue-text lighten-1-text prefix"></i>{input}{label}{error}{hint}</div>'])
                                ->textInput(['maxlength' => 128, 'id' => 'address', 'class' => '']) ?>
                        </div>
                        <div id="upload_avatar" class="row">
                            <div class="file-field input-field col s6 ">
                                <input class="file-path validate " type="text">
                                <div class="btn">
                                    <span><?= Yii::t('app', 'Your Photo') ?></span>
                                </div>
                                <?= $form->field($model, 'avatar_file')
                                    ->fileInput([
                                        'accept' => 'image/*'
                                    ]) ?>
                            </div>
                        </div>

                        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

                        <div class="row">
                            <div class="input-field col s12">
                                <?=
                                Html::submitButton(Yii::t('app', 'Save') . '<i class="mdi-content-send right"></i>',
                                    ['class' => 'btn light-blue lighten-1 waves-effect waves-light right']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
