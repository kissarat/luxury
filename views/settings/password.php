<?php

use app\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Password Change');
?>

<section id="content">
    <?php require '_breadcrumbs.php' ?>

    <div class="container">
        <div class="section">
            <?php require '_menu.php' ?>
            <!--<p class="caption"><?= Yii::t('app', 'Для изменения пароля введите существуюший пароль и новый') ?></p>-->
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header2"><?= Yii::t('app', 'Password Change') ?></h4>

                        <?php $form = ActiveForm::begin(); ?>

                        <?=
                        $form->field($model, 'current', [
                            'template' => '<div class="row margin">
                    <div class="input-field col s12">
                        <i class="mdi-action-lock-outline prefix"></i>{input}{label}{error}{hint}</div></div>'
                        ])->passwordInput(['maxlength' => 255, 'id' => 'current', 'class' => ''])
                        ?>

                        <div class="row">
                            <?=
                            $form->field($model, 'new', [
                                'template' => '<div class="input-field col s6">
                        <i class="mdi-action-lock-outline prefix"></i>{input}{label}{error}{hint}</div>'
                            ])->passwordInput(['maxlength' => 255, 'id' => 'new', 'class' => ''])
                            ?>

                            <?=
                            $form->field($model, 'repeat', [
                                'template' => '<div class="input-field col s6">
                        <i class="mdi-action-lock-outline prefix"></i>{input}{label}{error}{hint}</div>'
                            ])->passwordInput(['maxlength' => 255, 'id' => 'repeat', 'class' => ''])
                            ?>

                        </div>

                        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

                        <div class="row">
                            <div class="input-field col s12">
                                <?= Html::submitButton(
                                    Yii::t('app', 'Change') . '<i class="mdi-content-send right"></i>',
                                    ['class' => 'btn cyan waves-effect waves-light right']
                                ) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>