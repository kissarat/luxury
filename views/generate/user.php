<?php

use app\helpers\Html;
use yii\widgets\ActiveForm;

?>

<section>
    <div class="container">
        <div class="row">
            <div class="col l12 s12 m12">
                <?php $form = ActiveForm::begin(); ?>
                <br>
                <br>
                <br>
                <?= $form->field($model, 'sponsor'); ?>
                <?= $form->field($model, 'count'); ?>
                <?= $form->field($model, 'usd'); ?>
                <?= $form->field($model, 'btc'); ?>
                <?= $form->field($model, 'eth'); ?>
                <?= $form->field($model, 'created'); ?>
                <?= Html::submitButton('Generate'); ?>
                <?php if (!empty($errors)) {
                    echo Html::tag('pre', json_encode($errors, JSON_PRETTY_PRINT));
                } ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
