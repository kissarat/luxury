<?php
use app\helpers\Html;

/**
 * @var \app\models\Program[] $models
 */

$params = [
    'el' => '.program-embed .widget-container',
    'programs' => $models,
    'buy' => true,
];

if (YII_DEBUG) {
    $params['host'] = $_SERVER['HTTP_HOST'];
}

?>

<section class="structure-index" id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">
                        <?= Yii::t('app', 'Tariffs') ?>
                    </h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Tariffs') ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="section">
            <section class="program-embed">
                <div class="widget-container"></div>
                <script src="/ui/widget.js"></script>
                <script>
                    addEventListener('load', function () {
                        new ICO.Calculator(<?= json_encode($params) ?>)
                    })
                </script>
            </section>
        </div>
    </div>
</section>