<?php

use yii\widgets\ActiveForm;

/**
 * @var \app\models\Program[] $models
 */

$params = [
    'el' => '.program-embed .widget-container',
    'programs' => $models,
    'buy' => true
];

if (YII_DEBUG) {
    $params['host'] = $_SERVER['HTTP_HOST'];
}

?>

<section class="structure-index" id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">
                        <?= Yii::t('app', 'Tariffs') ?>
                    </h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Tariffs') ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <section class="program-embed">
                <div id="program-pro" v-cloak v-bind:class="{selected: currency}">
                    <pre v-if="alert.type" v-bind:class="{alert: true, ['alert-' + alert.type]: true}">{{ alert.message }}</pre>
                    <div class="info">
                        <div class="one">
                            <h2>PRO</h2>
                            <div class="min-amount">
                                <div class="label"><?= Yii::t('app', 'Amount of investment') ?>:</div>
                                <div class="list">
                                    <div><?= Yii::t('app', 'From') ?> 25 <span>USD</span></div>
                                    <div><?= Yii::t('app', 'From') ?> 0.01 <span>BTC</span></div>
                                    <div><?= Yii::t('app', 'From') ?> 0.1 <span>ETH</span></div>
                                </div>
                            </div>
                            <div class="available">
                                <div class="label"><?= Yii::t('app', 'Available today') ?>:</div>
                                <div class="list">
                                    <div v-for="(a, c) in available">{{ a / rates[c] }} <span>{{ c }}</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="two">
                            <div class="income">
                                <span>2%</span>
                                <span>/<?= Yii::t('app', 'day') ?></span>
                            </div>
                            <div class="about">
                                <div><?= Yii::t('app', 'Duration') ?> 90 <?= Yii::t('app', 'days') ?></div>
                                <div><?= Yii::t('app', 'Income') ?> 180%</div>
                            </div>
                        </div>
                        <div class="three">
                            <?php $form = ActiveForm::begin() ?>

                            <div class="currencies">
                                <div class="label"><?= Yii::t('app', 'Choose currency') ?>:</div>
                                <div class="list">
                                    <span v-for="c of currencies"
                                          v-on:click="select(c)"
                                    v-bind:class="{active: currency === c}">
                                        {{ c }}
                                    </span>
                                </div>
                            </div>
                            <div class="if-selected">
                                <?= $form->field($model, 'amount')->textInput([
                                    'maxlength' => 12, 'id' => 'pro-amount', 'v-model' => 'amount']) ?>
                                <button type="button"
                                        v-on:click="reserve(today)"
                                        v-bind:disabled="!(valid && availableToday)">
                                    <?= Yii::t('app', 'Invest') ?>
                                </button>
                            </div>
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                    <div class="free-date" v-if="valid && !availableToday">
                        <h6 v-if="availableAnytime" class="available">
                            <?= Yii::t('app', 'Open investment for a free date:') ?>
                        </h6>
                        <h6 v-else class="unavailable">
                            <?= Yii::t('app', 'No reservation available for entered amount') ?>
                        </h6>

                        <div class="reserved">
                            <div v-for="(m, date) in reservation[currency]"
                                 v-if="amount <= m / rates[currency]"
                                 v-on:click="reserve(date)" class="btn">
                                <span class="date">
                                    <span v-if="today === date">
                                        <?= Yii::t('app', 'Open now') ?>
                                    </span>
                                    <span v-else>
                                        <?= Yii::t('app', 'Date') ?>: {{ date }}
                                    </span>
                                </span>
                                <span class="max">
                                    <?= Yii::t('app', 'Limit') ?>
                                    <b>{{ m / rates[currency] }}</b>
                                    {{ currency }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-container"></div>
                <script src="/js/pro.js"></script>
                <script src="/ui/widget.js"></script>
                <script>
                  addEventListener('load', function () {
                    new ICO.Calculator(<?= json_encode($params) ?>)
                  })
                </script>
            </section>
        </div>
    </div>
</section>