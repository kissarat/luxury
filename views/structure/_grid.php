<?php

use app\helpers\Html;

function profileLink($attribute) {
    return [
        'attribute' => $attribute,
        'format' => 'html',
        'contentOptions' => ['class' => 'profile-link ' . $attribute],
        'value' => function ($model) use ($attribute) {
            $nick = $model->$attribute;
            $avatar = $model->avatar ?: 'https://office.ico-holding.com/images/user.png';
            $options = [
                'class' => 'avatar ' . ($model->avatar ? 'has' : 'no'),
                'style' => "background-image: url('$avatar')"
            ];
            return Html::tag('div', '', $options)
                . $nick;
//                . Html::p($nick, ['user/profile', 'nick' => $nick]);
        }
    ];
}

function configGrid($config) {
    return array_merge_recursive([
        'emptyText' => 'Nobody',
        'rowOptions' => function ($model) {
            return ['data-id' => $model->id];
        },
        'columns' => [

            profileLink('nick'),
            [
                'attribute' => 'surname',
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Surname')
                ]
            ],
            [
                'attribute' => 'forename',
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Forename')
                ]
            ],
            [
                'attribute' => 'telegram',
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Telegram')
                ]
            ],
            [
                'attribute' => 'children',
                'contentOptions' => function ($model) {
                    return ['data-children' => $model->children];
                }
            ],
            [
                'attribute' => 'level',
                'headerOptions' => ['class' => 'level hidden'],
                'filterOptions' => ['class' => 'level hidden'],
                'contentOptions' => ['class' => 'level hidden'],
            ],
            [
                'attribute' => 'eth',
                'contentOptions' => ['class' => 'eth'],
            ],
            [
                'attribute' => 'btc',
                'contentOptions' => ['class' => 'btc'],
            ],
            [
                'attribute' => 'usd',
                'contentOptions' => ['class' => 'usd'],
            ],
            profileLink('sponsor'),
            [
                'attribute' => 'created',
                'format' => 'date',
                'contentOptions' => function ($model) {
                    return ['data-time' => strtotime($model->created)];
                }
            ],

        ]
    ],
        $config);
}
