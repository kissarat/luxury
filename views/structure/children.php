<?php
use yii\grid\GridView;

require '_grid.php';

?>
<?= GridView::widget(configGrid([
    'dataProvider' => $provider,
    'layout' => '{items}'
]));
?>
