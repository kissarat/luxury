<?php

use app\helpers\Html;
use yii\grid\GridView;

/**
 * @var app\models\User $sponsor
 */

require '_grid.php';

/**
 * @var yii\data\ArrayDataProvider $provider
 */
?>

<section class="structure-index structure-table" id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m5 l5 right my-sponsor-cart">
                    <?php if ($sponsor): ?>
                        <div class="card">
                            <div class="card-content">
                                <div class="left2">
                                    <img src="<?= $sponsor->avatar ?: '/images/user.png' ?>" alt="sponsot"
                                         class="circle responsive-img activator card-profile-image" width="90px">
                                </div>
                                <div class="right2">
                                    <span class="small"><?= Yii::t('app', 'Your sponsor:') ?></span>
                                    <p><?= $sponsor->forename ?> <?= $sponsor->surname ?></p>
                                    <p style="text-transform: uppercase; font-weight: 300;"><?= Yii::t('app', 'Login:') ?>
                                        <span style="text-transform: uppercase; font-weight: 500;"><?= $sponsor->nick ?></span>
                                    </p>
                                    <p style="margin: 5px 0;"><a
                                                class="waves-effect waves-light btn modal-trigger red darken-2"
                                                href="#modal1"> <?= Yii::t('app', 'Show Card') ?></a></p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col s12 m6 l6">
                    <h5 class="breadcrumbs-title">
                        <?= Yii::t('app', 'Structure') ?>
                    </h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Structure') ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col l12 s12 m12">
                    <div id="striped-table">
                        <?= GridView::widget(configGrid([
                            'dataProvider' => $provider,
                            'filterModel' => $search,
                            'layout' => '<div class="table-control">{errors}</div>{items}{pager}{summary}',
                            'tableOptions' => ['class' => 'striped bordered responsive-table hoverable'],
                        ])) ?>

                    </div>
                </div>
            </div>

            <div id="modal1" class="modal modal-fixed-footer" style="max-width: 440px">
                <div class="modal-content">

                    <div id="sponsor">
                        <div class="button">
                        </div>
                        <?php
                        if ($sponsor) {
                            echo $this->render('@app/views/user/_card', ['model' => $sponsor]);
                        }
                        ?>
                    </div>

                    <br>
                    <br>
                    <br>
                </div>
                <div class="modal-footer">
                    <a href="#"
                       class="waves-effect waves-red btn-flat modal-action modal-close"><?= Yii::t('app', 'Close') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
