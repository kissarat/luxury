<?php

use app\helpers\Html;

$profileData = json_encode($model);
echo Html::script("const userProfile = $profileData;");

extract($model);

$referral_url = Yii::$app->params['referralLinkOrigin']
    . '/?r=' . $nick;

$isMe = Yii::$app->user->getIsGuest() ? false : Yii::$app->user->identity->nick === $nick;
?>

<div class="user-profile container">
    <!--chart dashboard start-->
    <div id="chart-dashboard">

        <div class="row">
            <div class="col s12 m8 18">
                <div class="card">
                    <div class="card-move-up waves-effect waves-block waves-light">
                        <div class="move-up light-blue lighten-1">
                            <div>
                                <span class="chart-title white-text"><?= Yii::t('app', 'Income / Outcome for month') ?></span>
                                <br>
                            </div>
                            <div class="trending-line-chart-wrapper">
                                <canvas id="trending-line-chart" height="70"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <a class="btn-floating btn-move-up waves-effect waves-light darken-2 right"><i
                                    class="mdi-content-add activator"></i></a>

                        <div class="cart-flex">
                            <div class="col s12 m3 l3" style="display: none;">
                                <div id="doughnut-chart-wrapper">
                                    <canvas id="doughnut-chart" height="200"></canvas>
                                    <div class="doughnut-chart-status">
                                        <span id="user-earned" class="eth"><?= Yii::t('app', 'unknown') ?></span>
                                        <p class="ultra-small center-align"><?= Yii::t('app', 'Earned') ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m4 l4" style="display: none;">
                                <ul class="doughnut-chart-legend">
                                    <li class="mobile ultra-small">
                                        <span class="legend-color"></span>
                                        <?= Yii::t('app', 'Deposit') ?>
                                    </li>
                                    <li class="kitchen ultra-small">
                                        <span class="legend-color"></span>
                                        <?= Yii::t('app', 'Partners') ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col s12 m5 l5">
                                <div class="trending-bar-chart-wrapper">
                                    <canvas id="trending-bar-chart" height="90"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="monthly-income" class="card-reveal">
                        <span class="card-title grey-text text-darken-4">
                            <?= Yii::t('app', 'Monthly income') ?>
                            <i class="mdi-navigation-close right"></i>
                        </span>
                        <table class="responsive-table">
                            <thead>
                            <tr>
                                <th data-field="month"><?= Yii::t('app', 'Month') ?></th>
                                <th data-field="item-sold"><?= Yii::t('app', 'Deposit') ?></th>
                                <th data-field="item-price"><?= Yii::t('app', 'Bonus') ?></th>
                                <th data-field="total-profit"><?= Yii::t('app', 'Summary') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="profile-right-column">
                <div class="col s12 m2 12 no-desctop">
                    <div class="card-panel  z-depth-3 waves-effect">
                        <span><?= Yii::t('app', 'Balance:') ?></span>
                        <?= Html::money($balance) ?>
                    </div>
                </div>
                <div class="col s12 m4 12">
                    <a href="https://office.ico-holding.com/program/embed">
                        <div class="invest-button waves-effect">
                            <i class="mdi-hardware-keyboard-arrow-right "></i>
                            <span><?= Yii::t('app', 'To Invest') ?></span>
                            <i class="mdi-hardware-keyboard-arrow-left"></i>
                        </div>
                    </a>
                </div>
                <div class="col s12 m4 14 pay-button-home no-desctop">
                    <a class="pay btn waves-effect light-blue lighten-1" href="https://office.ico-holding.com/invoice/pay"><i class="mdi-action-account-balance"></i> Пополнить</a>
                </div>
                <div class="col s12 m4 14 pay-button-home no-desctop">

                    <a class="withdraw btn waves-effect light-blue lighten-1" href="https://office.ico-holding.com/invoice/withdraw">Вывести</a>

                </div>


                <div class="col s12 m4 14">
                    <div class="line-divider"></div>
                </div>


                <div class="col s12 m2 14 ">
                    <div class=" red darken-1 z-depth-1 waves-effect white-text informers" style="display: flex;">
                        <i class="mdi-action-wallet-travel  icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Invested ') ?></span>
                            <h6><?= Html::money($buy) ?></h6>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2 14">
                    <div class="red darken-1 z-depth-1 waves-effect white-text informers" style="display: flex;">
                        <i class="mdi-editor-attach-money icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Income') ?></span>
                            <h6><?= Html::money($accrue) ?></h6>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2 14 ">
                    <div class="red darken-1 z-depth-1 waves-effect white-text informers" style="display: flex;">
                        <i class="mdi-action-account-child icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Bonus') ?></span>
                            <h6><?= Html::money($bonus) ?></h6>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2 14">
                    <div class="red darken-1 z-depth-1 waves-effect white-text informers" style="display: flex;">
                        <i class="mdi-action-get-app  icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Withdrawn') ?></span>
                            <h6><?= Html::money($withdraw) ?></h6>
                        </div>
                    </div>
                </div>

                <div class="form-group field-email required has-success">
                    <div class="input-field col  s12 m6 l4">
                        <i class="mdi-editor-attach-file prefix active"></i>
                        <?= Html::textInput('referral', $referral_url) ?>
                        <label class="control-label active" for="text"><?= Yii::t('app', 'Referral Link') ?></label>
                        <div class="help-block"></div>
                    </div>
                </div>


                <div class="col s12 m2 14">
                    <div class=" red darken-1 z-depth-1 waves-effect white-text informers-2" style="display: flex;">
                        <i class="mdi-editor-insert-link icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Visits') ?></span>
                            <h6><?= $visit ?></h6>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2 14 ">
                    <div class=" red darken-1 z-depth-1 waves-effect white-text informers-2" style="display: flex;">
                        <i class="mdi-action-assignment-ind  icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Invited') ?></span>
                            <h6><?= $invited ?></h6>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2 14">
                    <div class=" red darken-1 z-depth-1 waves-effect white-text informers-2" style="display: flex;">
                        <i class="mdi-action-add-shopping-cart  icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Activations') ?></span>
                            <h6><?= $node ?></h6>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2 14">
                    <div class=" red darken-1 z-depth-1 waves-effect white-text informers" style="display: flex;">
                        <i class="mdi-action-autorenew  icon-demo"></i>
                        <div>
                            <span><?= Yii::t('app', 'Turnover') ?></span>
                            <h6><?= Html::money($turnover) ?></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="income" class="col s12 m12 l12" style="display: none">
            <h4><?= Yii::t('app', 'My Tariffs') ?></h4>
            <table class="hoverable striped responsive-table">
                <thead>
                <tr>
                    <th data-field="created"><?= Yii::t('app', 'Invested') ?></th>
                    <th data-field="name"><?= Yii::t('app', 'Program') ?></th>
                    <th data-field="amount"><?= Yii::t('app', 'Amount') ?></th>
                    <th data-field="accrues"><?= Yii::t('app', 'Accrues') ?></th>
                    <th data-field="profit"><?= Yii::t('app', 'Profit') ?></th>
                    <th data-field="closing"><?= Yii::t('app', 'Closing') ?></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<div id="card-stats" class="container">
    <div class="row">
        <div class="col s12 m4 l4">
            <div class="card">
                <div class="card-content light-green darken-2 white-text">
                    <p class="card-stats-title"><i class="mdi-social-group-add"></i>
                        <?= Yii::t('app', 'All Team') ?>
                    </p>
                    <h4 class="card-stats-number"><?= $referral ?></h4>
                    <p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i>
                        <?= Yii::t('app', '{percent}% <span>growth <br/> of structure today</span>', [
                            'percent' => 1.3
                        ]) ?>
                    </p>
                </div>
                <div class="card-action light-green darken-3">
                    <div id="clients-bar"></div>
                </div>
            </div>
        </div>
        <div class="col s12 m4 l4">
            <div class="card">
                <div class="card-content light-blue lighten-1 white-text">
                    <p class="card-stats-title">
                        <i class="mdi-editor-attach-money"></i>
                        <span><?= Yii::t('app', 'Income from the team') ?></span>
                    </p>
                    <h4 class="card-stats-number"><?= Html::money($bonus) ?></h4>

                    <p class="card-stats-compare">
                        <i class="mdi-hardware-keyboard-arrow-up"></i>
                        <?= Yii::t('app', '{percent}% <span class="white-text">more <br/> than month before</span>', [
                            'percent' => 70
                        ]) ?>
                    </p>
                </div>
                <div class="card-action light-blue lighten-2">
                    <div id="sales-compositebar"></div>

                </div>
            </div>
        </div>
        <div class="col s12 m4 l4">
            <div class="card">
                <div class="card-content orange darken-1 white-text">
                    <p class="card-stats-title">
                        <i class="mdi-action-trending-up"></i>
                        <span><?= Yii::t('app', 'Daily income ') ?></span>
                    </p>
                    <h4 class="card-stats-number"><?= Html::money($daily) ?></h4>
                    <p class="card-stats-compare">
                        <i class="mdi-hardware-keyboard-arrow-up"></i>
                        <?= Yii::t('app', '{percent}% <span class="blue-grey-text text-lighten-5">more <br/> than yesterday</span>', [
                            'percent' => 80
                        ]) ?>
                    </p>
                </div>
                <div class="card-action orange darken-2">
                    <div id="profit-tristate"></div>
                </div>
            </div>
        </div>
    </div>
</div>
