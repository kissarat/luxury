<?php
/**
 * @var app\models\User $model
 */

use yii\helpers\Html;

?>

<div class="card">
    <div class="card-image waves-effect waves-block waves-light">
        <img class="activator" src="/images/user-bg.jpg" alt="user background">
    </div>
    <div class="card-content">
        <img src="<?= $model->avatar ?: '/images/user.png' ?>" alt="sponsot"
             class="circle responsive-img activator card-profile-image">
        <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
            <i class="mdi-action-account-circle"></i>
        </a>
        <span class="card-title activator grey-text text-darken-4"><?= Html::encode($model->forename . ' ' . $model->surname) ?></span>
        <br>
        <span><?= Yii::t('app', 'Your Sponsor') ?></span>

        <p>
            <i class="mdi-action-perm-identity light-blue-text text-lighten-1"></i>
            <span><?= Html::encode($model->nick) ?></span>
        </p>

        <?php if (!empty($model->email)) {
            echo '<p><i class="mdi-communication-email light-blue-text text-lighten-1"></i><span> ' . $model->email . '</span> </p>';
        }
        ?>
        <?php if (!empty($model->skype)) {
            echo '<p><i class="fa fa-skype light-blue-text text-lighten-1"></i><span>' . $model->skype . '</span> </p>';
        }
        ?>
        <?php if (!empty($model->telegram)) {
            echo '<p><i class="fa fa-telegram light-blue-text text-lighten-1"></i><span>' . $model->telegram . '</span> </p>';
        }
        ?>
    </div>
    <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">
                                <?= Html::encode($model->forename . ' ' . $model->surname) ?>
                                <i class="mdi-navigation-close right"></i>
                            </span>
        <p><?= Yii::t('app', '{days} days at ICO Holding', ['days' => 15]) ?></p>
        <?php if (!empty($model->country)) {
            echo '<p><i class="mdi-social-public light-blue-text text-lighten-1"></i><span>' . $model->country . '</span> </p>';
        }
        ?>
        <?php if (!empty($model->settlement)) {
            echo '<p><i class="mdi-social-location-city light-blue-text text-lighten-1"></i><span>' . $model->settlement . '</span> </p>';
        }
        ?>
        <?php if (!empty($model->address)) {
            echo '<p><i class="mdi-action-account-balance light-blue-text text-lighten-1"></i><span>' . $model->address . '</span> </p>';
        }
        ?>
    </div>
</div>
