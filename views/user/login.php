<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\form\Login */
/* @var $form ActiveForm */

$this->title = 'Login'
?>

<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'login-form']]); ?>
        <div class="row">
            <div class="input-field col s12 center">
                <img src="/images/login-logo.png" alt="ICO Holding Logo" class="responsive-img valign ">
                <p class="center login-form-text">
                    <?= Yii::t('app', '<strong>Login</strong><br> in back office') ?>
                </p>
            </div>
        </div>

        <?= $form->field($model, 'login', [
            'template' => '<div class="row margin">
          			<div class="input-field col s12">
            			<i class="mdi-social-person prefix"></i>{input}{label}{error}{hint}</div></div>'
        ])->textInput(['maxlength' => 24, 'id' => 'username', 'class' => ''])
        ?>

        <?= $form->field($model, 'password', [
            'template' => '<div class="row margin">
          			<div class="input-field col s12">
            			<i class="mdi-action-lock-outline prefix"></i>{input}{label}{error}{hint}</div></div>'
        ])->passwordInput(['maxlength' => 100, 'id' => 'password', 'class' => ''])
        ?>

        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="input-field col s12">
                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn waves-effect red darken-2 col s12']) ?>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s6 m6 l6">
                <p class="margin medium-small">
                    <?= Html::a(Yii::t('app', 'Register'), ['user/signup'], ['class' => 'red-text darken-2-text']) ?>
                </p>
            </div>
            <div class="input-field col s6 m6 l6">
                <p class="margin right-align medium-small">
                    <?= Html::a(Yii::t('app', 'Forgot Password?'), ['user/request'], ['class' => 'red-text darken-2-text']) ?>
                </p>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>


<!-- user-login -->
