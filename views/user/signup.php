<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Registration'
?>

<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'login-form signup-form']]); ?>
        <div class="row">
            <div class="input-field col s12 center">
                <img src="/images/login-logo.png" alt="ICO Holding Logo" class="responsive-img valign">
                <h4><?= Yii::t('app', 'Create an account') ?></h4>
                <p class="center"><?= Yii::t('app',
                        'Join to the out community of successful investors now!') ?></p>
            </div>
        </div>

        <?= $form->field($model, 'nick', [
            'template' => ' <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>{input}{label}{error}{hint}</div></div>'
        ])->textInput(['maxlength' => 24, 'id' => 'username', 'class' => ''])
        ?>

        <?= $form->field($model, 'email', [
            'template' => ' <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>{input}{label}{error}{hint}</div></div>'
        ])->textInput(['maxlength' => 48, 'id' => 'email', 'class' => ''])
        ?>

        <?= $form->field($model, 'password', [
            'template' => '<div class="row margin">
                    <div class="input-field col s12">
                        <i class="mdi-action-lock-outline prefix"></i>{input}{label}{error}{hint}</div></div>'
        ])->passwordInput(['maxlength' => 100, 'id' => 'password', 'class' => '',
            'autocomplete' => 'off'])
        ?>
        <!--      <?= $form->field($model, 'skype', [
            'template' => ' <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>{input}{label}{error}{hint}</div></div>'
        ])->textInput(['maxlength' => 48, 'id' => 'skype', 'class' => ''])
        ?>-->
        <?= $form->field($model, 'sponsor', [
            'template' => ' <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>{input}{label}{error}{hint}</div></div>'
        ])->textInput(['maxlength' => 24, 'id' => 'username', 'class' => ''])
        ?>

        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="input-field col s12">
                <?= Html::submitButton(Yii::t('app', 'Signup'), ['class' => 'btn waves-effect red darken-2 col s12']) ?>
            </div>
            <div class="input-field col s12">
                <p class="margin center medium-small sign-up">
                    <span><?= Yii::t('app', 'Have an account already?') ?></span>
                    <?= Html::a(Yii::t('app', 'Login'), ['user/login'], ['class' => 'red-text darken-2-text']) ?>
                </p>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
