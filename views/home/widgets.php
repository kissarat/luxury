<?php

use yii\helpers\Url;

?>

<section id="content" class="page widgets">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Widgets') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Widgets') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div id="widgets">
                <div id="widget-calc">
                    <template>
                        <div id="ico-calc-container"></div>
                        <script src="<?= Url::to('/ui/jsonp.js', true) ?>"></script>
                        <script>
                          new ICO.Calculator({
                            el: '#ico-calc-container',
                            sponsor: '<?= Yii::$app->user->identity->nick ?>'
                          })
                        </script>
                    </template>
                    <div class="info">
                        <h3><?= Yii::t('app', 'Calculator') ?></h3>
                        <div class="input-field col s12 m6 l6">
                            <span style="font-size: 13px; color: #a3a3a3"><?= Yii::t('app', 'Insert the calculator code on your site') ?></span>
                            <textarea id="textarea11" class="materialize-textarea"
                                      style="min-height: 155px"></textarea>
                        </div>
                        <button type="button" class="btn"><?= Yii::t('app', 'Show') ?></button>
                    </div>
                    <script>
                      addEventListener('load', function () {
                        $('#widget-calc textarea').val($('#widget-calc template').html().trim().replace(/^\s+/gm, ''))
                        $('#widget-calc button').click(function () {
                          $('#widget-calc .widget-src').html($('#widget-calc textarea').val())
                        })
                      })
                    </script>
                    <div class="widget-src"></div>
                </div>
            </div>
        </div>
</section>
