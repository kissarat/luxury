<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">   <?= Yii::t('app', 'Advertise') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Advertise') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div class="row advertice-ru">
                <div class="col s12 m12 l12">
                    <div class="material-placeholder" style="">
                        <img class="materialboxed intialized" src="/images/advertice.png" alt=""></div>
                </div>
            </div>
            <div class="row advertice-en">
                <div class="col s12 m12 l12">
                    <div class="material-placeholder" style="">
                        <img class="materialboxed intialized" src="/images/advertice-en.png" alt=""></div>
                </div>
            </div>
        </div>
    </div>
</section>