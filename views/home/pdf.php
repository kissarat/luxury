<?php
?>

<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'PDF Presentation') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'PDF Presentation') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            
            <div class="row pdf-ru" style="margin-bottom: 30px;">
                <div class="col s12 m12 l12">
                	<div class="material-placeholder" style=""><img class="materialboxed intialized" src="promo/pdf/pdf.png" alt="sample" style="" width=""></div>
                	<br>
                	<a href="promo/pdf/ICO-Holding.pdf" class="btn-floating btn-large waves-effect red darken-1" download><i class="mdi-editor-vertical-align-bottom"></i></a>
                	<a href="promo/pdf/ICO-Holding.pdf" class="btn-floating btn-large waves-effect light-blue lighten-1" target="_blank"><i class="mdi-content-add"></i></a>
              	</div>
            </div>
            <div class="row pdf-en" style="margin-bottom: 30px;">
                <div class="col s12 m12 l12">
                    <div class="material-placeholder" style=""><img class="materialboxed intialized" src="promo/pdf/pdf-en.png" alt="sample" style="" width=""></div>
                    <br>
                    <a href="promo/pdf/ICO-Holding-en.pdf" class="btn-floating btn-large waves-effect red darken-1" download><i class="mdi-editor-vertical-align-bottom"></i></a>
                    <a href="promo/pdf/ICO-Holding-en.pdf" class="btn-floating btn-large waves-effect light-blue lighten-1" target="_blank"><i class="mdi-content-add"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>