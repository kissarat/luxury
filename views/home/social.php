<?php
?>

<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Social Networks') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Social Networks') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div class="social-ru">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <p class="caption"><?= Yii::t('app', 'Photo for Facebook, VK, Twitter, etc') ?></p>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized"
                                 src="promo/social/sn/1.jpg" alt="sample" style=""
                                 width="100%">
                        </div>
                        <br>
                        <a href="promo/social/sn/1.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right"
                           download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/2.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/2.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/3.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/3.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/4.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/4.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/5.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/5.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/6.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/6.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/7.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/7.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
            </div>
            <div class="social-en">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <p class="caption"><?= Yii::t('app', 'Photo for Facebook, VK, Twitter, etc') ?></p>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized"
                                 src="promo/social/sn/en/1.jpg" alt="sample" style=""
                                 width="100%">
                        </div>
                        <br>
                        <a href="promo/social/sn/1.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right"
                           download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/en/2.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/2.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/en/3.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/3.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/en/4.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/4.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/en/5.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/5.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/en/6.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/6.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="promo/social/sn/en/7.jpg" alt="sample"
                                                                        style=""
                                                                        width="100%"></div>
                        <br>
                        <a href="promo/social/sn/7.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>