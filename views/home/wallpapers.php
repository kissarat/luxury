<?php
$lang = Yii::$app->language;
?>

<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Wallpapers') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a></li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a></li>
                        <li class="active"><?= Yii::t('app', 'Wallpapers') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div class="row wallp">

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized" src="/promo/wallp/01.jpg" style="" width="100%">
                        </div>
                        <br>
                        <a href="/promo/wallp/01.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized" src="/promo/wallp/02.jpg" style="" width="100%">
                        </div>
                        <br>
                        <a href="/promo/wallp/02.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized" src="/promo/wallp/03.jpg" style="" width="100%">
                        </div>
                        <br>
                        <a href="/promo/wallp/03.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized" src="/promo/wallp/04.jpg" style="" width="100%">
                        </div>
                        <br>
                        <a href="/promo/wallp/04.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized" src="/promo/wallp/05.jpg" style="" width="100%">
                        </div>
                        <br>
                        <a href="/promo/wallp/05.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download >
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized" src="/promo/wallp/06.jpg" style="" width="100%">
                        </div>
                        <br>
                        <a href="/promo/wallp/06.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>