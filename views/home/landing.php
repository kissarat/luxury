<?php
?>
<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">   <?= Yii::t('app', 'Landing Page') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Landing Page') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div class="row">
            </div>
        </div>
    </div>
</section>