<?php

$nick = Yii::$app->user->identity->nick;
?>
<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">   <?= Yii::t('app', 'Landing Page') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Landing Page') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div class="row">
                <div class="col s12 m6 l4">
                    <h4><?= Yii::t('app', 'Landing Page') ?></h4>
                    <a href="https://ico-holding.com/landing-page/?r=<?= $nick ?>" target="_blank">
                        <img src="/promo/lp/landing-page.jpg" alt="" style="max-width: 100%; border-radius: 9px">
                    </a>
                    <p><?= Yii::t('app', 'Рекламная страница с описанием тарифов и предоставлением начальной необходимой информации для инвестора.') ?></p>

                    <div class="input-field col  s12 m12 l12">
                        <i class="mdi-editor-attach-file prefix active"></i>
                        <input name="referral" value="https://ico-holding.com/landing-page/?r=<?= $nick ?>" type="text">
                        <label class="control-label active" for="text">Ваша партнёрская ссылка</label>
                        <div class="help-block"></div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</section>