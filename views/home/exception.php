<?php
use yii\helpers\Html;

/**
 * @var Error $exception
 */

?>
<div class="home-exception">
    <h1><?= Html::encode($this->title) ?></h1>
    <div><?= $exception->getMessage() ?></div>
    <div><?= $exception->getFile() ?></div>
    <pre><?= isset($exception->xdebug_message) ? $exception->xdebug_message : '' ?></pre>
</div>
