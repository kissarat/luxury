<section id="content" class="page instagram">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Instagram') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Instagram') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div class="row">
                <div class="col s12 m12 l12">
                    <p style="font-size: 13px; color: #a3a3a3"><?= Yii::t('app', 'Just copy for instagram') ?></p>
                </div>
            </div>
            <div class="row">
                <div class="input-field col m3 s12 l3 offset-l3 center">
                    <br>
                    <div class="circle instaphoto" style="background-image: url('/images/user.png');">
                    </div>
                </div>
                <div class="input-field col m6 s12 l6">
                    <div id="textarea1" class="materialize-textarea"
                         style="min-height: 80px; font-size: 14px; line-height: 1.1;">
                        <h4 class="header"><?= Yii::t('app', 'Name:') ?></h4>
                        <p style="font-size: 14px;"><b> <?= Yii::t('app', '🔵 INVESTING 🔵 BITCOIN 🔵 ICO 🔵') ?></b>
                        </p>

                        <h4 class="header"><?= Yii::t('app', 'Description:') ?></h4>
                        <p>🌐 <?= Yii::t('app', 'Network Marketing (MLM)') ?> 🌐</p>
                        <p>💎 <?= Yii::t('app', 'Investing in the Internet') ?> 💎</p>
                        <p>🔥 <?= Yii::t('app', 'Partner of ICO Holding') ?> 🔥</p>
                        <p>💰 <?= Yii::t('app', 'Income from 30% per month') ?> 💰</p>
                        <p>⬇️ <?= Yii::t('app', 'Registration with ICO Holding') ?> ⬇️</p>
                        <h4 class="header"><?= Yii::t('app', 'Link:') ?></h4>
                        <i style="color: blue">https://ico-holding.com/?r=admin</i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l6">
                    <p class="caption"><?= Yii::t('app', 'Images for Instagram') ?></p>
                </div>
            </div>
            <div class="ru-instagram">
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized"
                                 src="https://office.ico-holding.com/promo/social/instagram/1.jpg" alt="sample"
                                 style=""
                                 width="100%">
                        </div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/1.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right"
                           download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/2.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/2.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/3.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/3.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/4.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/4.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/5.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/5.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/6.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/6.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/7.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/7.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
            </div>
            <div class="en-instagram">
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style="">
                            <img class="materialboxed intialized"
                                 src="https://office.ico-holding.com/promo/social/instagram/en/1.jpg" alt="sample"
                                 style=""
                                 width="100%">
                        </div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/en/1.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right"
                           download>
                            <i class="mdi-editor-vertical-align-bottom"></i>
                        </a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/en/2.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/en/2.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/en/3.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/en/3.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/en/4.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/en/4.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/en/5.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/en/5.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>

                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/en/6.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/en/6.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col s12 m4 l4">
                        <div class="material-placeholder" style=""><img class="materialboxed intialized"
                                                                        src="https://office.ico-holding.com/promo/social/instagram/en/7.jpg"
                                                                        alt="sample"
                                                                        style="" width="100%"></div>
                        <br>
                        <a href="https://office.ico-holding.com/promo/social/instagram/en/7.jpg"
                           class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                    class="mdi-editor-vertical-align-bottom"></i></a>
                    </div>
                </div>
            </div>
            <div class="input-field col m6 s12 l6">
                <p class="caption" style="margin-bottom: 5px;"><?= Yii::t('app', '#HashTags #for #Instagram') ?></p>
                <span style="font-size: 13px; color: #a3a3a3"><?= Yii::t('app', 'Copy for your post') ?></span>
                <textarea id="textarea1" class="materialize-textarea" style="height: 80px; font-size: 12px">#ico #icoholding #блокчейн #blockchain #деньги #майнинг #mining #бизнес #бизнесдлявсех #mlm #bitcoin #млм #доход #сетевойбизнес #сетевой #инвестиции #криптовалюта #работавинтернете #сетевоймаркетинг #биткоин #пассивныйдоход #сетевойэтомое #новыйсетевой #бизнесс #маркетинг #заработок #работаонлайн #заработать #бизнесмен #финансы</textarea>
            </div>
        </div>
    </div>
</section>