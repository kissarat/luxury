<?php
$lang = Yii::$app->language;
?>

<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'ICO Cases') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'ICO Cases') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
            <div class="row">
                <div class="col s12 m6 l6">
                    <p class="caption"><?= Yii::t('app', 'Photo for Facebook, VK, Twitter, etc') ?></p>
                </div>
            </div>


            <div class="row" style="margin-bottom: 30px;">
                <div class="col s12 m6 l6">
                    <div class="material-placeholder" >
                        <img class="materialboxed intialized"
                             src="/promo/cases/<?= $lang ?>/1.jpg" alt="The Best ICO Cases Profit" 
                             width="100%">
                    </div>
                    <br>
                    <a href="/promo/cases/<?= $lang ?>/1.jpg"
                       class="btn-floating btn-large waves-effect light-blue lighten-1 right"
                       download>
                        <i class="mdi-editor-vertical-align-bottom"></i>
                    </a>
                </div>
                <div class="col s12 m6 l6">
                    <div class="material-placeholder" ><img class="materialboxed intialized"
                                                                    src="/promo/cases/<?= $lang ?>/2.jpg" alt="The Best ICO Cases Profit"
                                                                    
                                                                    width="100%"></div>
                    <br>
                    <a href="/promo/cases/<?= $lang ?>/2.jpg"
                       class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                class="mdi-editor-vertical-align-bottom"></i></a>
                </div>
            </div>
            <div class="row" style="margin-bottom: 30px;">
                <div class="col s12 m6 l6">
                    <div class="material-placeholder" ><img class="materialboxed intialized"
                                                                    src="/promo/cases/<?= $lang ?>/3.jpg" alt="The Best ICO Cases Profit"
                                                                    
                                                                    width="100%"></div>
                    <br>
                    <a href="/promo/cases/<?= $lang ?>/3.jpg"
                       class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                class="mdi-editor-vertical-align-bottom"></i></a>
                </div>
                <div class="col s12 m6 l6">
                    <div class="material-placeholder" ><img class="materialboxed intialized"
                                                                    src="/promo/cases/<?= $lang ?>/4.jpg" alt="The Best ICO Cases Profit"
                                                                    
                                                                    width="100%"></div>
                    <br>
                    <a href="/promo/cases/<?= $lang ?>/4.jpg"
                       class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                class="mdi-editor-vertical-align-bottom"></i></a>
                </div>
            </div>
            <div class="row" style="margin-bottom: 30px;">
                <div class="col s12 m6 l6">
                    <div class="material-placeholder" ><img class="materialboxed intialized"
                                                                    src="/promo/cases/<?= $lang ?>/5.jpg" alt="The Best ICO Cases Profit"
                                                                    
                                                                    width="100%"></div>
                    <br>
                    <a href="/promo/cases/<?= $lang ?>/5.jpg"
                       class="btn-floating btn-large waves-effect light-blue lighten-1 right" download><i
                                class="mdi-editor-vertical-align-bottom"></i></a>
                </div>
            </div>
        </div>

    </div>
</section>