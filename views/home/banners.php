<?php

use yii\helpers\Url;

$nick = Yii::$app->user->identity->nick;

?>

<section id="content" class="page banners">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col  m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Banners') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li><a href="/advertise"><?= Yii::t('app', 'Advertise') ?></a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Banners') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <?php require '_adv.php' ?>
        </div>

        <div class="ru-banners">
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-120x600.gif" alt="sample">
                <h5>120x600</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-120x600.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-160x600.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>160x600</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-160x600.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-300x600.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x600</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-300x600.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-240x400.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>240x400</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-240x400.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-250x250.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>250x250</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-250x250.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-300x250.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x250</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-300x250.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-320x100.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x100</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-320x100.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-200x200.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>200x200</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-200x200.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-320x50.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x50</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-320x50.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-336x280.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>336x280</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea">
		<a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-336x280.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1">
                        <?= Yii::t('app', 'Insert the banner code on your site') ?>
                    </label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-468x60.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>468x60</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea">
		<a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-468x60.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-728x90.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>728x90</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea">
		<a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-728x90.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-970x250.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x250</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-970x250.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/ico-970x90.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x90</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/ico-970x90.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
        </div>


        <div class="en-banners">
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-120x600.gif" alt="sample">
                <h5>120x600</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-120x600.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-160x600.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>160x600</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-160x600.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-300x600.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x600</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-300x600.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-240x400.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>240x400</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-240x400.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-250x250.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>250x250</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-250x250.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-300x250.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x250</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-300x250.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-320x100.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x100</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-320x100.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-200x200.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>200x200</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-200x200.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-320x50.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x50</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea"><a
                href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-320x50.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-336x280.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>336x280</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea">
		<a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-336x280.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1">
                        <?= Yii::t('app', 'Insert the banner code on your site') ?>
                    </label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-468x60.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>468x60</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea">
		<a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-468x60.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-728x90.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>728x90</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea">
		<a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-728x90.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>


            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-970x250.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x250</h5>
                <div class="input-field col ">
	<textarea id="textarea1 myparams" class="materialize-textarea">
		<a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                    src="https://office.ico-holding.com/promo/banners/03/en/ico-970x250.gif"
                    alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/03/en/ico-970x90.gif"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x90</h5>
                <div class="input-field col ">
		<textarea id="textarea1 myparams banner-params" class="materialize-textarea"><a
                    href="https://ico-holding.com/?r=<?= $nick ?>"><img
                        src="https://office.ico-holding.com/promo/banners/03/en/ico-970x90.gif"
                        alt="banner ico holding"></a></textarea>
                    <label for="textarea1">
                        <?= Yii::t('app', 'Insert the banner code on your site') ?>
                    </label>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="ru-banners">
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-120x600.png" alt="sample">
                <h5>120x600</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-120x600.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-160x600.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>160x600</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-160x600.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-300x600.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x600</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-300x600.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-240x400.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>240x400</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-240x400.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-250x250.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>250x250</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-250x250.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-300x250.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x250</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-300x250.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-320x100.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x100</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-320x100.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-200x200.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>200x200</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-200x200.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-320x50.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x50</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-320x50.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-336x280.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>336x280</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea">
                <a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-336x280.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1">
                        <?= Yii::t('app', 'Insert the banner code on your site') ?>
                    </label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-468x60.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>468x60</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea">
                <a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-468x60.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-728x90.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>728x90</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea">
                <a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-728x90.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-970x250.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x250</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-970x250.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/ico-970x90.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x90</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/ico-970x90.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
        </div>


        <div class="en-banners">
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-120x600.png" alt="sample">
                <h5>120x600</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-120x600.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-160x600.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>160x600</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-160x600.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-300x600.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x600</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-300x600.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-240x400.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>240x400</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-240x400.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-250x250.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>250x250</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-250x250.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-300x250.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>300x250</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-300x250.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-320x100.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x100</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-320x100.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-200x200.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>200x200</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-200x200.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-320x50.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>320x50</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea"><a
                        href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-320x50.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>

            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-336x280.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>336x280</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea">
                <a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-336x280.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1">
                        <?= Yii::t('app', 'Insert the banner code on your site') ?>
                    </label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-468x60.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>468x60</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea">
                <a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-468x60.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-728x90.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>728x90</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea">
                <a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-728x90.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>


            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-970x250.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x250</h5>
                <div class="input-field col ">
			<textarea id="textarea1 myparams" class="materialize-textarea">
                <a href="https://ico-holding.com/?r=<?= $nick ?>"><img
                            src="https://office.ico-holding.com/promo/banners/02/en/ico-970x250.png"
                            alt="banner ico holding"></a></textarea>
                    <label for="textarea1"
                    ><?= Yii::t('app', 'Insert the banner code on your site') ?></label>
                </div>
            </div>
            <div>
                <img src="https://office.ico-holding.com/promo/banners/02/en/ico-970x90.png"
                     alt="Banners ICO Holding"
                     title="Banners of ICO Holding">
                <h5>970x90</h5>
                <div class="input-field col ">
			    <textarea id="textarea1 myparams banner-params" class="materialize-textarea"><a
                            href="https://ico-holding.com/?r=<?= $nick ?>"><img
                                src="https://office.ico-holding.com/promo/banners/02/en/ico-970x90.png"
                                alt="banner ico holding"></a></textarea>
                    <label for="textarea1">
                        <?= Yii::t('app', 'Insert the banner code on your site') ?>
                    </label>
                </div>
            </div>
        </div>


    </div>
</section>