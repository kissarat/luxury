<div id="flat-button" class="section">
    <a class="waves-effect waves-light btn light-blue lighten-1 white-text" href="/social">
        <?= Yii::t('app', 'Social Networks') ?>
    </a>
    <a class="waves-effect waves-light btn light-blue lighten-1 white-text" href="/pdf">
        <?= Yii::t('app', 'PDF Presentation') ?>
    </a>
    <a class="waves-effect waves-light btn light-blue lighten-1 white-text" href="/landing">
        <?= Yii::t('app', 'Landing Page') ?>
    </a>
    <a class="waves-effect waves-light btn light-blue lighten-1 white-text" href="/banners">
        <?= Yii::t('app', 'Banners') ?>
    </a>
    <a class="waves-effect waves-light btn light-blue lighten-1 white-text" href="/home/cases">
        <?= Yii::t('app', 'ICO cases') ?>
    </a>
    <a class="waves-effect waves-light btn light-blue lighten-1 white-text" href="/home/instagram">
        <?= Yii::t('app', 'Instagram') ?>
    </a>
    <a class="waves-effect waves-light btn light-blue lighten-1 white-text" href="/home/widgets">
        <?= Yii::t('app', 'Widgets / Calculator') ?>
    </a>
</div>