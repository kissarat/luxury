<?php

use app\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\data\ArrayDataProvider $provider
 */
?>
<section class="structure-index" id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">
                        <?= Yii::t('app', 'My Tariffs') ?>
                    </h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'My Tariffs') ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div id="striped-table">
                <div class="row">
                    <div class="col m12 s12 l12">
                        <div class="page transfer-index">
                            <?= GridView::widget([
                                'dataProvider' => $provider,
                                'layout' => '{items}',
                                'tableOptions' => ['class' => 'striped bordered responsive-table hoverable'],
                                'emptyText' => 'Nothing found',
                                'rowOptions' => function ($model) {
                                    return ['class' => $model->status];
                                },
                                'columns' => [
                                    [
                                        'attribute' => 'created',
                                        'format' => 'datetime',
                                        'contentOptions' => function ($model) {
                                            return ['data-date' => strtotime($model->created)];
                                        }
                                    ],
                                    'name',
                                    [
                                        'attribute' => 'amount',
                                        'label' => Yii::t('app', 'Invest Amount'),
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            $sign = $model->user === Yii::$app->user->identity->user ? '+' : '-';
                                            $currency = strtolower($model->currency);
                                            return "<span class=\"sign\">$sign</span><span class=\"$currency\">$model->amount</span>";
                                        }
                                    ],
                                    'accrues',
                                    [
                                        'attribute' => 'profit',
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            $currency = strtolower($model->currency);
                                            return "<span class=\"$currency\">$model->profit</span>";
                                        }
                                    ],
//                                'currency',

                                    [
                                        'attribute' => 'closing',
                                        'format' => 'datetime',
                                        'contentOptions' => function ($model) {
                                            return ['data-date' => strtotime($model->closing)];
                                        }
                                    ],
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>