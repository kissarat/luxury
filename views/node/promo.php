<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

?>

<div class="page node-promo">
    <h4><?= Yii::t('app', 'Promo') ?></h4>
    <?php
    echo GridView::widget([
        'dataProvider' => new ArrayDataProvider(['models' => $models]),
        'layout' => '{items}',
        'columns' => [
            [
                'attribute' => 'created',
                'format' => 'datetime',
                'label' => Yii::t('app', 'Created'),
                'contentOptions' => function ($model) {
                    return ['data-date' => strtotime($model->created)];
                }
            ],
            [
                'attribute' => 'nick',
                'label' => Yii::t('app', 'Username')
            ],
            [
                'attribute' => 'amount',
                'label' => Yii::t('app', 'Amount'),
                'format' => 'html',
                'value' => function ($model) {
                    $currency = strtolower($model->currency);
                    return "<span class=\"$currency\">$model->amount</span>";
                }
            ],
        ]
    ]);
    ?>
</div>
