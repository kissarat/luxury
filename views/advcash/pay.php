<?php use yarcode\advcash\RedirectForm;

echo RedirectForm::widget([
    'api' => Yii::$app->get('advcash'),
    'invoiceId' => $id,
    'amount' => $amount,
    'description' => '',
]);
