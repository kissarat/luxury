<?php if (!YII_DEBUG): ?>
    <script type='text/javascript'>
      (function () {
        var widget_id = 'TnKkzFiwhQ';
        var d = document;
        var w = window;

        function l() {
          var s = document.createElement('script');
          s.type = 'text/javascript';
          s.async = true;
          s.src = '//code.jivosite.com/script/widget/' + widget_id;
          var ss = document.getElementsByTagName('script')[0];
          ss.parentNode.insertBefore(s, ss);
        }

        if (d.readyState == 'complete') {
          l();
        } else {
          if (w.attachEvent) {
            w.attachEvent('onload', l);
          } else {
            w.addEventListener('load', l, false);
          }
        }
      })();</script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
          try {
            w.yaCounter46356693 = new Ya.Metrika({
              id: 46356693,
              clickmap: true,
              trackLinks: true,
              accurateTrackBounce: true,
              webvisor: true
            });
          } catch (e) {
          }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
          n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js";
        if (w.opera == "[object Opera]") {
          d.addEventListener("DOMContentLoaded", f, false);
        } else {
          f();
        }
      })(document, window, "yandex_metrika_callbacks"); </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/46356693" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript> <!-- /Yandex.Metrika counter -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108432612-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', 'UA-108432612-1');
    </script>
<?php endif;
