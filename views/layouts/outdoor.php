<?php
use app\Alert;
use app\assets\OutdoorAsset;
use yii\helpers\Html;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

OutdoorAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="login"
      xmlns:og="http://ogp.me/ns#">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="geo.region" content="PL"/>
    <meta name="geo.position" content="51.076077;17.039359"/>
    <meta name="ICBM" content="51.076077, 17.039359"/>
    <meta name="description" content="Join to the out community of successful investors now!"/>
    <meta name="keywords" content="ico, cryptocurrency, investment"/>
    <meta property="og:image" content="https://ico-holding.com/wp-content/uploads/2017/08/site-part.png" />
    <meta property="og:image:width" content="888" />
    <meta property="og:image:height" content="444" />
    <meta property="og:site_name" content="ICO Holding" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="ICO Holding" />
    <meta name="twitter:image" content="https://ico-holding.com/wp-content/uploads/2017/08/site-part.png" />
    <meta name="generator" content="Drupal 7 (http://drupal.org)" />

    <link rel="preconnect" href="//fonts.googleapis.com" />
    <link rel="dns-prefetch" href="//fonts.googleapis.com" />
    <link rel="preconnect" href="//fonts.gstatic.com" crossorigin="" />
    <link rel="dns-prefetch" href="//fonts.gstatic.com" />
    <link rel="dns-prefetch" href="//www.google-analytics.com" />
    <link rel="dns-prefetch" href="//stats.g.doubleclick.net" />
    <link rel="preconnect" href="//stats.g.doubleclick.net" />
    <link rel="preconnect" href="//www.google-analytics.com" />
    <title><?= $this->title ?> | ICO HOLDING</title>

    <!-- Favicons-->
    <link rel="icon" href="/images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="/images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="/images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->

    <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet"
          media="screen,projection">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body id="outdoor" class="outdoor-body login-bg" data-uri="<?= $_SERVER['REQUEST_URI'] ?>">

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?php $this->beginBody(); ?>

<?= Alert::widget() ?>

<?= $content ?>

<?php $this->endBody() ?>

<!-- ================================================
   Scripts
   ================================================ -->

<!-- jQuery Library -->
<script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
<!--materialize js-->
<script type="text/javascript" src="/js/materialize.js"></script>
<!--prism-->
<script type="text/javascript" src="/js/prism.js"></script>
<!--scrollbar-->
<script type="text/javascript" src="/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="/js/plugins.js"></script>

<script src="//api.ipify.org/?format=jsonp&callback=main"></script>

<?php require '_external.php' ?>

</body>
</html>
<?php $this->endPage() ?>
