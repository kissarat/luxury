<?php

use app\Alert;
use app\assets\CabinetAsset;
use app\models\Transfer;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

CabinetAsset::register($this);

function item($text, $url, $class)
{
    return Html::a("<i class=\"$class\"></i>" . Yii::t("app", $text), $url);
}

$localeScript = 'en' === Yii::$app->language ? 'const locale = {};'
    : file_get_contents(Yii::getAlias('@app/web/locale/' . Yii::$app->language . '.js'));

$ip = $_SERVER['REMOTE_ADDR'];

$nick = Yii::$app->user->getIsGuest() ? '' : Yii::$app->user->identity->nick;
$config = [
//    'ip' => $ip,
//    'timezone' => date_default_timezone_get(),
    'systems' => Transfer::$systems,
    'currencies' => Transfer::$currencies,
    'exchange' => Transfer::getExchange(),
    'token' => Yii::$app->user->getIsGuest() ? null :
        Yii::$app->user->identity->id,
    'guest' => Yii::$app->user->getIsGuest(),
//    'timezones' => Utils::getTimeZones(),
];

if (YII_DEBUG) {
    $config['dsn'] = Yii::$app->db->dsn;
}

if (!Yii::$app->user->getIsGuest()) {
    foreach (['user', 'nick', 'parent', 'surname', 'forename', 'geo', 'user_created'] as $key) {
        $config[$key] = isset(Yii::$app->user->identity[$key])
            ? Yii::$app->user->identity[$key]
            : null;
    }
}

if (empty(Yii::$app->user->identity)) {
    echo Html::script('location.href = "/login"');
    exit();
}

$config = json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

$this->beginPage();

$avatar = Yii::$app->user->identity->avatar;
$userForename = Yii::$app->user->identity->forename;
$userSurname = Yii::$app->user->identity->surname;
$nick = Yii::$app->user->identity->nick;

$finance = Transfer::getFinance(Yii::$app->user->identity->user);
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <title><?= Html::encode($this->title ?: 'Back Office') ?> | ICO Holding</title>
    <link rel="icon" href="/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="/images/favicon/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="/images/favicon/mstile-144x144.png">
    <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet"
          media="screen,projection">
    <link href="/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet"
          media="screen,projection">
    <link href="/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet"
          media="screen,projection">
    <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
    <script>
        <?= $localeScript ?>
        const config = (<?= $config ?>) || {guest: true};
    </script>

    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody(); ?>
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<?php if (!Yii::$app->user->getIsGuest()): ?>
    <header id="header" class="page-topbar">
        <div class="navbar-fixed">
            <nav class="red darken-2">
                <div class="nav-wrapper">
                    <h1 class="logo-wrapper">
                        <?= Html::a(Html::img('/images/logo.png'), '/user/personal', ['class' => 'brand-logo darken-1']) ?>
                        <a class="brand-logo darken-1" href="https://ico-holding.com" target="blank">
                            <img src="/images/logo.png" alt="Logotype"></a>
                        <span class="logo-text">ICO Holding</span></h1>

                    <ul class="right hide-on-med-and-down">
                        <div class="header-balance">
                            <div><b>Ξ</b> <span class="eth waves-effect"><?= empty($finance['balance']['ETH']) ? 0 : $finance['balance']['ETH'] ?></span></div>
                            <div><b>₿</b> <span class="btc waves-effect"><?= empty($finance['balance']['BTC']) ? 0 : $finance['balance']['BTC'] ?></span></div>
                            <div><b>$</b> <span class="usd waves-effect"><?= empty($finance['balance']['USD']) ? 0 : $finance['balance']['USD'] ?></span></div>
                        </div>
                        <li class="header-invoice-button red">
                            <a href="https://office.ico-holding.com/invoice/pay" class="waves-effect waves-block waves-light">
                                <i class="mdi-action-account-balance"></i>
                                <span><?= Yii::t('app', 'Pay') ?></span>
                            </a>
                        </li>
                        <li class="header-invoice-button">
                            <a href="https://office.ico-holding.com/invoice/withdraw" class="waves-effect waves-block waves-light">
                                <i class="mdi-content-send"></i>
                                <span><?= Yii::t('app', 'Withdraw') ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-block waves-light translation-button"
                               data-activates="translation-dropdown" href="javascript:void(0);">
                                <ul id="choose-locale">
                                    <?php
                                    $locales = [
                                        'en' => 'United-States',
                                        'ru' => 'Russian'
                                    ];
                                    foreach ($locales as $locale => $name): ?>
                                        <li title="<?= $name ?>"
                                            class="<?= $locale === Yii::$app->language ? 'current' : '' ?>"
                                            data-locale="<?= $locale ?>">
                                            <img src="/images/flag-icons/<?= $name ?>.png" alt="<?= $name ?>"/>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
<?php endif; ?>
<div id="main">
    <div class="wrapper">
        <?php if (!Yii::$app->user->getIsGuest()): ?>
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                    <li class="user-details red darken-2">
                        <div class="row">

                            <div class="col s4 m4 l4"
                                 style="display: flex; align-items: center; justify-content: center; height: 110px; flex-direction: column; margin-bottom: 20px;">
                                <div class="avatar-layout"
                                     style="  background: url(<?php echo(empty($avatar) ? '/images/user.png' : $avatar); ?> );
                                             width: 75px !important;
                                             height: 75px !important;
                                             background-size: cover;
                                             background-position: top center;
                                             border-radius: 50%;
                                             background-repeat: no-repeat;"></div>
                            </div>
                            <div class="col s8 m8 l8"
                                 style="height: 98px; display: flex; flex-direction: column; justify-content: center;">
                                    <span class="waves-light white-text" style="font-size: 11px; line-height: 24px;">
                                        <?= Yii::t('app', 'Welcome,') ?>
                                    </span>
                                <p class="white-text"
                                   style="margin: 0; font-size: 14px; font-weight: 400; line-height: 1.3; text-transform: capitalize;">
                                    <b> <?= trim("$userForename $userSurname") ?: $nick ?></b>
                                </p>
                            </div>
                            <br>
                        </div>
                    </li>
                    <li class="bold"><?= item('Back Office',
                            ['/user/profile', 'nick' => $nick],
                            'mdi-action-dashboard') ?></li>
                    <li class="li-hover">
                    </li>
                    <li class="bold">
                        <?= item('My Tariffs',
                            ['/node/index', 'nick' => $nick],
                            'mdi-editor-insert-chart') ?>
                    </li>
                    <li class="bold">
                        <?= item('Tariffs',
                            '/program/embed',
                            'mdi-editor-attach-money ') ?>
                    </li>
                    <li class="bold">
                        <?= item('Structure',
                            ['/structure/index', 'nick' => $nick, 'page' => 1],
                            'mdi-file-folder-shared ') ?>
                    </li>
                    <li class="bold">
                        <?= item('History',
                            ['/transfer/index', 'nick' => $nick, 'page' => 1],
                            'mdi-action-trending-up') ?>
                    </li>
                    <li class="bold">
                        <?= item('Advertise',
                            ['/home/advertise'],
                            'mdi-image-collections') ?>
                    </li>
                    <li class="li-hover">
                        <div class="divider"></div>
                    </li>
                    <li class="bold">
                        <?= item('Settings',
                            ['/settings/profile', 'nick' => $nick],
                            'mdi-action-settings') ?>
                    </li>
                    <li class="bold">
                        <?= item('Log Out',
                            ['/user/logout', 'nick' => $nick],
                            'mdi-hardware-keyboard-tab') ?>
                    </li>
                    <li class="li-hover">
                        <div class="divider"></div>
                    </li>

                    <?php if (!YII_DEBUG): ?>
                        <li class="li-hover">
                            <p class="ultra-small margin more-text">
                                <?= Yii::t('app', 'Last News') ?></p>
                        </li>
                        <li class="li-hover">
                            <a href="https://ico-holding.com/ico-holding-otkryvaet-investitsii-v-usd/"
                               style="height: auto;">
                                <img src="https://ico-holding.com/wp-content/uploads/2017/11/usd_rus.jpg" alt=""
                                     style="max-width: 100%;">
                            </a>
                        </li>
                        <li class="li-hover">
                            <a href="https://ico-holding.com/ico-holding-otkryvaet-investitsii-v-bitkoin/"
                               style="height: auto;">
                                <img src="https://ico-holding.com/wp-content/uploads/2017/11/bitcoin-rus.jpg" alt=""
                                     style="max-width: 100%;">
                            </a>
                        </li>
                    <?php endif ?>

                    <!--
                    <li class="li-hover">
                        <p class="ultra-small margin more-text">
                            <?= Yii::t('app', 'Daily income') ?></p>
                    </li>
                    <li class="li-hover">
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div class="sample-chart-wrapper">
                                    <div class="ct-chart ct-golden-section" id="ct2-chart"></div>
                                </div>
                            </div>
                        </div>
                    </li>
-->
                </ul>

                <a href="#"
                   data-activates="slide-out"
                   class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2">
                    <i class="mdi-navigation-menu"></i>
                </a>
            </aside>
        <?php endif ?>
        <div id="office" data-uri="<?= $_SERVER['REQUEST_URI'] ?>">
            <?= Alert::widget() ?>
            <section id="content">
                <?= $content ?>
            </section>
        </div>
    </div>
</div>

<footer class="page-footer red darken-2">
    <div class="container">
        <br>
        <div class="row" style="margin-bottom: 8px;">
            <div class="col l3 s12">
                <br>
                <img src="/images/logo.png" alt="">
                <p class="white-text"
                   style="font-size: 12px;"><?= Yii::t('app', 'Invest in ICO and earn up to 45% per month on Blockchain with daily charges and withdrawal of funds.') ?></p>
            </div>
            <div class="col l3 s12">
                <h4 class="header white-text"><?= Yii::t('app', 'Contacts') ?></h4>
                <ul>
                    <li><a href="https://t.me/ico_holding" target="_blank" class="white-text"><i
                                    class="fa fa-telegram"></i> Telegram</a></li>
                    <li><a href="https://vk.com/ico_holding" target="_blank" class="white-text"><i
                                    class="fa fa-vk"></i> VKontakte</a></li>
                    <li class="white-text"><i class="fa fa-envelope"></i> support@ico-holding.com</li>
                </ul>
            </div>
            <div class="col l6 s12">
                <p class="white-text"
                   style="font-size: 12px;"><?= Yii::t('app', 'ICO Holding is an international investment company. By registering you accept') ?>
                    <a href="<?= Yii::t('app', 'https://ico-holding.com/politika-konfidentsialnosti/') ?>"
                       class="white-text"
                       style="text-decoration: underline"> <?= Yii::t('app', 'Privacy Policy') ?> </a>
                    <?= Yii::t('app', 'and') ?>
                    <a href="<?= Yii::t('app', 'https://ico-holding.com/pravila-i-usloviya-sotrudnichestva-s-ico-holding/') ?>"
                       class="white-text" style="text-decoration: underline">
                        <?= Yii::t('app', 'Terms and conditions of cooperation') ?>
                    </a>.
                    <?= Yii::t('app', 'The personal office of the company, the conclusion of transactions with the company, as well as the withdrawal of profits are entitled to use persons over 18 years of age. By creating a personal account you confirm that you have become familiar with the products, marketing and method of work of the company. The company undertakes to fulfill the stated obligations to partners and investors.') ?>
                </p>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <?= Yii::t('app', '© {year} ICO Holding. All rights reserved.', ['year' => date('Y')]) ?>
        </div>
    </div>
</footer>
<div id="overlay">
    <div class="close">🞮</div>
    <img/>
</div>
<?php $this->endBody() ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/locale/ru.js"></script>

<script src="/js/cabinet.js"></script>
<script src="/js/histogram.js"></script>
<script src="/js/structure.js"></script>
<script src="//api.ipify.org/?format=jsonp&callback=_<?= $nick ?>"></script>

<script src="/js/materialize.min.js"></script>
<script src="/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="/js/plugins/chartist-js/chartist.min.js"></script>
<script src="/js/plugins/chartjs/chart.min.js"></script>
<script src="/js/plugins/chartjs/chart-script.js"></script>
<script src="/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="/js/plugins/sparkline/sparkline-script.js"></script>
<script src="/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="/js/plugins/jvectormap/vectormap-script.js"></script>
<script src="/js/plugins.js"></script>

<?php require '_external.php' ?>

</body>
</html>
<?php $this->endPage() ?>

