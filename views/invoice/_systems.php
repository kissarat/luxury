<div id="widget-invoice-systems">
    <ul>
        <li v-for="(s, id) in systems"
            v-if="!s.crypto || 'withdraw' === action"
            v-on:data-system="id"
            v-bind:title="s.title"
            v-on:click="system = id"
            v-bind:class="{current: id === system}"
            v-bind:style="{backgroundImage: 'url(' + s.image + ')'}">
<!--            {{ s.title }}-->
        </li>
    </ul>
</div>
