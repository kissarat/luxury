<?php

use yii\bootstrap\ActiveForm;

echo \app\helpers\Html::script('const wallets = ' . json_encode($wallets));
?>

<section id="content">
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Payment') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Payment') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="row">
                <div id="page-invoice-pay">
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.3/vue.min.js"></script>
                    <div id="app">
                        <div class="col s12 m6 l6">
                            <div class="card-panel-no-margin">
                                <div class="payment">
                                    <?= $this->render('_systems') ?> 
                                    <div class="card-panel">
                                        <?php $form = ActiveForm::begin() ?>
                                        <?= $form->field($model, 'amount', ['template' => '<div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-editor-attach-money prefix"></i>{input}{label}{error}{hint}</div></div>'
                                        ])->textInput(['maxlength' => 255, 'id' => 'amount', 'class' => '', 'v-model' => 'amount'])
                                        ?>
                                        <?= $form->field($model, 'system')->hiddenInput(['v-bind:value' => 'system'])->label(false) ?>

                                        <div class="row">
                                            <div class="input-field col s12">
                                                <button type="submit"
                                                        v-bind:disabled="!validAmount"
                                                        class="btn-large red darken-2 waves-effect waves-light">
                                                    <i class="mdi-content-send right"></i>
                                                    <?= Yii::t('app', 'Pay') ?>
                                                </button>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6">
                            <div class="card-panel min-height-card">
                                <div class="addresses">
                                    <ul>
                                        <li v-for="(s, id) in systems"
                                            v-if="s.crypto">
                                            <img v-bind:src="s.image" v-bind:alt="s.title"/>

                                            <div v-if="wallets[id]" class="value">{{ wallets[id] }}</div>
                                            <button v-else
                                                    type="button"
                                                    v-on:click="fetchAddress(id)"
                                                    class="btn light-blue lighten-1 waves-effect waves-light">
                                                <?= Yii::t('app', 'Get Address') ?>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <script src="/js/invoice.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>