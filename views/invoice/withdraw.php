<?php

use yii\bootstrap\ActiveForm;

?>

<section id="content">
    <script>const wallets = {}</script>
    <div id="breadcrumbs-wrapper" class="grey lighten-3">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title"><?= Yii::t('app', 'Withdraw') ?></h5>
                    <ol class="breadcrumb">
                        <li><a href="/">ICO HOLDING</a>
                        </li>
                        <li class="active"><?= Yii::t('app', 'Withdraw') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="row">
                <div id="page-invoice-withdraw">
                    <div id="app">
                        <div class="col s12 m12 l12">
                            <div class="card-panel-no-margin">
                                <div class="payment">
                                    <?= $this->render('_systems') ?>
                                    <div class="card-panel">
<!--                                        <?= Yii::t('app', 'The commission is approximately {amount} and may differ at the time of withdrawal of money', [
                                            'amount' => '0.5 USD'
                                        ]) ?> -->

                                        <?php $form = ActiveForm::begin() ?>
                                        <?= $form->field($model, 'system')->hiddenInput(['v-bind:value' => 'system'])->label(false) ?>

                                        <?= $form->field($model, 'amount', ['template' => '<div class="row margin">
                                        <div class="input-field col s12">
                                        <i class="mdi-editor-attach-money prefix"></i>{input}{label}{error}{hint}</div></div>'
                                        ])->textInput(['maxlength' => 255, 'id' => 'amount', 'class' => '', 'v-model' => 'amount'])
                                        ?>

                                        <?= $form->field($model, 'wallet', ['template' => '<div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-action-account-balance-wallet prefix"></i>{input}<label>' . Yii::t('app', 'Wallet') . ' </label>{error}{hint}</div></div>'
                                        ])->textInput(['maxlength' => 128, 'id' => 'wallet', 'class' => '', 'v-model' => 'wallet'])
                                        ?>

                                        <?= $form->field($model, 'text', ['template' => '<div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-editor-insert-comment prefix"></i>{input}{label}{error}{hint}</div></div>'
                                        ])->textInput(['maxlength' => 255, 'id' => 'text', 'class' => '', 'v-model' => 'text'])
                                        ?>

                                        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

                                        <div class="row">
                                            <div class="input-field col s12">
                                                <button type="submit"
                                                        v-bind:disabled="!valid"
                                                        class="btn-large red darken-2 waves-effect waves-light">
                                                    <i class="mdi-content-send right"></i>
                                                    <?= Yii::t('app', 'Withdraw') ?>
                                                </button>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end() ?>
                                    </div>
                                </div>
                            </div>
                            <script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.5.3/vue.min.js"></script>
                            <script src="/js/invoice.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
