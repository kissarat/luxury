<?php

namespace app\components;


use app\models\Log;
use app\traits\HTTPTrait;
use yii\base\Component;

class Telegram extends Component
{
    use HTTPTrait;

    public $token;
    public $chat_id;
    public $enabled = true;

    public function getBaseURL() {
        return "https://api.telegram.org/bot$this->token/";
    }

    public function request($method, $params) {
        if ($this->enabled) {
            $response = $this->client->post($this->getBaseURL() . $method, $params)
                ->send()
                ->getData();
            if ($response['ok']) {
                return $response['result'];
            } else {
                throw new \Exception('Telegram: ' . $response['description']);
            }
        }
    }

    public function send($params) {
        $params['chat_id'] = $this->chat_id;
        $this->request('sendMessage', $params);
    }

    public function sendText(string $text) {
        $this->send(['text' => $text]);
    }
}
