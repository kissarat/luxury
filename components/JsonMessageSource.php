<?php

namespace app\components;


use app\helpers\Utils;
use yii\i18n\MessageSource;

class JsonMessageSource extends MessageSource
{
    protected function loadMessages($category, $language) {
        if ('en' === $language) {
            return [];
        }
        else {
            return Utils::getLocale($language);
        }
    }
}
