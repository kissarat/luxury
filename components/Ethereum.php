<?php

namespace app\components;

use app\models\User;
use app\traits\RPCTrait;
use Yii;
use yii\base\Component;

class Ethereum extends Component {
    use RPCTrait;

    public function getUserAddress(int $id = null, $password = 'eeghazei5deehuub7seug2ielee6IChie6on') {
        if (!$id) {
            $id = Yii::$app->user->identity->user;
        }
        $user = User::find()
            ->where(['id' => $id])
            ->select(['id', 'ethereum'])
            ->one();
        if (!$user->ethereum) {
            $user->ethereum = $this->newAccount($password);
            $this->unlockAccount($user->ethereum, $password);
            if ($user->save(true, ['ethereum'])) {
                return $user->ethereum;
            }
            throw new \Exception('Cannot save ethereum address');
        }
        return $user->ethereum;
    }

    public function getBalance(string $address, string $tag = 'latest') {
        return $this->request('eth_getBalance', [$address, $tag]);
    }

    public function newAccount($password = '') {
        return $this->request('personal_newAccount', [$password]);
    }

    public function getListAccounts() {
        return $this->request('personal_getListAccounts');
    }

    public function lockAccount($address) {
        return $this->request('personal_lockAccount', [$address]);
    }

    public function unlockAccount($address, $password = '') {
        return $this->request('personal_unlockAccount', [$address, $password]);
    }
}
