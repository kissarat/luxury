<?php

namespace app\components;


use Yii;
use yii\base\Component;
use yii\web\BadRequestHttpException;

class Perfect extends Component {
    public $wallet;
    public $secret;
    public $method;

    public function getSecretHash() {
        return strtoupper(md5($this->secret));
    }

    public function verify($data) {
        $fields = [
            'PAYMENT_ID' => '/^\d+$/',
            'PAYEE_ACCOUNT' => '/^U\d{7,8}$/',
            'PAYMENT_AMOUNT' => '/^\d+(\.\d{2})?$/',
            'PAYMENT_UNITS' => '/^USD$/',
            'PAYMENT_BATCH_NUM' => '/^\d+$/',
            'PAYER_ACCOUNT' => '/^U\d{7,8}$/',
            'TIMESTAMPGMT' => '/^\d+$/',
            'V2_HASH' => '/^[0-9A-Z]+$/',
        ];
        foreach ($fields as $name => $pattern) {
            if (!empty($data[$name]) && !preg_match($pattern, $data[$name])) {
                throw new BadRequestHttpException(Yii::t('app', 'Invalid parameter {name}', [
                    'name' => $name
                ]));
            }
        }

        if ($this->wallet != $data['PAYEE_ACCOUNT']) {
            throw new BadRequestHttpException(Yii::t('app', 'Invalid payee'));
        }

        $string = implode(':', [
            $data['PAYMENT_ID'],
            $data['PAYEE_ACCOUNT'],
            $data['PAYMENT_AMOUNT'],
            $data['PAYMENT_UNITS'],
            $data['PAYMENT_BATCH_NUM'],
            $data['PAYER_ACCOUNT'],
            $this->getSecretHash(),
            $data['TIMESTAMPGMT']
        ]);

        if ($data['V2_HASH'] != strtoupper(md5($string))) {
            throw new BadRequestHttpException(Yii::t('app', 'Invalid hash'));
        }
        return true;
    }
}
