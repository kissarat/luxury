<?php

namespace app\components;


use app\models\User;
use yii\base\Component;
use yii\httpclient\Client;

class Blockio extends Component
{
    public $key;
    public $secret;
    public $version = 2;
    public $delay = 0.34;
    public $last;
    public $client;

    public function init() {
        $this->client = new Client([
            'requestConfig' => [
                'format' => Client::FORMAT_JSON
            ],
            'responseConfig' => [
                'format' => Client::FORMAT_JSON
            ],
        ]);
        $this->last = 0;
    }

    public function invoke($method, $params = []) {
        $delta = microtime(true) - $this->last;
        if ($delta < $this->delay) {
            $d = (int)(($this->delay - $delta) * 1000 * 1000);
            usleep($d);
        }
        $this->last = microtime(true);
        $params['api_key'] = $this->key;
        $url = "https://block.io/api/v2/$method/?" . http_build_query($params);
//        header('URL: ' . $url);
        return $this->client->get($url)
            ->send()
            ->getData();
    }

    public function unarchive($labels) {
        $this->invoke('unarchive_addresses', ['labels' => implode(',', $labels)]);
    }

    public function getAddress($nick) {
        $params = ['label' => $nick];
        $exist = $this->invoke('get_address_by_label', $params);
        $address = null;
        if ('success' === $exist['status']) {
            $this->unarchive([$nick]);
            $address = $exist['data']['address'];
        } else {
            $new = $this->invoke('get_new_address', $params);
//            header('NEW: ' . json_encode($new));
            if ('success' === $new['status']) {
                $address = $new['data']['address'];
            }
        }
        return $address;
    }

    public function getUserAddress($nick) {
        $user = User::find()
            ->where(['nick' => $nick])
            ->select(['id', 'nick', 'blockio'])
            ->one();
        if ($user && $user->blockio) {
            $this->unarchive([$nick]);
            return $user->blockio;
        }
        $address = $this->getAddress($nick);
        if ($user && $address) {
            $user->blockio = $address;
            $user->save(false);
        }
        return $address;
    }
}
