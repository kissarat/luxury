<?php

namespace app\components;

class Session extends \yii\web\Session {
    public function getUseCustomStorage() {
        return true;
    }

    public function get($key, $defaultValue = null) {
        if ('id' === $key) {
            return session_id();
        }
        return parent::get($key, $defaultValue);
    }
}
